# ns-3 IEEE 80211ad RTS DMG CTS

This work is based on ns-3 IEEE 802.11ad model developped by Hany Assasa (IMDEA).

This repo contains the updated model and the python scripts to evaluate the 
performance of a parametric scenario of N STA sending UDP traffic to the AP.

## 1. Installation

This work relies on the last release of the IEEE 802.11ad code from Hany Assasa bitBucket.
Just clone the repo and build.

### Python environment

The Python scripts require some libraries:

*  **Matplotlib**
*  **numpy**
*  **pylab**

### ns-3 environment

Clean, Configure and build, the configure command is specific to the 802.11ad implementation. :

**Clean**
```
./waf clean
```
**Configure**
```
./waf configure --disable-examples --disable-tests --disable-python --enable-modules='applications','core','internet','point-to-point','wifi','flow-monitor'

```
**Build**
```
./waf build

```

To build the project in optimized mode for fast execution type the following command:
```
./waf configure --disable-examples --disable-tests --disable-python --enable-modules='applications','core','internet','point-to-point','wifi' --enable-static -d optimized
./waf build
```

## 2. Usage

## ns-3

To run the scenario, type the following command:
```
./waf --run "scenario_F_test --scheme=1 --dataRate='100Mbps' --phyMode='DMG_MCS5' --name=Scenario_F_Test1 --simulationTime=10 --disApSta=5 --nodeNb=25 --run=1 --rtscts=1 --distAngle=360 --path='results/' --pcap=false --verbose=false"
```

Where:

*  **scheme** is the access scheme used for channel access (0=SP,1=CBAP)
*  **dataRate** is the application data rate
*  **phyMode** is the 802.11ad PHY Mode
*  **name** is string used in log files to identify the simuulation
*  **simulationTime** is the simulation time in seconds
*  **disApSta** is the distance in meters used to uniformaly distribute the STA around the AP
*  **nodeNb** is the number of STA
*  **run** is the run number value
*  **rtscts** is the toggle to enable/disable RTS/DMG CTS (0=DISABLED,1=ENABLED)
*  **distAngle** is the angle of distribution of the STA in degrees 
*  **path** is the path to save the results (folder must be created before launching the simulation)
*  **pcap** is the command to enable PCAP tracing (0=DISABLED,1=ENABLED)
*  **verbose** is the command to turn on log components (0=DISABLED,1=ENABLED)


## Python Scripts


To execute the script to compare a specific scenario with and without RTS/DMG CTS activated, type the following command:
```
python CBAP_Simul_RTS_CTS_Slices_CBAP.py 10 5 2 20 10STA_MCS12_500Mbps 500Mbps DMG_MCS12
```

where:

*  ```10``` is the simulation time in seconds
*  ```5``` is the distance of the distribution between AP and STA
*  ```25``` is the number of STA
*  ```20``` is the number of runs of simulation
*  ```10STA_MCS12_500Mbps``` is the name of the simulation for logs
*  ```500Mbps``` is the data rate
*  ```DMG_MCS12``` is the 802.11ad PHY Mode

This script generates the following results* for both RTS/DMG CTS and NO RTS/DMG CTS cases:

*  **Throughput** Average throughput
*  **Phy State** Time during IDLE / RX / TX / CCA BUSY
*  **Delay 1** Delay taking queuing into account
*  **Delay 2** MAC delay
*  **Backoff** Average value of backoff of backoff values
*  **MacTxDrop** Box plot of the packet dropped at Mac berfore transmission
*  **PhyTxDrop** Box plot of the packet dropped at phy during transmission 
*  **PhyRxDrop** Box plot of the packet dropped at phy during reception
*  **Backoff histogram** Histogram of the backoff value depending on percentage of packet loss
*  **Contention Window size histogram** Histogram of the CW size value depending on percentage of packet loss 
*  **PacketLoss** Topology of the generated network with packetloss showed as a colomap for each STA

*Each PDF files are saved in separate folders for the two cases, all the logs are located in a folder named after the name provided in parameter.

To run a full evaluation of all MCS with adapted distances, type the following command:
```
python RTS-DMG_CTS-Evaluation-All_MCS.py
```
This script generates the same outputs as the previous one, for each MCS.



About the Model:
----------------
The model is developed and maintained by Hany Assasa. For more information about the author research,
please check his personal website on: http://people.networks.imdea.org/~hany_assasa/
