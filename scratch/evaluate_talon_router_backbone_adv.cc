/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/wifi-module.h"
#include "common-functions.h"
#include <string>
#include <iomanip>

/**
 * Simulation Objective:
 * This script is used to evaluate the performance of the CSMA/CA using TALON Router confiugration.
 * The TALON Router uses the following configuration:
 * 1. A-MPDU aggregation is enabled with aggregation up-to 32 packets or 65535 Bytes.
 * 2. A-MSDU aggregation is disabled.
 * 3. PHY SC is used and MCS_12 is used.
 * 4. The BTI access period compromises 32 sectors.
 * 5. The SLS access period compromises 35 sectors.
 * 6. ATI Present is false.
 * 7. TALON Router radiation beam patterns are utilized.
 *
 * Network Topology:
 * The network consists of three nodes (End Server, A DMG STA, and DMG PCP).
 * The scenario consists of a signle DMG STA and a single DMG PCP/AP.
 *
 *                10 Gbps/s (Variable Delay)
 *     Server <---------------------------------> DMG PCP/AP (0,0)              DMG STA (+1,0)
 *
 * Simulation Description:
 * The STA generates the traffic towards the end server (Uplink scenario)
 * The whole DTI access period is allocated as CBAP access period.
 * The simulation uses a custom TCP Send Application to access socket information.
 *
 * Running Simulation:
 * To evaluate CSMA/CA channel access scheme:
 * ./waf --run "evaluate_talon_router_backbone_adv --simulationTime=10 --pcap=true"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station.
 * 2. TCP level traces (CWND, RTT, TcpBufferChanges)
 * 3. Traffic Control Layer Queue changes.
 * 4. Wifi MAC Level Queue changes.
 */

NS_LOG_COMPONENT_DEFINE ("EvaluateTalonBackbone");

using namespace ns3;
using namespace std;

/**  Application Variables **/
uint64_t totalRx = 0;
double throughput = 0;
Ptr<PacketSink> packetSink;

/* Network Nodes */
Ptr<Node> serverNode;
Ptr<Node> apWifiNode;
Ptr<Node> staWifiNode;

double
CalculateSingleStreamThroughput (Ptr<PacketSink> sink, uint64_t &lastTotalRx, double &averageThroughput)
{
  double thr = (sink->GetTotalRx() - lastTotalRx) * (double) 8/1e5;     /* Convert Application RX Packets to MBits. */
  lastTotalRx = sink->GetTotalRx ();
  averageThroughput += thr;
  return thr;
}

void
CalculateThroughput (void)
{
  double thr = CalculateSingleStreamThroughput (packetSink, totalRx, throughput);
  std::cout << std::left << std::setw (12) << Simulator::Now ().GetSeconds ()
            << std::left << std::setw (12) << thr << std::endl;
  Simulator::Schedule (MilliSeconds (100), &CalculateThroughput);
}


/********************************************************
 *            Custom TCP Send Application
 ********************************************************/

/**
 * This code defines an application to run during the simulation that
 * setups connections and manages sending data.
 */
class TcpSendApplication : public Application
{
public:
  /**
   * Default constructors.
   */
  TcpSendApplication ();
  virtual ~TcpSendApplication ();

  /**
   * Setup the TCP send application.
   *
   * \param socket Socket to send data to.
   * \param address Address to send data to.
   * \param packetSize Size of the packets to send.
   * \param dataRate Data rate used to determine when to send the packets.
   * \param isBulk The Application behaves as BulkSendApplication or as OnOffApplication.
   */
  void Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, DataRate dataRate, bool isBulk);
  /**
   * \return the total packets transmitted
   */
  uint64_t GetTotalTxPackets (void) const;
  /**
   * \return the total bytes transmitted
   */
  uint64_t GetTotalTxBytes (void) const;

private:
  virtual void StartApplication (void);
  virtual void StopApplication (void);

  /**
   * Schedule when the next packet will be sent.
   */
  void ScheduleTx (void);
  void SendPacket (void);

  Ptr<Socket>     m_socket;
  Address         m_peer;
  uint32_t        m_packetSize;
  DataRate        m_dataRate;
  EventId         m_sendEvent;
  bool            m_running;
  uint64_t        m_packetsSent;
  uint64_t        m_totBytes;     //!< Total bytes sent so far.
  bool            m_connected;    //!< True if connected.
  bool            m_bulk;         //!< True if Bulk, otherwise OnOff.

private:
  void ConnectionSucceeded (Ptr<Socket> socket);
  void ConnectionFailed (Ptr<Socket> socket);
  void DataSend (Ptr<Socket>, uint32_t); // for socket's SetSendCallback

};

TcpSendApplication::TcpSendApplication ()
  : m_socket (0),
    m_peer (),
    m_packetSize (0),
    m_dataRate (0),
    m_sendEvent (),
    m_running (false),
    m_packetsSent (0),
    m_totBytes (0),
    m_connected (false),
    m_bulk (true)
{
}

TcpSendApplication::~TcpSendApplication ()
{
  m_socket = 0;
}

void
TcpSendApplication::Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, DataRate dataRate, bool isBulk)
{
  m_socket = socket;
  m_peer = address;
  m_packetSize = packetSize;
  m_dataRate = dataRate;
  m_bulk = isBulk;
}

void
TcpSendApplication::StartApplication (void)
{
  m_running = true;
  // Make sure the socket is created
  if (m_socket)
    {
      // Fatal error if socket type is not NS3_SOCK_STREAM or NS3_SOCK_SEQPACKET
      if (m_socket->GetSocketType () != Socket::NS3_SOCK_STREAM &&
          m_socket->GetSocketType () != Socket::NS3_SOCK_SEQPACKET)
        {
          NS_FATAL_ERROR ("Using BulkSend with an incompatible socket type. "
                          "BulkSend requires SOCK_STREAM or SOCK_SEQPACKET. "
                          "In other words, use TCP instead of UDP.");
        }

      if (Inet6SocketAddress::IsMatchingType (m_peer))
        {
          m_socket->Bind6 ();
        }
      else if (InetSocketAddress::IsMatchingType (m_peer))
        {
          m_socket->Bind ();
        }

      m_socket->Connect (m_peer);
      m_socket->ShutdownRecv ();
      m_socket->SetConnectCallback (
        MakeCallback (&TcpSendApplication::ConnectionSucceeded, this),
        MakeCallback (&TcpSendApplication::ConnectionFailed, this));
      m_socket->SetSendCallback (
        MakeCallback (&TcpSendApplication::DataSend, this));
    }
  if (m_connected)
    {
      SendPacket ();
    }
}

void
TcpSendApplication::StopApplication (void)
{
  m_running = false;
  if (m_sendEvent.IsRunning ())
    {
      Simulator::Cancel (m_sendEvent);
    }

  if (m_socket)
    {
      m_socket->Close ();
      m_connected = false;
    }
  else
    {
      NS_LOG_WARN ("TcpSendApplication found null socket to close in StopApplication");
    }
}

void
TcpSendApplication::SendPacket (void)
{
  NS_LOG_FUNCTION (this);
  Ptr<Packet> packet = Create<Packet> (m_packetSize);
  TimestampTag timestamp;
  timestamp.SetTimestamp (Simulator::Now ());
  packet->AddByteTag (timestamp);

  if (m_bulk)
    {
      while (true)
        {
          NS_LOG_LOGIC ("sending packet at " << Simulator::Now ());
          int actual = m_socket->Send (packet);
          if (actual > 0)
            {
              m_totBytes += actual;
              m_packetsSent++;
            }
          // We exit this loop when actual < toSend as the send side
          // buffer is full. The "DataSent" callback will pop when
          // some buffer space has freed ip.
          if ((unsigned)actual != m_packetSize)
            {
              break;
            }
        }
    }
  else
    {
      m_socket->Send (packet);
      ScheduleTx ();
    }
}

void
TcpSendApplication::ScheduleTx (void)
{
  if (m_running)
    {
      Time tNext (Seconds (m_packetSize * 8 / static_cast<double> (m_dataRate.GetBitRate ())));
      m_sendEvent = Simulator::Schedule (tNext, &TcpSendApplication::SendPacket, this);
    }
}

void
TcpSendApplication::ConnectionSucceeded (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  NS_LOG_LOGIC ("TcpSendApplication Connection succeeded");
  m_connected = true;
  SendPacket ();
}

void
TcpSendApplication::ConnectionFailed (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  NS_LOG_LOGIC ("TcpSendApplication, Connection Failed");
}

void
TcpSendApplication::DataSend (Ptr<Socket>, uint32_t)
{
  NS_LOG_FUNCTION (this);
  if (m_connected  && m_bulk)
    { // Only send new data if the connection has completed
      SendPacket ();
    }
}

uint64_t
TcpSendApplication::GetTotalTxPackets (void) const
{
  return m_packetsSent;
}

uint64_t
TcpSendApplication::GetTotalTxBytes (void) const
{
  return m_totBytes;
}

/**
 * Callback method to log changes of the congestion window.
 */
void
CwndChange (Ptr<OutputStreamWrapper> file, const uint32_t oldCwnd, const uint32_t newCwnd)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newCwnd << endl;
}

/**
* Callback method to log changes of the receive window.
*/
void
RwndChange (Ptr<OutputStreamWrapper> file, const uint32_t oldRwnd, const uint32_t newRwnd)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newRwnd << endl;
}

/**
 * Callback method to log changes of the round trip time.
 */
void
RttChange (Ptr<OutputStreamWrapper> file, const Time oldRtt, const Time newRtt)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newRtt.GetNanoSeconds () << endl;
}

/**
 * Callback method to log changes of the TcpTxBufferSize.
 */
void
TcpBufferSizeChange (Ptr<OutputStreamWrapper> file, uint32_t oldValue, uint32_t newValue)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newValue << endl;
}

/**
 * Callback method to log the number of packets in the Wifi MAC Queue.
 */
void
QueueOccupancyChange (Ptr<OutputStreamWrapper> file, uint32_t oldValue, uint32_t newValue)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newValue << endl;
}

/**
 * Callback method to log number of Packets in TC Queue.
 */
void
TcPacketsInQueueTrace (Ptr<OutputStreamWrapper> file, uint32_t oldValue, uint32_t newValue)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newValue << endl;
}

int
main (int argc, char *argv[])
{
  string applicationType = "bulk";              /* Type of the Tx application */
  uint32_t flows = 1;                           /* The number of TCP/UDP flows. */
  string socketType = "ns3::TcpSocketFactory";  /* Socket Type (TCP/UDP) */
  uint32_t packetSize = 1448;                   /* Application payload size in bytes. */
  string dataRate = "300Mbps";                  /* Application data rate. */
  string tcpVariant = "NewReno";                /* TCP Variant Type. */
  uint32_t bufferSize = 131072;                 /* TCP Send/Receive Buffer Size. */
  uint32_t maxPackets = 0;                      /* Maximum Number of Packets */
  bool trafficControl = false;                  /* Whether to isntall Traffic Control layer */
  uint32_t msduAggregationSize = 0;             /* The maximum aggregation size for A-MSDU in Bytes. */
  uint32_t mpduAggregationSize = 65535;         /* The maximum aggregation size for A-MSPU in Bytes. */
  uint32_t queueMaxDelay = 500;                 /* The maximum allowable delay for a packet to reside in the Queue. */
  uint32_t queueSize = 1000;                    /* Wifi MAC Queue Size. */
  string phyMode = "DMG_MCS12";                 /* Type of the Physical Layer. */
  uint64_t backboneDelay = 20;                  /* The delay in the backbone network in ms */
  std::string tracesPath = "/";                 /* The location where to save traces. */
  bool verbose = false;                         /* Print Logging Information. */
  double simulationTime = 10;                   /* Simulation time in seconds. */
  bool pcapTracing = false;                     /* PCAP Tracing is enabled or not. */
  std::map<std::string, std::string> tcpVariants; /* List of the tcp Variants */

  /** TCP Variants **/
  tcpVariants.insert (std::make_pair ("NewReno",       "ns3::TcpNewReno"));
  tcpVariants.insert (std::make_pair ("Hybla",         "ns3::TcpHybla"));
  tcpVariants.insert (std::make_pair ("HighSpeed",     "ns3::TcpHighSpeed"));
  tcpVariants.insert (std::make_pair ("Vegas",         "ns3::TcpVegas"));
  tcpVariants.insert (std::make_pair ("Scalable",      "ns3::TcpScalable"));
  tcpVariants.insert (std::make_pair ("Veno",          "ns3::TcpVeno"));
  tcpVariants.insert (std::make_pair ("Bic",           "ns3::TcpBic"));
  tcpVariants.insert (std::make_pair ("Westwood",      "ns3::TcpWestwood"));
  tcpVariants.insert (std::make_pair ("WestwoodPlus",  "ns3::TcpWestwoodPlus"));

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue ("applicationType", "Type of the Tx Application: onoff or bulk", applicationType);
  cmd.AddValue ("flows", "The number of TCP/UDP flows", flows);
  cmd.AddValue ("packetSize", "Application packet size in bytes", packetSize);
  cmd.AddValue ("dataRate", "Application data rate", dataRate);
  cmd.AddValue ("maxPackets", "Maximum number of packets to send", maxPackets);
  cmd.AddValue ("tcpVariant", "Transport protocol to use: TcpTahoe, TcpReno, TcpNewReno, TcpWestwood, TcpWestwoodPlus", tcpVariant);
  cmd.AddValue ("socketType", "Type of the Socket (ns3::TcpSocketFactory, ns3::UdpSocketFactory)", socketType);
  cmd.AddValue ("bufferSize", "TCP Buffer Size (Send/Receive) in Bytes", bufferSize);
  cmd.AddValue ("trafficControl", "Add traffic control layer (QueueDisc)", trafficControl);
  cmd.AddValue ("msduAggregation", "The maximum aggregation size for A-MSDU in Bytes", msduAggregationSize);
  cmd.AddValue ("mpduAggregation", "The maximum aggregation size for A-MPDU in Bytes", mpduAggregationSize);
  cmd.AddValue ("queueMaxDelay", "The maximum allowable delay in MilliSeconds for a packet to reside in the Queue", queueMaxDelay);
  cmd.AddValue ("queueSize", "The maximum size of the Wifi and P2P MAC Queues", queueSize);
  cmd.AddValue ("phyMode", "802.11ad PHY Mode", phyMode);
  cmd.AddValue ("delay", "Backbone Network delay in ms", backboneDelay);
  cmd.AddValue ("verbose", "Turn on all WifiNetDevice log components", verbose);
  cmd.AddValue ("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue ("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue ("tracesPath", "The path where to store traces under Traces Folder", tracesPath);
  cmd.Parse (argc, argv);

  /* Global params: no fragmentation, no RTS/CTS, fixed rate for all packets */
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("999999"));
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("999999"));
  Config::SetDefault ("ns3::QueueBase::MaxPackets", UintegerValue (queueSize));

  /*** Configure TCP Options ***/
  /* Select TCP variant */
  std::map<std::string, std::string>::const_iterator iter = tcpVariants.find (tcpVariant);
  NS_ASSERT_MSG (iter != tcpVariants.end (), "Cannot find Tcp Variant");
  TypeId tid = TypeId::LookupByName (iter->second);
  Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (tid));
  if (tcpVariant.compare ("Westwood") == 0)
    {
      Config::SetDefault ("ns3::TcpWestwood::ProtocolType", EnumValue (TcpWestwood::WESTWOOD));
      Config::SetDefault ("ns3::TcpWestwood::FilterType", EnumValue (TcpWestwood::TUSTIN));
    }
  else if (tcpVariant.compare ("WestwoodPlus") == 0)
    {
      Config::SetDefault ("ns3::TcpWestwood::ProtocolType", EnumValue (TcpWestwood::WESTWOODPLUS));
      Config::SetDefault ("ns3::TcpWestwood::FilterType", EnumValue (TcpWestwood::TUSTIN));
    }

  /* Configure TCP Segment Size */
  Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (packetSize));
  Config::SetDefault ("ns3::TcpSocket::SndBufSize", UintegerValue (bufferSize));
  Config::SetDefault ("ns3::TcpSocket::RcvBufSize", UintegerValue (bufferSize));

  /**** DmgWifiHelper is a meta-helper ****/
  DmgWifiHelper wifi;

  /* Basic setup */
  wifi.SetStandard (WIFI_PHY_STANDARD_80211ad);

  /* Turn on logging */
  if (verbose)
    {
      wifi.EnableLogComponents ();
      LogComponentEnable ("EvaluateTalonBackbone", LOG_LEVEL_ALL);
    }

  /**** Set up Channel ****/
  DmgWifiChannelHelper wifiChannel ;
  /* Simple propagation delay model */
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel", "Frequency", DoubleValue (60.48e9));

  /**** Setup physical layer ****/
  DmgWifiPhyHelper wifiPhy = DmgWifiPhyHelper::Default ();
  /* Nodes will be added to the channel we set up earlier */
  wifiPhy.SetChannel (wifiChannel.Create ());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wifiPhy.Set ("TxPowerStart", DoubleValue (10.0));
  wifiPhy.Set ("TxPowerEnd", DoubleValue (10.0));
  wifiPhy.Set ("TxPowerLevels", UintegerValue (1));
  /* The TALON Router does not support OFDM */
  wifiPhy.Set ("SupportOfdmPhy", BooleanValue (false));
  /* Set operating channel */
  wifiPhy.Set ("ChannelNumber", UintegerValue (2));
  /* Sensitivity model includes implementation loss and noise figure */
  wifiPhy.Set ("CcaMode1Threshold", DoubleValue (-79));
  wifiPhy.Set ("EnergyDetectionThreshold", DoubleValue (-79 + 3));
  /* Set default algorithm for all nodes to be constant rate */
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "ControlMode", StringValue (phyMode),
                                                                "DataMode", StringValue (phyMode));

  /* Make two nodes and set them up with the PHY and the MAC */
  NodeContainer nodes;
  nodes.Create (3);
  serverNode = nodes.Get (0);
  apWifiNode = nodes.Get (1);
  staWifiNode = nodes.Get (2);

  /* Create Backbone network */
  PointToPointHelper p2pHelper;
  p2pHelper.SetDeviceAttribute ("DataRate", StringValue ("10Gbps"));
  p2pHelper.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (backboneDelay)));
  p2pHelper.SetQueue ("ns3::DropTailQueue", "MaxPackets", UintegerValue (queueSize));

  NetDeviceContainer serverDevices;
  serverDevices = p2pHelper.Install (serverNode, apWifiNode);

  /* Add a DMG upper mac */
  DmgWifiMacHelper wifiMac = DmgWifiMacHelper::Default ();

  Ssid ssid = Ssid ("Throughput");
  wifiMac.SetType ("ns3::DmgApWifiMac",
                   "Ssid", SsidValue(ssid),
                   "BE_MaxAmpduSize", UintegerValue (mpduAggregationSize),
                   "BE_MaxAmsduSize", UintegerValue (msduAggregationSize),
                   "SSSlotsPerABFT", UintegerValue (1), "SSFramesPerSlot", UintegerValue (16),
                   "BeaconInterval", TimeValue (MicroSeconds (102400)),
                   "ATIPresent", BooleanValue (false));

  /* Set Numerical Codebook for the AP */
  wifi.SetCodebook ("ns3::CodebookNumerical",
                    "FileName", StringValue ("codebook_ap.txt"));

  NetDeviceContainer apDevice;
  apDevice = wifi.Install (wifiPhy, wifiMac, apWifiNode);

  /* Set Numerical Codebook for the STA */
  wifi.SetCodebook ("ns3::CodebookNumerical",
                    "FileName", StringValue ("codebook_sta.txt"));

  wifiMac.SetType ("ns3::DmgStaWifiMac",
                   "Ssid", SsidValue (ssid), "ActiveProbing", BooleanValue (false),
                   "BE_MaxAmpduSize", UintegerValue (mpduAggregationSize),
                   "BE_MaxAmsduSize", UintegerValue (msduAggregationSize));

  NetDeviceContainer staDevice;
  staDevice = wifi.Install (wifiPhy, wifiMac, staWifiNode);

  /* Connect Wifi MAC Queue Occupancy */
  AsciiTraceHelper asciiTraceHelper;
  Ptr<OutputStreamWrapper> queueOccupanyStream;
  /* Trace DMG STA MAC Queue Changes */
  queueOccupanyStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "StationMacQueueOccupany.txt");
  Config::ConnectWithoutContext ("/NodeList/2/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/BE_EdcaTxopN/Queue/OccupancyChanged",
                                 MakeBoundCallback (&QueueOccupancyChange, queueOccupanyStream));
  /* Trace DMG PCP/AP MAC Queue Changes */
  queueOccupanyStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "AccessPointMacQueueOccupany.txt");
  Config::ConnectWithoutContext ("/NodeList/1/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/BE_EdcaTxopN/Queue/OccupancyChanged",
                                 MakeBoundCallback (&QueueOccupancyChange, queueOccupanyStream));

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0));	/* DMG PCP/AP */
  positionAlloc->Add (Vector (1.0, 0.0, 0.0));  /* DMG STA */

  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (apWifiNode);
  mobility.Install (staWifiNode);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install (nodes);

  /* Install Traffic Control Layer if required */
  if (trafficControl)
    {
      TrafficControlHelper tch;
      uint16_t handle = tch.SetRootQueueDisc ("ns3::RedQueueDisc");
      // Add the internal queue used by Red
      tch.AddInternalQueues (handle, 1, "ns3::DropTailQueue", "MaxPackets", UintegerValue (10000));
      QueueDiscContainer qdiscs = tch.Install (serverDevices);

      Ptr<OutputStreamWrapper> tcPacketsStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "TcPackets.txt");
      Ptr<QueueDisc> q = qdiscs.Get (1);
      q->TraceConnectWithoutContext ("PacketsInQueue", MakeBoundCallback (&TcPacketsInQueueTrace, tcPacketsStream));
    }

  Ipv4AddressHelper address;
  address.SetBase ("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer serverInterface;
  serverInterface = address.Assign (serverDevices);
  address.NewNetwork ();
  Ipv4InterfaceContainer apInterface;
  apInterface = address.Assign (apDevice);
  Ipv4InterfaceContainer staInterface;
  staInterface = address.Assign (staDevice);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  /* We do not want any ARP packets */
  PopulateArpCache ();

  /* Install Simple TCP/UDP Server on the Server */
  PacketSinkHelper sinkHelper (socketType, InetSocketAddress (Ipv4Address::GetAny (), 9999));
  ApplicationContainer sinkApp = sinkHelper.Install (serverNode);
  packetSink = StaticCast<PacketSink> (sinkApp.Get (0));
  sinkApp.Start (Seconds (0.0));

  /* Install TCP/UDP Transmitter on the DMG STA */
  Address dest (InetSocketAddress (serverInterface.GetAddress (0), 9999));
  ApplicationContainer srcApp;
  TypeId socketTid = TypeId::LookupByName (socketType);
  if (applicationType == "bulk")
    {
      /* Random variable for the initialization of the TCP/UDP connection */
      Ptr<UniformRandomVariable> variable = CreateObject<UniformRandomVariable> ();
      variable->SetAttribute ("Min", DoubleValue (0));
      variable->SetAttribute ("Max", DoubleValue (20));

      /* Generate #tcpFlows */
      Ptr<TcpSocketBase> tcpSocket[flows];
      Ptr<TcpTxBuffer> txBuffer[flows];
      Ptr<TcpSendApplication> app[flows];

      for (uint32_t i = 0; i < flows; i++)
        {
          ostringstream flowId;
          flowId << i + 1;
          tcpSocket[i] = StaticCast<TcpSocketBase> (Socket::CreateSocket (staWifiNode, socketTid));
          txBuffer[i] = tcpSocket[i]->GetTxBuffer ();
          app[i] = CreateObject<TcpSendApplication> ();

          app[i]->Setup (tcpSocket[i], dest, packetSize, DataRate (dataRate), true);
          staWifiNode->AddApplication (app[i]);
          app[i]->SetStartTime (Seconds (1.0) + MilliSeconds (variable->GetInteger ()));
          app[i]->SetStopTime (Seconds (simulationTime));
          srcApp.Add (app[i]);

          /* Connect TCP Socket Traces */
          AsciiTraceHelper asciiTraceHelper;
          Ptr<OutputStreamWrapper> cwndStream
              = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "cwnd_" + flowId.str () + ".txt");
          Ptr<OutputStreamWrapper> rwndStream
              = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "rwnd_" + flowId.str () + ".txt");
          Ptr<OutputStreamWrapper> rttStream
              = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "rtt_" + flowId.str () + ".txt");
          Ptr<OutputStreamWrapper> bufferSizeStream
              = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "tcpBufferSize_" + flowId.str () + ".txt");

          tcpSocket[i]->TraceConnectWithoutContext ("CongestionWindow", MakeBoundCallback (&CwndChange, cwndStream));
          tcpSocket[i]->TraceConnectWithoutContext ("RWND", MakeBoundCallback (&RwndChange, rwndStream));
          tcpSocket[i]->TraceConnectWithoutContext ("RTT", MakeBoundCallback (&RttChange, rttStream));
          txBuffer[i]->TraceConnectWithoutContext ("SizeChanged", MakeBoundCallback (&TcpBufferSizeChange, bufferSizeStream));
        }
    }
  else if (applicationType == "onoff")
    {
      Ptr<TcpSocketBase> tcpSocket = StaticCast<TcpSocketBase> (Socket::CreateSocket (staWifiNode, socketTid));
      Ptr<TcpTxBuffer> txBuffer = tcpSocket->GetTxBuffer ();
      Ptr<TcpSendApplication> app = CreateObject<TcpSendApplication> ();

      app->Setup (tcpSocket, dest, packetSize, DataRate (dataRate), false);
      staWifiNode->AddApplication (app);
      app->SetStartTime (Seconds (1.0));
      app->SetStopTime (Seconds (simulationTime));
      srcApp.Add (app);

      /* Connect TCP Socket Traces */
      Ptr<OutputStreamWrapper> cwndStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "cwnd.txt");
      Ptr<OutputStreamWrapper> rwndStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "rwnd.txt");
      Ptr<OutputStreamWrapper> rttStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "rtt.txt");
      Ptr<OutputStreamWrapper> bufferSizeStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "bufferSize.txt");

      tcpSocket->TraceConnectWithoutContext ("CongestionWindow", MakeBoundCallback (&CwndChange, cwndStream));
      tcpSocket->TraceConnectWithoutContext ("RWND", MakeBoundCallback (&RwndChange, rwndStream));
      tcpSocket->TraceConnectWithoutContext ("RTT", MakeBoundCallback (&RttChange, rttStream));
      txBuffer->TraceConnectWithoutContext ("SizeChanged", MakeBoundCallback (&TcpBufferSizeChange, bufferSizeStream));
    }

  /* Print Traces */
  if (pcapTracing)
    {
      p2pHelper.SetSnapshotLength (120);
      p2pHelper.EnablePcap ("Traces/Server", serverDevices.Get (0));
      wifiPhy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);
      wifiPhy.SetSnapshotLength (120);
      wifiPhy.EnablePcap ("Traces/AccessPoint", apDevice, false);
      wifiPhy.EnablePcap ("Traces/Station", staDevice, false);
    }

  /* Install FlowMonitor on all nodes */
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll ();

  /* Print Output*/
  std::cout << std::left << std::setw (12) << "Time [s]"
            << std::left << std::setw (12) << "Throughput [Mbps]" << std::endl;

  /* Schedule Throughput Calulcations */
  Simulator::Schedule (Seconds (1.1), &CalculateThroughput);

  Simulator::Stop (Seconds (simulationTime + 0.101));
  Simulator::Run ();
  Simulator::Destroy ();

  /* Print per flow statistics */
  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats ();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
      Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
      std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")" << std::endl;;
      std::cout << "  Tx Packets: " << i->second.txPackets << std::endl;
      std::cout << "  Tx Bytes:   " << i->second.txBytes << std::endl;
      std::cout << "  TxOffered:  " << i->second.txBytes * 8.0 / ((simulationTime - 1) * 1e6)  << " Mbps" << std::endl;;
      std::cout << "  Rx Packets: " << i->second.rxPackets << std::endl;;
      std::cout << "  Rx Bytes:   " << i->second.rxBytes << std::endl;
      std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / ((simulationTime - 1) * 1e6)  << " Mbps" << std::endl;;
    }

  /* Print Application Layer Results Summary */
  Ptr<TcpSendApplication> app;
  std::cout << "\nApplication Layer Statistics:" << std::endl;
  for (uint32_t i = 0; i < flows; i++)
    {
      app = StaticCast<TcpSendApplication> (srcApp.Get (i));
      std::cout << "  Tx Packets: " << app->GetTotalTxPackets () << std::endl;
      std::cout << "  Tx Bytes:   " << app->GetTotalTxBytes () << std::endl;
      std::cout << "  Rx Packets: " << packetSink->GetTotalReceivedPackets () << std::endl;
      std::cout << "  Rx Bytes:   " << packetSink->GetTotalRx () << std::endl;
      std::cout << "  Throughput: " << packetSink->GetTotalRx () * 8.0 / ((simulationTime - 1) * 1e6) << " Mbps" << std::endl;
    }

  return 0;
}
