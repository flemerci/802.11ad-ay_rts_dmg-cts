/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "common-functions.h"
#include <string>
#include <iomanip>

/**
 * Simulation Objective:
 * This script is used to evaluate the performance of the CSMA/CA access scheme in deafness scenario.
 *
 * Network Topology:
 * The scenario consists of two DMG STAs and a single DMG PCP/AP.
 *
 *
 *                            DMG PCP/AP (0,+3)
 *
 *
 *
 *      DMG STA (-3,0)                               DMG STA (+3,0)
 *
 *
 * Simulation Description:
 * The two stations start TCP flows towards the DMG PCP/AP. Each STA uses a sector with beamwidth of 45 degree.
 * In theory, when each station receives packet from peer station it should drop it due to low power.
 *
 * Running Simulation:
 * To evaluate CSMA/CA channel access scheme:
 * ./waf --run "evaluate_talon_throughput --simulationTime=10 --pcap=true"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station.
 * 2. Average throughput per node and total throughput
 * 3. Flow monitor statistics.
 */

NS_LOG_COMPONENT_DEFINE ("EvaluateDeafnessScenario");

using namespace ns3;
using namespace std;

#define MAX_NODES 2
uint16_t numNodes = 2;

/**  Application Variables **/
uint64_t totalRx[MAX_NODES] = {0};
double throughput[MAX_NODES] = {0};
ApplicationContainer sinkApps;

/* Statistics */
struct Statistics : public SimpleRefCount<Statistics> {
  uint64_t macTxDataFailed = 0;
  uint64_t phyTxPackets = 0;
  uint64_t phyRxPackets = 0;
  uint64_t phyRxDroppedPackets = 0;
};

typedef std::map<Ptr<WifiMac>, Ptr<Statistics> > GlobalStatistics;
typedef GlobalStatistics::const_iterator GlobalStatisticsCI;
GlobalStatistics globalStatistics;

double
CalculateSingleStreamThroughput (Ptr<PacketSink> sink, uint64_t &lastTotalRx, double &averageThroughput)
{
  double thr = (sink->GetTotalRx() - lastTotalRx) * (double) 8/1e5;     /* Convert Application RX Packets to MBits. */
  lastTotalRx = sink->GetTotalRx ();
  averageThroughput += thr;
  return thr;
}

void
CalculateThroughput (void)
{
  double totalThr = 0;
  double thr;
  std::cout << std::left << std::setw (12)<< Simulator::Now ().GetSeconds ();
  for (uint8_t i = 0; i < numNodes; i++)
    {
      Ptr<PacketSink> packetSink = StaticCast<PacketSink> (sinkApps.Get (i));
      thr = CalculateSingleStreamThroughput (packetSink, totalRx[i], throughput[i]);
      totalThr += thr;
      std::cout << std::left << std::setw (12) << thr;
    }
  std::cout << std::left << std::setw (12) << totalThr << std::endl;
  Simulator::Schedule (MilliSeconds (100), &CalculateThroughput);
}

void
MacTxDataFailed (Ptr<WifiMac> netDevice, Mac48Address)
{
  globalStatistics[netDevice]->macTxDataFailed++;
}

void
PhyTxEnd (Ptr<WifiMac> netDevice, Ptr<const Packet>)
{
  globalStatistics[netDevice]->phyTxPackets++;
}

void
PhyRxDrop (Ptr<WifiMac> netDevice, Ptr<const Packet>)
{
  globalStatistics[netDevice]->phyRxDroppedPackets++;
}

void
PhyRxEnd (Ptr<WifiMac> netDevice, Ptr<const Packet>)
{
  globalStatistics[netDevice]->phyRxPackets++;
}

void
StationAssoicated (Ptr<DmgWifiMac> staWifiMac, Mac48Address address, uint16_t aid)
{
  std::cout << "DMG STA " << staWifiMac->GetAddress () << " associated with DMG AP " << address
            << ", Association ID (AID) = " << aid << std::endl;
}

void
SLSCompleted (Ptr<DmgWifiMac> wifiMac, Mac48Address address, ChannelAccessPeriod accessPeriod,
              BeamformingDirection beamformingDirection, bool isInitiatorTxss, bool isResponderTxss,
              SECTOR_ID sectorId, ANTENNA_ID antennaId)
{
  std::cout << "DMG STA " << wifiMac->GetAddress () << " completed SLS phase with DMG STA " << address << std::endl;
  std::cout << "Best Tx Antenna Configuration: SectorID=" << uint16_t (sectorId) << ", AntennaID=" << uint16_t (antennaId) << std::endl;
}

int
main (int argc, char *argv[])
{
  string applicationType = "bulk";              /* Type of the Tx application */
  string socketType = "ns3::TcpSocketFactory";  /* Socket Type (TCP/UDP) */
  uint32_t packetSize = 1448;                   /* Application payload size in bytes. */
  string dataRate = "300Mbps";                  /* Application data rate. */
  string tcpVariant = "NewReno";                /* TCP Variant Type. */
  uint32_t bufferSize = 131072;                 /* TCP Send/Receive Buffer Size. */
  uint32_t maxPackets = 0;                      /* Maximum Number of Packets */
  uint32_t msduAggregationSize = 0;             /* The maximum aggregation size for A-MSDU in Bytes. */
  uint32_t mpduAggregationSize = 65535;         /* The maximum aggregation size for A-MSPU in Bytes. */
  uint32_t queueSize = 1000;                    /* Wifi MAC Queue Size. */
  string phyMode = "DMG_MCS12";                 /* Type of the Physical Layer. */
  bool singleNode = false;                      /* Allow single node to transmit. */
  uint32_t snapShotLength = std::numeric_limits<uint32_t>::max (); /* The maximum PCAP Snapshot Length */
  bool verbose = false;                         /* Print Logging Information. */
  double simulationTime = 10;                   /* Simulation time in seconds. */
  bool pcapTracing = false;                     /* PCAP Tracing is enabled or not. */
  std::map<std::string, std::string> tcpVariants; /* List of the tcp Variants */
  bool deafness = true;                         /* Flag to indicate whether we want to simulate deafness scenario or not. */

  /** TCP Variants **/
  tcpVariants.insert (std::make_pair ("NewReno",       "ns3::TcpNewReno"));
  tcpVariants.insert (std::make_pair ("Hybla",         "ns3::TcpHybla"));
  tcpVariants.insert (std::make_pair ("HighSpeed",     "ns3::TcpHighSpeed"));
  tcpVariants.insert (std::make_pair ("Vegas",         "ns3::TcpVegas"));
  tcpVariants.insert (std::make_pair ("Scalable",      "ns3::TcpScalable"));
  tcpVariants.insert (std::make_pair ("Veno",          "ns3::TcpVeno"));
  tcpVariants.insert (std::make_pair ("Bic",           "ns3::TcpBic"));
  tcpVariants.insert (std::make_pair ("Westwood",      "ns3::TcpWestwood"));
  tcpVariants.insert (std::make_pair ("WestwoodPlus",  "ns3::TcpWestwoodPlus"));

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue ("applicationType", "Type of the Tx Application: onoff or bulk", applicationType);
  cmd.AddValue ("packetSize", "Application packet size in bytes", packetSize);
  cmd.AddValue ("dataRate", "Application data rate", dataRate);
  cmd.AddValue ("maxPackets", "Maximum number of packets to send", maxPackets);
  cmd.AddValue ("tcpVariant", "Transport protocol to use: TcpTahoe, TcpReno, TcpNewReno, TcpWestwood, TcpWestwoodPlus", tcpVariant);
  cmd.AddValue ("socketType", "Type of the Socket (ns3::TcpSocketFactory, ns3::UdpSocketFactory)", socketType);
  cmd.AddValue ("bufferSize", "TCP Buffer Size (Send/Receive) in Bytes", bufferSize);
  cmd.AddValue ("msduAggregation", "The maximum aggregation size for A-MSDU in Bytes", msduAggregationSize);
  cmd.AddValue ("mpduAggregation", "The maximum aggregation size for A-MPDU in Bytes", mpduAggregationSize);
  cmd.AddValue ("queueSize", "The maximum size of the Wifi MAC Queue", queueSize);
  cmd.AddValue ("phyMode", "802.11ad PHY Mode", phyMode);
  cmd.AddValue ("singleNode", "Allow single node to transmit", singleNode);
  cmd.AddValue ("deafness", "Flag to indicate whether we want to simulate deafness scenario or not", deafness);
  cmd.AddValue ("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue ("snapShotLength", "The maximum PCAP Snapshot Length", snapShotLength);
  cmd.AddValue ("verbose", "Turn on all WifiNetDevice log components", verbose);
  cmd.AddValue ("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.Parse (argc, argv);

  /* Global params: no fragmentation, no RTS/CTS, fixed rate for all packets */
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("999999"));
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("999999"));
  Config::SetDefault ("ns3::QueueBase::MaxPackets", UintegerValue (queueSize));

  /* Check how many nodes transmit */
  if (singleNode)
    {
      numNodes = 1;
    }

  /*** Configure TCP Options ***/
  /* Select TCP variant */
  std::map<std::string, std::string>::const_iterator iter = tcpVariants.find (tcpVariant);
  NS_ASSERT_MSG (iter != tcpVariants.end (), "Cannot find Tcp Variant");
  TypeId tid = TypeId::LookupByName (iter->second);
  Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (tid));
  if (tcpVariant.compare ("Westwood") == 0)
    {
      Config::SetDefault ("ns3::TcpWestwood::ProtocolType", EnumValue (TcpWestwood::WESTWOOD));
      Config::SetDefault ("ns3::TcpWestwood::FilterType", EnumValue (TcpWestwood::TUSTIN));
    }
  else if (tcpVariant.compare ("WestwoodPlus") == 0)
    {
      Config::SetDefault ("ns3::TcpWestwood::ProtocolType", EnumValue (TcpWestwood::WESTWOODPLUS));
      Config::SetDefault ("ns3::TcpWestwood::FilterType", EnumValue (TcpWestwood::TUSTIN));
    }

  /* Configure TCP Segment Size */
  Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (packetSize));
  Config::SetDefault ("ns3::TcpSocket::SndBufSize", UintegerValue (bufferSize));
  Config::SetDefault ("ns3::TcpSocket::RcvBufSize", UintegerValue (bufferSize));

  /**** DmgWifiHelper is a meta-helper ****/
  DmgWifiHelper wifi;

  /* Basic setup */
  wifi.SetStandard (WIFI_PHY_STANDARD_80211ad);

  /* Turn on logging */
  if (verbose)
    {
      wifi.EnableLogComponents ();
      LogComponentEnable ("EvaluateDeafnessScenario", LOG_LEVEL_ALL);
    }

  /**** Set up Channel ****/
  DmgWifiChannelHelper wifiChannel ;
  /* Simple propagation delay model */
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel", "Frequency", DoubleValue (60.48e9));

  /**** Setup physical layer ****/
  DmgWifiPhyHelper wifiPhy = DmgWifiPhyHelper::Default ();
  /* Nodes will be added to the channel we set up earlier */
  wifiPhy.SetChannel (wifiChannel.Create ());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wifiPhy.Set ("TxPowerStart", DoubleValue (8.0));
  wifiPhy.Set ("TxPowerEnd", DoubleValue (8.0));
  wifiPhy.Set ("TxPowerLevels", UintegerValue (1));
  /* Frame Capture Model */
  wifiPhy.Set ("FrameCaptureModel", StringValue ("ns3::SimpleFrameCaptureModel"));
  /* Set operating channel */
  wifiPhy.Set ("ChannelNumber", UintegerValue (2));
  /* Sensitivity model includes implementation loss and noise figure */
  wifiPhy.Set ("CcaMode1Threshold", DoubleValue (-79));             /* CCA-ED */
  wifiPhy.Set ("EnergyDetectionThreshold", DoubleValue (-79 + 3));  /* CCA-SD Signal Detection Threshold */
  /* Set default algorithm for all nodes to be constant rate */
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "ControlMode", StringValue (phyMode),
                                                                "DataMode", StringValue (phyMode));

  /* Make two nodes and set them up with the PHY and the MAC */
  NodeContainer apWifiNodes;
  apWifiNodes.Create (1);
  NodeContainer staWifiNodes;
  staWifiNodes.Create (MAX_NODES);
  NodeContainer wifiNodes (apWifiNodes, staWifiNodes);

  /* Add a DMG upper mac */
  DmgWifiMacHelper wifiMac = DmgWifiMacHelper::Default ();

  Ssid ssid = Ssid ("Deafness");
  wifiMac.SetType ("ns3::DmgApWifiMac",
                   "Ssid", SsidValue(ssid),
                   "BE_MaxAmpduSize", UintegerValue (mpduAggregationSize),
                   "BE_MaxAmsduSize", UintegerValue (msduAggregationSize),
                   "SSSlotsPerABFT", UintegerValue (8), "SSFramesPerSlot", UintegerValue (16),
                   "BeaconInterval", TimeValue (MicroSeconds (102400)),
                   "ATIPresent", BooleanValue (false));

  /* Set Analytical Codebook for the DMG Devices */
  wifi.SetCodebook ("ns3::CodebookAnalytical",
                    "CodebookType", EnumValue (SIMPLE_CODEBOOK),
                    "Antennas", UintegerValue (1),
                    "Sectors", UintegerValue (16));

  NetDeviceContainer apDevice;
  apDevice = wifi.Install (wifiPhy, wifiMac, apWifiNodes);

  wifiMac.SetType ("ns3::DmgStaWifiMac",
                   "Ssid", SsidValue (ssid), "ActiveProbing", BooleanValue (false),
                   "BE_MaxAmpduSize", UintegerValue (mpduAggregationSize),
                   "BE_MaxAmsduSize", UintegerValue (msduAggregationSize));

  NetDeviceContainer staDevices;
  staDevices = wifi.Install (wifiPhy, wifiMac, staWifiNodes);

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  if (deafness)
    {
      positionAlloc->Add (Vector (0.0, +0.5, 0.0));   /* DMG PCP/AP */
      positionAlloc->Add (Vector (-1.0, -0.5, 0.0));  /* DMG STA (1) */
      positionAlloc->Add (Vector (+1.0, -0.5, 0.0));  /* DMG STA (2) */
    }
  else
    {
//      positionAlloc->Add (Vector (0.0, 0.0, 0.0));   /* DMG PCP/AP */
//      positionAlloc->Add (Vector (-0.5, -0.1, 0.0));  /* DMG STA (1) */
//      positionAlloc->Add (Vector (-0.5, +0.1, 0.0));  /* DMG STA (2) */
      positionAlloc->Add (Vector (0.0, 0.0, 0.0));   /* DMG PCP/AP */
      positionAlloc->Add (Vector (-0.5, 0.0, 0.0));  /* DMG STA (1) */
      positionAlloc->Add (Vector (+0.5, 0.0, 0.0));  /* DMG STA (2) */
    }

  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install (wifiNodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer apInterface;
  apInterface = address.Assign (apDevice);
  Ipv4InterfaceContainer staInterfaces;
  staInterfaces = address.Assign (staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  /* We do not want any ARP packets */
  PopulateArpCache ();

  /* Create Applications */
  ApplicationContainer genApps;
  for (uint8_t i = 0; i < numNodes; i++)
    {
      /* Install Simple TCP/UDP Server on the DMG AP */
      PacketSinkHelper sinkHelper (socketType, InetSocketAddress (Ipv4Address::GetAny (), 9000 + i));
      sinkApps.Add (sinkHelper.Install (apWifiNodes));

      /* Install TCP/UDP Transmitter on the DMG STA */
      Address dest (InetSocketAddress (apInterface.GetAddress (0), 9000 + i));
      if (applicationType == "onoff")
        {
          OnOffHelper src (socketType, dest);
          src.SetAttribute ("MaxBytes", UintegerValue (maxPackets));
          src.SetAttribute ("PacketSize", UintegerValue (packetSize));
          src.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1e6]"));
          src.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
          src.SetAttribute ("DataRate", DataRateValue (DataRate (dataRate)));
          genApps.Add (src.Install (staWifiNodes.Get (i)));
        }
      else if (applicationType == "bulk")
        {
          BulkSendHelper src (socketType, dest);
          genApps.Add (src.Install (staWifiNodes.Get (i)));
        }
    }

  /* Schedule applications */
  sinkApps.Start (Seconds (0.0));
  genApps.Start (Seconds (1.0));
  genApps.Stop (Seconds (simulationTime));

  /* Print Traces */
  if (pcapTracing)
    {
      wifiPhy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);
      wifiPhy.SetSnapshotLength (snapShotLength);
      wifiPhy.EnablePcap ("Traces/AccessPoint", apDevice, false);
      wifiPhy.EnablePcap ("Traces/Station", staDevices, false);
    }

  /** Connect Traces **/
  NetDeviceContainer devices (apDevice, staDevices);
  for (uint i = 0; i < devices.GetN (); i++)
    {
      Ptr<WifiNetDevice> staWifiNetDevice;
      Ptr<WifiRemoteStationManager> staRemoteStationManager;
      Ptr<DmgStaWifiMac> staWifiMac;
      Ptr<DmgWifiPhy> staWifiPhy;
      Ptr<Statistics> stats = Create<Statistics> ();

      staWifiNetDevice = StaticCast<WifiNetDevice> (devices.Get (i));
      staWifiMac = StaticCast<DmgStaWifiMac> (staWifiNetDevice->GetMac ());
      staWifiPhy = StaticCast<DmgWifiPhy> (staWifiNetDevice->GetPhy ());
      staRemoteStationManager = staWifiNetDevice->GetRemoteStationManager ();

      if (i > 0)
        {
          staWifiMac->TraceConnectWithoutContext ("Assoc", MakeBoundCallback (&StationAssoicated, staWifiMac));

        }
      staWifiMac->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, staWifiMac));
      staWifiPhy->TraceConnectWithoutContext ("PhyTxEnd", MakeBoundCallback (&PhyTxEnd, staWifiMac));
      staWifiPhy->TraceConnectWithoutContext ("PhyRxEnd", MakeBoundCallback (&PhyRxEnd, staWifiMac));
      staWifiPhy->TraceConnectWithoutContext ("PhyRxDrop", MakeBoundCallback (&PhyRxDrop, staWifiMac));
      staRemoteStationManager->TraceConnectWithoutContext ("MacTxDataFailed", MakeBoundCallback (&MacTxDataFailed, staWifiMac));

      globalStatistics[staWifiMac] = stats;
    }

  /* Install FlowMonitor on all nodes */
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll ();

  /* Print Output */
  std::cout << "Application Throughput per Node [Mbps]" << std::endl;
  std::cout << std::left << std::setw (12) << "Time [s]";
  string columnName;
  for (uint8_t i = 0; i < numNodes; i++)
    {
      columnName = "Node (" + std::to_string (i + 1) + ")";
      std::cout << std::left << std::setw (12) << columnName;
    }
   std::cout << std::left << std::setw (12) << "Total" << std::endl;

  /* Schedule Throughput Calculations */
  Simulator::Schedule (Seconds (1.1), &CalculateThroughput);

  Simulator::Stop (Seconds (simulationTime + 0.101));
  Simulator::Run ();
  Simulator::Destroy ();

  /* Print per flow statistics */
  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats ();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
      Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
      std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")" << std::endl;;
      std::cout << "  Tx Packets: " << i->second.txPackets << std::endl;
      std::cout << "  Tx Bytes:   " << i->second.txBytes << std::endl;
      std::cout << "  TxOffered:  " << i->second.txBytes * 8.0 / ((simulationTime - 1) * 1e6)  << " Mbps" << std::endl;;
      std::cout << "  Rx Packets: " << i->second.rxPackets << std::endl;;
      std::cout << "  Rx Bytes:   " << i->second.rxBytes << std::endl;
      std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / ((simulationTime - 1) * 1e6)  << " Mbps" << std::endl;;
    }

  /* Print Application Layer Results Summary */
  std::cout << "\nApplication Layer Statistics:" << std::endl;
  Ptr<OnOffApplication> onoff;
  Ptr<BulkSendApplication> bulk;
  uint64_t txPackets;
  double totalRx = 0;
  for (uint32_t i = 0; i < numNodes; i++)
    {
      std::cout << "STA (" << i + 1 << "):" << std::endl;
      if (applicationType == "onoff")
        {
          onoff = StaticCast<OnOffApplication> (genApps.Get (i));
          txPackets = onoff->GetTotalTxPackets ();
          std::cout << "  Tx Packets: " << txPackets << std::endl;
          std::cout << "  Tx Bytes:   " << onoff->GetTotalTxBytes () << std::endl;
        }
      else
        {
          bulk = StaticCast<BulkSendApplication> (genApps.Get (i));
          txPackets = bulk->GetTotalTxPackets ();
          std::cout << "  Tx Packets: " << txPackets << std::endl;
          std::cout << "  Tx Bytes:   " << bulk->GetTotalTxBytes () << std::endl;
        }
      Ptr<PacketSink> packetSink = StaticCast<PacketSink> (sinkApps.Get (i));
      std::cout << "  Rx Packets: " << packetSink->GetTotalReceivedPackets () << std::endl;
      std::cout << "  Rx Bytes:   " << packetSink->GetTotalRx () << std::endl;
      std::cout << "  Dropped Packets:   " << txPackets - packetSink->GetTotalReceivedPackets () << std::endl;
      totalRx += packetSink->GetTotalRx ();
      std::cout << "  Throughput: " << packetSink->GetTotalRx () * 8.0 / ((simulationTime - 1) * 1e6) << " Mbps" << std::endl;
    }
  std::cout << "\nTotal Average Throughput: " << totalRx * 8.0 / ((simulationTime - 1) * 1e6) << " Mbps\n";

  for (GlobalStatisticsCI it = globalStatistics.begin (); it != globalStatistics.end (); it++)
    {
      std::cout << std::endl;
      std::cout << "*******************************************************" << std::endl;
      std::cout << "Station Statistics: " << it->first->GetAddress () << std::endl;
      std::cout << "*******************************************************" << std::endl;

      /* Print MAC Layer Statistics */
      std::cout << "\nMAC Layer Statistics:" << std::endl;;
      std::cout << "  Number of Failed Tx Data Packets:  " << it->second->macTxDataFailed << std::endl;

      /* Print PHY Layer Statistics */
      std::cout << "\nPHY Layer Statistics:" << std::endl;;
      std::cout << "  Number of Tx Packets:         " << it->second->phyTxPackets << std::endl;
      std::cout << "  Number of Rx Packets:         " << it->second->phyRxPackets << std::endl;
      std::cout << "  Number of Rx Dropped Packets: " << it->second->phyRxDroppedPackets << std::endl;
    }

  return 0;
}

