/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "common-functions.h"
#include "custom-tcp-application.h"
#include <iomanip>
#include <sstream>

/**
 * Simulation Objective:
 * This script is used to evaluate IEEE 802.11ad for Multi-BSS i.e. Multi-AP using custom TCP Send Application.
 *
 * Network Topology:
 * The scenario consists of variable number of parallel links. The distance between each DMG STA and its corresponding
 * DMG PCP/AP is 2 m. The distance between two adjacent BSSs is variable and is set by the user.
 *
 *          DMG AP_1  (0, +1)               DMG AP_2  (+1, +1)    ------   DMG AP_N  (+1, +1)
 *
 *                 ^                               ^                              ^
 *                 |                               |                              |
 *                 |       <--- distance --->      |              ------          |
 *                 |                               |                              |
 *
 *          DMG STA_1 (0, -1)               DMG STA_2 (+1, -1)    ------   DMG STA_2 (+1, -1)
 *
 *
 * Simulation Description:
 * Once a DMG STA is associated with its corresponding DMG PCP/ACP, it generates single TCP flow towards that DMG PCP/AP.
 * We use DMG Error Model generated using MATLAB WLAN Toolbox.
 *
 * Running Simulation:
 * To use this script simply type the following run command:
 * ./waf --run "evaluate_multi_bss_operaetion_adv"
 *
 * To change the number of parallel links, we execute the following script:
 * ./waf --run "evaluate_multi_bss_operaetion_adv --parallelLinks=5"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station.
 * 2. TCP level traces (CWND and RTT).
 * 3. Traffic Control Layer Queue changes.
 * 4. WiFi MAC Level Queue changes.
 */

NS_LOG_COMPONENT_DEFINE ("EvaluateMultiBSSOperation");

using namespace ns3;
using namespace std;

/* Type definitions */
struct CommunicationPair {
  Ptr<TcpSendApplication> srcApp;
  Ptr<PacketSink> packetSink;
  uint64_t totalRx = 0;
  double throughput = 0;
  Time startTime;
};

typedef std::map<Ptr<Node>, CommunicationPair> CommunicationPairList;
typedef CommunicationPairList::iterator CommunicationPairList_I;
typedef CommunicationPairList::const_iterator CommunicationPairList_CI;

/** Simulation Arguments **/
bool csv = false;                               /* Enable CSV output. */

/**  Applications **/
CommunicationPairList communicationPairList;    /* List of communicating devices. */

template <typename T>
std::string to_string_with_precision (const T a_value, const int n = 6)
{
    std::ostringstream out;
    out.precision (n);
    out << std::fixed << a_value;
    return out.str ();
}

double
CalculateSingleStreamThroughput (Ptr<PacketSink> sink, uint64_t &lastTotalRx, double &averageThroughput)
{
  double thr = (sink->GetTotalRx() - lastTotalRx) * (double) 8/1e5;     /* Convert Application RX Packets to MBits. */
  lastTotalRx = sink->GetTotalRx ();
  averageThroughput += thr;
  return thr;
}

void
CalculateThroughput (void)
{
  double totalThr = 0;
  double thr;
  if (!csv)
    {
      string duration = to_string_with_precision<double> (Simulator::Now ().GetSeconds () - 0.1, 1)
                      + " - " + to_string_with_precision<double> (Simulator::Now ().GetSeconds (), 1);
      std::cout << std::left << std::setw (12) << duration;
      for (CommunicationPairList_I it = communicationPairList.begin (); it != communicationPairList.end (); it++)
        {
          thr = CalculateSingleStreamThroughput (it->second.packetSink, it->second.totalRx, it->second.throughput);
          totalThr += thr;
        std::cout << std::left << std::setw (12) << thr;
        }
      std::cout << std::left << std::setw (12) << totalThr << std::endl;
    }
  else
    {
      std::cout << to_string_with_precision<double> (Simulator::Now ().GetSeconds (), 1);
      for (CommunicationPairList_I it = communicationPairList.begin (); it != communicationPairList.end (); it++)
        {
          thr = CalculateSingleStreamThroughput (it->second.packetSink, it->second.totalRx, it->second.throughput);
          totalThr += thr;
        std::cout << "," << thr;
        }
      std::cout << "," << totalThr << std::endl;
    }
  Simulator::Schedule (MilliSeconds (100), &CalculateThroughput);
}

void
SLSCompleted (Ptr<DmgWifiMac> wifiMac, Mac48Address address, ChannelAccessPeriod accessPeriod,
              BeamformingDirection beamformingDirection, bool isInitiatorTxss, bool isResponderTxss,
              SECTOR_ID sectorId, ANTENNA_ID antennaId)
{
  if (!csv)
    {
      std::cout << "DMG STA " << wifiMac->GetAddress () << " completed SLS phase with DMG STA " << address << std::endl;
      std::cout << "Best Tx Antenna Configuration: SectorID=" << uint16_t (sectorId) << ", AntennaID=" << uint16_t (antennaId) << std::endl;
    }
}

void
StationAssoicated (Ptr<Node> node, Ptr<DmgWifiMac> staWifiMac, Mac48Address address, uint16_t aid)
{
  if (!csv)
    {
      std::cout << "DMG STA " << staWifiMac->GetAddress () << " associated with DMG PCP/AP " << address
                << ", Association ID (AID) = " << aid << std::endl;
    }
  CommunicationPairList_I it = communicationPairList.find (node);
  if (it != communicationPairList.end ())
    {
      it->second.startTime = Simulator::Now ();
      it->second.srcApp->StartApplication ();
    }
  else
    {
      NS_FATAL_ERROR ("Could not find application to run.");
    }
}

void
StationDeassoicated (Ptr<Node> node, Ptr<DmgWifiMac> staWifiMac, Mac48Address address)
{
  if (!csv)
    {
      std::cout << "DMG STA " << staWifiMac->GetAddress () << " deassociated from DMG PCP/AP " << address << std::endl;
    }
  CommunicationPairList_I it = communicationPairList.find (node);
  if (it != communicationPairList.end ())
    {
      it->second.srcApp->StopApplication ();
    }
  else
    {
      NS_FATAL_ERROR ("Could not find application to stop.");
    }
}

/**
 * Callback method to log changes of the congestion window.
 */
void
CwndChange (Ptr<OutputStreamWrapper> file, const uint32_t oldCwnd, const uint32_t newCwnd)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newCwnd << endl;
}

/**
* Callback method to log changes of the receive window.
*/
void
RwndChange (Ptr<OutputStreamWrapper> file, const uint32_t oldRwnd, const uint32_t newRwnd)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newRwnd << endl;
}

/**
 * Callback method to log changes of the round trip time.
 */
void
RttChange (Ptr<OutputStreamWrapper> file, const Time oldRtt, const Time newRtt)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newRtt.GetNanoSeconds () << endl;
}

/**
 * Callback method to log changes of the TcpTxBufferSize.
 */
void
TcpBufferSizeChange (Ptr<OutputStreamWrapper> file, uint32_t oldValue, uint32_t newValue)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newValue << endl;
}

/**
 * Callback method to log the number of packets in the Wifi MAC Queue.
 */
void
QueueOccupancyChange (Ptr<OutputStreamWrapper> file, uint32_t oldValue, uint32_t newValue)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newValue << endl;
}

/**
 * Callback method to log number of Packets in TC Queue.
 */
void
TcPacketsInQueueTrace (Ptr<OutputStreamWrapper> file, uint32_t oldValue, uint32_t newValue)
{
  std::ostream *output = file->GetStream ();
  *output << Simulator::Now ().GetNanoSeconds () << "," << newValue << endl;
}

int
main (int argc, char *argv[])
{
  string applicationType = "bulk";                /* Type of the Tx application */
  string socketType = "ns3::TcpSocketFactory";    /* Socket Type (TCP/UDP) */
  uint32_t packetSize = 1448;                     /* Application payload size in bytes. */
  string dataRate = "300Mbps";                    /* Application data rate. */
  string tcpVariant = "NewReno";                  /* TCP Variant Type. */
  uint32_t maxPackets = 0;                        /* Maximum Number of Packets */
  uint32_t bufferSize = 131072;                   /* TCP Send/Receive Buffer Size. */
  bool trafficControl = false;                    /* Whether to isntall Traffic Control layer */
  uint32_t queueSize = 1000;                      /* Wifi MAC Queue Size. */
  bool frameCapture = false;                      /* Use a frame capture model. */
  double frameCaptureMargin = 10;                 /* Frame capture margin in dB. */
  uint32_t msduAggregationSize = 7935;            /* The maximum aggregation size for A-MSDU in Bytes. */
  uint32_t mpduAggregationSize = 262143;          /* The maximum aggregation size for A-MPDU in Bytes. */
  bool enableJitter = true;                       /* Enable Beacon Interval Jitter to randomize Beacon Interval start time. */
  std::string beaconJitter = "ns3::UniformRandomVariable[Min=0|Max=10]";
  string phyMode = "DMG_MCS12";                   /* Type of the Physical Layer. */
  uint32_t parallelLinks = 2;                     /* The number of parallel links. */
  double distance = 1;                            /* The distance between devices. */
  double simulationTime = 10;                     /* Simulation time in seconds. */
  std::string tracesPath = "/";                   /* The location where to save traces. */
  uint32_t snapShotLength = std::numeric_limits<uint32_t>::max (); /* The maximum PCAP Snapshot Length */
  bool verbose = false;                           /* Print Logging Information. */
  bool pcapTracing = false;                       /* PCAP Tracing is enabled or not. */
  std::map<std::string, std::string> tcpVariants; /* List of the TCP Variants */

  /** TCP Variants **/
  tcpVariants.insert (std::make_pair ("NewReno",       "ns3::TcpNewReno"));
  tcpVariants.insert (std::make_pair ("Hybla",         "ns3::TcpHybla"));
  tcpVariants.insert (std::make_pair ("HighSpeed",     "ns3::TcpHighSpeed"));
  tcpVariants.insert (std::make_pair ("Vegas",         "ns3::TcpVegas"));
  tcpVariants.insert (std::make_pair ("Scalable",      "ns3::TcpScalable"));
  tcpVariants.insert (std::make_pair ("Veno",          "ns3::TcpVeno"));
  tcpVariants.insert (std::make_pair ("Bic",           "ns3::TcpBic"));
  tcpVariants.insert (std::make_pair ("Westwood",      "ns3::TcpWestwood"));
  tcpVariants.insert (std::make_pair ("WestwoodPlus",  "ns3::TcpWestwoodPlus"));

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue ("applicationType", "Type of the Tx Application: onoff or bulk", applicationType);
  cmd.AddValue ("packetSize", "Application packet size in bytes", packetSize);
  cmd.AddValue ("dataRate", "Application data rate", dataRate);
  cmd.AddValue ("maxPackets", "Maximum number of packets to send", maxPackets);
  cmd.AddValue ("tcpVariant", "Transport protocol to use: TcpTahoe, TcpReno, TcpNewReno, TcpWestwood, TcpWestwoodPlus", tcpVariant);
  cmd.AddValue ("socketType", "Type of the Socket (ns3::TcpSocketFactory, ns3::UdpSocketFactory)", socketType);
  cmd.AddValue ("bufferSize", "TCP Buffer Size (Send/Receive) in Bytes", bufferSize);
  cmd.AddValue ("trafficControl", "Add traffic control layer (QueueDisc)", trafficControl);
  cmd.AddValue ("msduAggregation", "The maximum aggregation size for A-MSDU in Bytes", msduAggregationSize);
  cmd.AddValue ("mpduAggregation", "The maximum aggregation size for A-MPDU in Bytes", mpduAggregationSize);
  cmd.AddValue ("queueSize", "The maximum size of the Wifi MAC Queue", queueSize);
  cmd.AddValue ("frameCapture", "Use a frame capture model", frameCapture);
  cmd.AddValue ("frameCaptureMargin", "Frame capture model margin in dB", frameCaptureMargin);
  cmd.AddValue ("phyMode", "802.11ad PHY Mode", phyMode);
  cmd.AddValue ("enableJitter", "Enable Beacon Interval Jitter to randomize Beacon Interval start time", enableJitter);
  cmd.AddValue ("beaconJitter", "Beacon Jitter value in MicroSeconds", beaconJitter);
  cmd.AddValue ("parallelLinks", "The number of parallel links", parallelLinks);
  cmd.AddValue ("dist", "The distance between devices", distance);
  cmd.AddValue ("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue ("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue ("snapShotLength", "The maximum PCAP Snapshot Length", snapShotLength);
  cmd.AddValue ("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue ("csv", "Enable CSV output instead of plain text. This mode will suppress all the messages related statistics and events.", csv);
  cmd.Parse (argc, argv);

  /* Global params: no fragmentation, no RTS/CTS, fixed rate for all packets */
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("999999"));
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("999999"));
  Config::SetDefault ("ns3::QueueBase::MaxPackets", UintegerValue (queueSize));

  /*** Configure TCP Options ***/
  /* Select TCP variant */
  std::map<std::string, std::string>::const_iterator iter = tcpVariants.find (tcpVariant);
  NS_ASSERT_MSG (iter != tcpVariants.end (), "Cannot find Tcp Variant");
  TypeId tid = TypeId::LookupByName (iter->second);
  Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (tid));
  if (tcpVariant.compare ("Westwood") == 0)
    {
      Config::SetDefault ("ns3::TcpWestwood::ProtocolType", EnumValue (TcpWestwood::WESTWOOD));
      Config::SetDefault ("ns3::TcpWestwood::FilterType", EnumValue (TcpWestwood::TUSTIN));
    }
  else if (tcpVariant.compare ("WestwoodPlus") == 0)
    {
      Config::SetDefault ("ns3::TcpWestwood::ProtocolType", EnumValue (TcpWestwood::WESTWOODPLUS));
      Config::SetDefault ("ns3::TcpWestwood::FilterType", EnumValue (TcpWestwood::TUSTIN));
    }

  /* Configure TCP Segment Size */
  Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (packetSize));
  Config::SetDefault ("ns3::TcpSocket::SndBufSize", UintegerValue (bufferSize));
  Config::SetDefault ("ns3::TcpSocket::RcvBufSize", UintegerValue (bufferSize));

  /**** WifiHelper is a meta-helper: it helps creates helpers ****/
  DmgWifiHelper wifi;

  /* Basic setup */
  wifi.SetStandard (WIFI_PHY_STANDARD_80211ad);

  /* Turn on logging */
  if (verbose)
    {
      wifi.EnableLogComponents ();
      LogComponentEnable ("EvaluateMultiBSSOperation", LOG_LEVEL_ALL);
    }

  /**** Set up Channel ****/
  DmgWifiChannelHelper wifiChannel ;
  /* Simple propagation delay model */
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel", "Frequency", DoubleValue (60.48e9));

  /**** SETUP ALL NODES ****/
  DmgWifiPhyHelper wifiPhy = DmgWifiPhyHelper::Default ();
  /* Nodes will be added to the channel we set up earlier */
  wifiPhy.SetChannel (wifiChannel.Create ());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wifiPhy.Set ("TxPowerStart", DoubleValue (10.0));
  wifiPhy.Set ("TxPowerEnd", DoubleValue (10.0));
  wifiPhy.Set ("TxPowerLevels", UintegerValue (1));
  /* Set operating channel */
  wifiPhy.Set ("ChannelNumber", UintegerValue (2));
  if (frameCapture)
    {
      /* Frame Capture Model */
      wifiPhy.Set ("FrameCaptureModel", StringValue ("ns3::SimpleFrameCaptureModel"));
      Config::SetDefault ("ns3::SimpleFrameCaptureModel::Margin", DoubleValue (frameCaptureMargin));
    }
  /* Set error model */
  wifiPhy.SetErrorRateModel ("ns3::DmgErrorModel",
                             "FileName", StringValue ("DmgFiles/ErrorModel/LookupTable_1458.txt"));
  /* Sensitivity model includes implementation loss and noise figure */
  wifiPhy.Set ("CcaMode1Threshold", DoubleValue (-79));
  wifiPhy.Set ("EnergyDetectionThreshold", DoubleValue (-79 + 3));
  /* Set default algorithm for all nodes to be constant rate */
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue (phyMode));

  /* Make four nodes and set them up with PHY and MAC */
  NodeContainer apWifiNodes;
  apWifiNodes.Create (parallelLinks);

  NodeContainer staWifiNodes;
  staWifiNodes.Create (parallelLinks);

  /* Set Analytical Codebook for the DMG Devices */
  wifi.SetCodebook ("ns3::CodebookAnalytical",
                    "CodebookType", EnumValue (SIMPLE_CODEBOOK),
                    "Antennas", UintegerValue (1),
                    "Sectors", UintegerValue (8));

  /* Install DMG PCP/AP Nodes */
  NetDeviceContainer apDevices;
  for (uint32_t i = 0; i < parallelLinks; i++)
    {
      DmgWifiMacHelper wifiMac = DmgWifiMacHelper::Default ();
      wifiMac.SetType ("ns3::DmgApWifiMac",
                       "Ssid", SsidValue (Ssid ("AP" + std::to_string (i))),
                       "BeaconInterval", TimeValue (MicroSeconds (102400)),
                       "BE_MaxAmpduSize", UintegerValue (mpduAggregationSize),
                       "BE_MaxAmsduSize", UintegerValue (msduAggregationSize),
                       "SSSlotsPerABFT", UintegerValue (8), "SSFramesPerSlot", UintegerValue (16),
                       "EnableBeaconJitter", BooleanValue (enableJitter),
                       "ATIPresent", BooleanValue (false));
      if (enableJitter)
        {
          wifiMac.SetAttrbiute ("BeaconJitter", StringValue (beaconJitter));
        }
      apDevices.Add (wifi.Install (wifiPhy, wifiMac, apWifiNodes.Get (i)));
    }

  /** Install DMG STA Nodes **/
  NetDeviceContainer staDevices;
  for (uint32_t i = 0; i < parallelLinks; i++)
    {
      DmgWifiMacHelper wifiMac = DmgWifiMacHelper::Default ();
      wifiMac.SetType ("ns3::DmgStaWifiMac",
                       "Ssid", SsidValue (Ssid ("AP" + std::to_string (i))), "ActiveProbing", BooleanValue (false),
                       "BE_MaxAmpduSize", UintegerValue (mpduAggregationSize),
                       "BE_MaxAmsduSize", UintegerValue (msduAggregationSize),
                       "RSSRetryLimit", UintegerValue (32),
                       "RSSBackoff", UintegerValue (1));
      staDevices.Add (wifi.Install (wifiPhy, wifiMac, staWifiNodes.Get (i)));
    }

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  /* Add locations of the DMG PCP/APs */
  for (uint32_t i = 0; i < parallelLinks; i++)
    {
      positionAlloc->Add (Vector (i * distance, +1.0, 0.0));
    }
  /* Add locations of the DMG PCP/APs */
  for (uint32_t i = 0; i < parallelLinks; i++)
    {
      positionAlloc->Add (Vector (i * distance, -1.0, 0.0));
    }

  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (apWifiNodes);
  mobility.Install (staWifiNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install (apWifiNodes);
  stack.Install (staWifiNodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer apInterfaces;
  apInterfaces = address.Assign (apDevices);
  address.Assign (staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  /* We do not want any ARP packets */
  PopulateArpCache ();

  /** Install Applications **/
  AsciiTraceHelper asciiTraceHelper;

  /* DMG STA -->  DMG AP */
  for (uint32_t i = 0; i < parallelLinks; i++)
    {
      CommunicationPair commPair;
      ostringstream linkId;
      linkId << i + 1;

      /* Install Simple TCP/UDP Server on the destination node */
      PacketSinkHelper sinkHelper (socketType, InetSocketAddress (Ipv4Address::GetAny (), 9999));
      ApplicationContainer sinkApp = sinkHelper.Install (apWifiNodes.Get (i));
      commPair.packetSink = StaticCast<PacketSink> (sinkApp.Get (0));
      sinkApp.Start (Seconds (0.0));

      /* Install TCP/UDP Transmitter on the DMG STA */
      Address dest (InetSocketAddress (apInterfaces.GetAddress (i), 9999));
      ApplicationContainer srcApp;
      TypeId socketTid = TypeId::LookupByName (socketType);
      Ptr<TcpSocketBase> tcpSocket = StaticCast<TcpSocketBase> (Socket::CreateSocket (staWifiNodes.Get (i), socketTid));
      Ptr<TcpSendApplication> app = CreateObject<TcpSendApplication> ();

      app->Setup (tcpSocket, dest, packetSize, DataRate (dataRate), (applicationType == "bulk"));
      staWifiNodes.Get (i)->AddApplication (app);
      srcApp.Add (app);

      /* Connect TCP Socket Traces */
      Ptr<OutputStreamWrapper> cwndStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "cwnd_" + linkId.str () + ".txt");
      Ptr<OutputStreamWrapper> rwndStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "rwnd_" + linkId.str () + ".txt");
      Ptr<OutputStreamWrapper> rttStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "rtt_" + linkId.str () + ".txt");

      tcpSocket->TraceConnectWithoutContext ("CongestionWindow", MakeBoundCallback (&CwndChange, cwndStream));
      tcpSocket->TraceConnectWithoutContext ("RWND", MakeBoundCallback (&RwndChange, rwndStream));
      tcpSocket->TraceConnectWithoutContext ("RTT", MakeBoundCallback (&RttChange, rttStream));

      srcApp.Start (Seconds (simulationTime + 1));
      srcApp.Stop (Seconds (simulationTime));
      commPair.srcApp = app;
      communicationPairList[staWifiNodes.Get (i)] = commPair;
    }

  Ptr<WifiNetDevice> wifiNetDevice;
  Ptr<DmgWifiMac> dmgWifiMac;
  for (uint32_t i = 0; i < apDevices.GetN (); i++)
    {
      ostringstream linkId;
      linkId << i + 1;

      /* Connect DMG PCP/AP Traces */
      wifiNetDevice = StaticCast<WifiNetDevice> (apDevices.Get (i));
      dmgWifiMac = StaticCast<DmgWifiMac> (wifiNetDevice->GetMac ());
      dmgWifiMac->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, dmgWifiMac));

      /* Connect Wifi MAC Queue Occupancy */
      Ptr<OutputStreamWrapper> apQueueOccupanyStream;
      apQueueOccupanyStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "AP_MAC_Queue_Occupany" + linkId.str () + ".txt");
      dmgWifiMac->GetBEQueue ()->GetQueue ()->TraceConnectWithoutContext ("OccupancyChanged",
                                                                          MakeBoundCallback (&QueueOccupancyChange, apQueueOccupanyStream));

      /* Connect DMG STA Traces */
      wifiNetDevice = StaticCast<WifiNetDevice> (staDevices.Get (i));
      dmgWifiMac = StaticCast<DmgWifiMac> (wifiNetDevice->GetMac ());
      dmgWifiMac->TraceConnectWithoutContext ("Assoc", MakeBoundCallback (&StationAssoicated, staWifiNodes.Get (i), dmgWifiMac));
      dmgWifiMac->TraceConnectWithoutContext ("DeAssoc", MakeBoundCallback (&StationDeassoicated, staWifiNodes.Get (i), dmgWifiMac));
      dmgWifiMac->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, dmgWifiMac));

      /* Connect Wifi MAC Queue Occupancy */
      Ptr<OutputStreamWrapper> staQueueOccupanyStream;
      staQueueOccupanyStream = asciiTraceHelper.CreateFileStream ("Traces" + tracesPath + "STA_MAC_Queue_Occupany" + linkId.str () + ".txt");
      dmgWifiMac->GetBEQueue ()->GetQueue ()->TraceConnectWithoutContext ("OccupancyChanged",
                                                                          MakeBoundCallback (&QueueOccupancyChange, staQueueOccupanyStream));
    }

  /* Enable Traces */
  if (pcapTracing)
    {
      wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
      wifiPhy.SetSnapshotLength (snapShotLength);
      wifiPhy.EnablePcap ("Traces/AccessPoint", apDevices, false);
      wifiPhy.EnablePcap ("Traces/STA", staDevices, false);
    }

  /* Install FlowMonitor on all nodes */
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll ();

  /* Print Output */
  if (!csv)
    {
      std::cout << "Application Layer Throughput per Communicating Pair [Mbps]" << std::endl;
      std::cout << std::left << std::setw (12) << "Time [s]";
      string columnName;
      for (uint8_t i = 0; i < communicationPairList.size (); i++)
        {
          columnName = "Pair (" + std::to_string (i + 1) + ")";
          std::cout << std::left << std::setw (12) << columnName;
        }
       std::cout << std::left << std::setw (12) << "Total" << std::endl;
    }

  /* Schedule Throughput Calulcations */
  Simulator::Schedule (Seconds (0.1), &CalculateThroughput);

  Simulator::Stop (Seconds (simulationTime + 0.101));
  Simulator::Run ();
  Simulator::Destroy ();

  if (!csv)
    {
      /* Print per flow statistics */
      monitor->CheckForLostPackets ();
      Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
      FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats ();
      for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
        {
          Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
          std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")" << std::endl;;
          std::cout << "  Tx Packets: " << i->second.txPackets << std::endl;
          std::cout << "  Tx Bytes:   " << i->second.txBytes << std::endl;
          std::cout << "  TxOffered:  " << i->second.txBytes * 8.0 / ((simulationTime - 0.1) * 1e6)  << " Mbps" << std::endl;;
          std::cout << "  Rx Packets: " << i->second.rxPackets << std::endl;;
          std::cout << "  Rx Bytes:   " << i->second.rxBytes << std::endl;
          std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / ((simulationTime - 0.1) * 1e6)  << " Mbps" << std::endl;;
        }

      /* Print Application Layer Results Summary */
      std::cout << "\nApplication Layer Statistics:" << std::endl;
      uint16_t communicationLinks = 1;
      for (CommunicationPairList_I it = communicationPairList.begin (); it != communicationPairList.end (); it++)
        {
          std::cout << "Communication Link (" << communicationLinks << ") Statistics:" << std::endl;
          std::cout << "  Tx Packets: " << it->second.srcApp->GetTotalTxPackets () << std::endl;
          std::cout << "  Tx Bytes:   " << it->second.srcApp->GetTotalTxBytes () << std::endl;
          std::cout << "  Rx Packets: " << it->second.packetSink->GetTotalReceivedPackets () << std::endl;
          std::cout << "  Rx Bytes:   " << it->second.packetSink->GetTotalRx () << std::endl;
          std::cout << "  Throughput: " << it->second.packetSink->GetTotalRx () * 8.0 / ((simulationTime - it->second.startTime.GetSeconds ()) * 1e6)
                    << " Mbps" << std::endl;
          communicationLinks++;
        }
    }

  return 0;
}
