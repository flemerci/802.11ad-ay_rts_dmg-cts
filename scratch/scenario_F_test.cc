/*
 * Copyright (c) 2015-2017 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "common-functions.h"
#include <sys/stat.h>
#include <string>
#include <complex>
#include <iomanip>
#include <sstream>

/**
 * Simulation Objective:
 * This script is used to compare the performance of the channel access schemes in IEEE 802.11ad.
 * Basically, the simulation compares the achievable throughput between CSMA/CA and SP allocations.
 *
 * Network Topology:
 * The scenario consists of single PCP/AP and n DMG STAs circularly distributed around AP.
 *
 *          DMG PCP/AP (0,0)                       DMG STA (+1,0)
 *
 * Simulation Description:
 * In thet case of CSMA/CA access period, the whole DTI access period is allocated as CBAP. Whereas in
 * SP applications, once the DMG STA has associated successfully with the PCP/AP. The PCP/AP allocates the
 * whole DTI as SP allocation.
 *
 * Running Simulation:
 * To evaluate CSMA/CA channel access scheme with 3 nodes:
 * ./waf --run "compare_access_schemes --scheme=1 --simulationTime=10 --nodeNb=3 --pcap=true"
 *
 * To evaluate SP channel access scheme:
 * ./waf --run "compare_access_schemes --scheme=0 --simulationTime=10 --pcap=true"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station.
 */
#define PI 3.14159265

NS_LOG_COMPONENT_DEFINE ("ScenarioFTest");

using namespace ns3;
using namespace std;

// MAPPING MAC ID

typedef std::map<Ipv4Address, uint32_t> MAP_IP2ID;
typedef std::map<Mac48Address, uint32_t> MAP_MAC2ID;
typedef MAP_MAC2ID::iterator MAP_MAC2ID_I;
MAP_MAC2ID map_Mac2ID;
typedef MAP_IP2ID::iterator MAP_IP2ID_I;
MAP_IP2ID map_IP2ID;

std::vector< std::vector< std::vector< uint32_t > > > node_Pair_rts; //To ADD for NAV update
std::vector< std::vector< std::vector< uint32_t > > > node_Pair_cts; //To ADD for NAV update

/* Type definitions */
struct CommunicationPair {
  Ptr<Application> srcApp;
  Ptr<PacketSink> packetSink;
  uint64_t totalRx = 0;
  double throughput = 0;
  Time startTime;
  Time stopTime;
};

typedef std::map<Ptr<Node>, CommunicationPair> CommunicationPairList;
typedef CommunicationPairList::iterator CommunicationPairList_I;
typedef CommunicationPairList::const_iterator CommunicationPairList_CI;

struct Parameters : public SimpleRefCount<Parameters> {
  uint32_t srcNodeID;
  Ptr<DmgWifiMac> wifiMac;
};



template <typename T>
std::string to_string_with_precision (const T a_value, const int n = 6)
{
    std::ostringstream out;
    out.precision (n);
    out << std::fixed << a_value;
    return out.str ();
}

CommunicationPairList communicationPairList;    /* List of communicating devices. */

/**  Application Variables **/
uint64_t totalRx = 0;
double throughput = 0;
Ptr<PacketSink> sink1;
double simulationTime = 10;                   /* Simulation time in seconds. */
bool bi_Flag = 1;
bool dti_Flag = 0;
uint32_t packetSize = 1448;                   /* Application payload size in bytes. */
string dataRate = "300Mbps";                  /* Application data rate. */
std::vector< uint32_t > VarMacDrop;
std::vector< uint32_t > VarMacRxDrop;
std::vector< uint32_t > VarPhyRxDrop;
int oldAppNb = 0;
Time oldDelay;
double oldnow = 0;
int allSTAAssoc =0;
int globalNodeUsernb =0;

std::vector< Time > timeArray;
/* Network Nodes */

Ptr<DmgApWifiMac> apWifiMac;

/* Access point variable */
bool scheduledStaticPeriods = false;      /* Flag to indicate whether we scheduled Static Service Periods or not */

/*** Service Period Parameters ***/
uint16_t sp1Duration = 20000;             /* The duration of the allocated service period (1) in MicroSeconds */
uint16_t sp2Duration = 12000;             /* The duration of the allocated service period (2) in MicroSeconds */
uint32_t sp1StartTime;                    /* The start time of the allocated service period (1) in MicroSeconds */
uint32_t sp2StartTime;                    /* The start time of the allocated service period (2) in MicroSeconds */
uint16_t offsetDuration = 1000;           /* The offset between the start of the two service period allocations in MicroSeconds */
bool stationAssociated = false;
/* Access Period Parameters */
uint32_t allocationType = 1;               /* The type of channel access scheme during DTI (CBAP is the default) */

std::ofstream m_cwStateFile;
std::ofstream m_nodeStateFile;
std::ofstream m_throughputStateFile;
std::ofstream m_delayStateFile;
std::ofstream m_delay2StateFile;
std::ofstream m_dropStateFile;
std::ofstream m_navUpdateFile;
std::ofstream m_nodeAssocFile;
std::ofstream m_BackOffFile;
std::ofstream m_MacDropFile;
std::ofstream m_MacRxDropFile;
std::ofstream m_PhyRxDropFile;

bool fileExists(const std::string& filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}

std::vector<std::string>
split(std::string str, char delimiter)
{
               std::vector<std::string> internal;
               std::stringstream ss(str); // Turn the string into a stream.
               std::string tok;

               while(getline(ss, tok, delimiter)) {
                              internal.push_back(tok);
               }
    return internal;
}

float lastupdatestatus[500];
float timeIDLE[500];
float timeCCA[500];
float timeRX[500];
float timeTX[500];
float throughput_AP = 0;
WifiPhyState node_state[500] = {SLEEP};

void
PhyRxDrop(std::string context, Ptr<const Packet> p)
{
	std::vector< std::string> sep = split(context, '/');

	int idNode =  atoi(sep[2].c_str());
	VarPhyRxDrop[idNode]++;

}

void
MacTxDrop_F(std::string context, Ptr<const Packet> p)
{

  std::vector< std::string> sep = split(context, '/');

  int idNode =  atoi(sep[2].c_str());
  VarMacDrop[idNode]++;

}

void
MacRxDrop_F(std::string context, Ptr<const Packet> p)
{

  std::vector< std::string> sep = split(context, '/');

  int idNode =  atoi(sep[2].c_str());
  VarMacRxDrop[idNode]++;

}

void PhyStateTrace(std::string context, Time start, Time duration, enum WifiPhyState state)
{

	std::vector< std::string> sep = split(context, '/');
	float timeSecond = start.GetDouble()/1000000000;
	int idNode =  atoi(sep[2].c_str());

	if ( Simulator::Now ().GetSeconds () <= simulationTime && dti_Flag == 1)
			{

			if(lastupdatestatus[idNode] == -1)
	               {
								  node_state[idNode] = state;
	                              lastupdatestatus[idNode] = timeSecond;
	               }
	               else
	               {
	                              switch (node_state[idNode])
	                              {
	                              case IDLE:
	                                            // m_idleStateFile << idNode << "," << lastupdatestatus[idNode] << "," << timeSecond << std::endl;
	                                             timeIDLE[idNode] = timeIDLE[idNode] + timeSecond - lastupdatestatus[idNode];
	                                             lastupdatestatus[idNode] = timeSecond;
	                                             node_state[idNode] = state;
	                                             break;
	                              case RX:
	                                             //m_rxStateFile << idNode << "," << lastupdatestatus[idNode] << "," << timeSecond << std::endl;
	                            	  	  	  	 timeRX[idNode] = timeRX[idNode] + timeSecond - lastupdatestatus[idNode];
	                                             lastupdatestatus[idNode] = timeSecond;
	                                             node_state[idNode] = state;
	                                             break;
	                              case TX:
	                                            // m_txStateFile << idNode << "," << lastupdatestatus[idNode] << "," << timeSecond << std::endl;

	                                             timeTX[idNode] = timeTX[idNode] + timeSecond - lastupdatestatus[idNode];
	                                             lastupdatestatus[idNode] = timeSecond;
	                                             node_state[idNode] = state;
	                                             break;
	                              case CCA_BUSY:
	                                            // m_ccaStateFile << idNode << "," << lastupdatestatus[idNode] << "," << timeSecond << std::endl;

	                                             timeCCA[idNode] = timeCCA[idNode] + timeSecond - lastupdatestatus[idNode];
	                                             lastupdatestatus[idNode] = timeSecond;
	                                             node_state[idNode] = state;
	                                             break;

	                              case SLEEP:
	                                             //std::cout << context << " SLEEP " << std::endl;
	                                             break;
	                              case SWITCHING:
	                                             //std::cout << context << " SWITCHING " << std::endl;
	                                             break;
	                              case OFF:
	                              	             //std::cout << context << " OFF " << std::endl;
	                              	             break;
	                              default:
	                                             //std::cout << context << "VALEUR ETAT " << node_state[idNode] << " DEFAULT " << std::endl;
	                                             break;
	                              }

	               }
			}
}

double
CalculateSingleStreamThroughput (Ptr<PacketSink> sink, uint64_t &lastTotalRx, double &averageThroughput)
{
  double thr = (sink->GetTotalRx() - lastTotalRx) * (double) 8/1e5;     /* Convert Application RX Packets to MBits. */
  lastTotalRx = sink->GetTotalRx ();
  averageThroughput += thr;

  return thr;
}

void
CalculateThroughput (void)
{
  double thr = CalculateSingleStreamThroughput (sink1, totalRx, throughput);
  std::cout << Simulator::Now ().GetSeconds () << '\t' << thr << std::endl;
  Simulator::Schedule (MilliSeconds (100), &CalculateThroughput);
}

void
StationAssoicated (Ptr<Node> node, Ptr<DmgWifiMac> staWifiMac, Ptr<DmgApWifiMac> apWifiMac, Mac48Address address, uint16_t aid)
{

  std::cout << "DMG STA " << staWifiMac->GetAddress () << " associated with DMG PCP/AP " << address << ", Association ID (AID) = " << aid << std::endl;
  m_nodeAssocFile  << map_Mac2ID[staWifiMac->GetAddress ()] << "," << aid << std::endl;
  stationAssociated = true;


  allSTAAssoc = allSTAAssoc + 1;

  if (allSTAAssoc == globalNodeUsernb)
  {
	  for (CommunicationPairList_I it = communicationPairList.begin (); it != communicationPairList.end (); it++)
		{
		  it->second.stopTime = Simulator::Now () + Seconds(simulationTime);
		  //std::cout << "STOPTIME: " << it->second.stopTime << std::endl;
		  it->second.startTime = Simulator::Now ();

		  //std::cout << "STOPTIME: " << it->second.stopTime << std::endl;
		  it->second.srcApp->StartApplication ();


		}
  }
}

void
StationDeassoicated (Ptr<Node> node, Ptr<DmgWifiMac> staWifiMac, Mac48Address address)
{

  std::cout << "DMG STA " << staWifiMac->GetAddress () << " deassociated from DMG PCP/AP " << address << std::endl;

  CommunicationPairList_I it = communicationPairList.find (node);
  if (it != communicationPairList.end ())
    {
      it->second.srcApp->StopApplication ();
    }
  else
    {
      NS_FATAL_ERROR ("Could not find application to delete.");
    }
}

CommunicationPair
InstallApplications (Ptr<Node> srcNode, Ptr<Node> dstNode, Ipv4Address address, uint8_t appNumber)
{
  CommunicationPair commPair;

  /* Install TCP/UDP Transmitter on the source node */
  ApplicationContainer srcApp;
  OnOffHelper src ("ns3::UdpSocketFactory", InetSocketAddress (address, 9000 + appNumber));
  src.SetAttribute ("MaxBytes", UintegerValue (0));
  src.SetAttribute ("PacketSize", UintegerValue (packetSize));
  src.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1e6]"));
  src.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
  src.SetAttribute ("DataRate", DataRateValue (DataRate (dataRate)));
  srcApp = src.Install (srcNode);

  srcApp.Start (Seconds (simulationTime + 21));
  //srcApp.Start (Seconds (0.0));
  srcApp.Stop (Seconds (simulationTime+10 ));
  //srcApp.Stop (Seconds (0.0));
  commPair.srcApp = srcApp.Get (0);
  //Server
  PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), 9000 + appNumber));
  ApplicationContainer sinkApp = sinkHelper.Install (dstNode);
  commPair.packetSink = StaticCast<PacketSink> (sinkApp.Get (0));
  sinkApp.Start (Seconds (0.0));
  return commPair;
}

void
CalculateDrop (std::string context, Time time)
{
	std::vector< std::string> sep = split(context, '/');
	timeArray.push_back(time);
}
void
Navupdate (std::string context, Mac48Address from, Mac48Address to, bool rts, bool cts)
{
	std::vector< std::string> sep = split(context, '/');
	uint32_t NodeID =  atoi(sep[2].c_str());

	uint32_t node_to;
	uint32_t node_from;
	node_to = map_Mac2ID[to];
	node_from = map_Mac2ID[from];
	uint32_t temp;

	if (rts)
	{
		temp = node_Pair_rts[NodeID][node_from][node_to];
		node_Pair_rts[NodeID][node_from][node_to] = temp+1;
	}
	if (cts)
	{
		temp = node_Pair_cts[NodeID][node_from][node_to];
		node_Pair_cts[NodeID][node_from][node_to] = temp+1;
	}
}

void
CalculateDelay (std::string context, Time delay, Time tx)
{
	std::vector< std::string> sep = split(context, '/');
	int appNb =  atoi(sep[4].c_str());
	double now = 0;

	now = Simulator::Now ().GetSeconds ();
	m_delay2StateFile << now << "," << delay << "," << appNb <<  std::endl;

	if (std::find(timeArray.begin(), timeArray.end(), tx) != timeArray.end() )
	{
		m_dropStateFile << Simulator::Now ().GetSeconds () <<  "," << tx <<  std::endl;
	}

	oldAppNb = appNb;
	oldDelay = delay;
	oldnow = now;

}

void
CalculateThroughputAndDelay (void)
{
	double totalThr = 0;
	double thr;

	string duration = to_string_with_precision<double> (Simulator::Now ().GetSeconds () - 0.1, 1) + " - " + to_string_with_precision<double> (Simulator::Now ().GetSeconds (), 1);
	std::cout << std::left << std::setw (12) << duration;
	for (CommunicationPairList_I it = communicationPairList.begin (); it != communicationPairList.end (); it++)
	        {
	          thr = CalculateSingleStreamThroughput (it->second.packetSink, it->second.totalRx, it->second.throughput);
	          totalThr += thr;
	          std::cout << std::left << std::setw (12) << thr;
	        }
	std::cout << std::left << std::setw (12) << totalThr << std::endl;
	m_throughputStateFile << Simulator::Now ().GetSeconds () << "," << totalThr << std::endl;
	Simulator::Schedule (MilliSeconds (100), &CalculateThroughputAndDelay);
}

void
SLSCompleted (Ptr<OutputStreamWrapper> stream, Ptr<Parameters> parameters,
	      Mac48Address address, ChannelAccessPeriod accessPeriod,
	      BeamformingDirection beamformingDirection, bool isInitiatorTxss, bool isResponderTxss,
	      SECTOR_ID sectorId, ANTENNA_ID antennaId)
{
  // In the Visualizer, node ID takes into account the AP so +1 is needed
  *stream->GetStream () << parameters->srcNodeID  << "," << map_Mac2ID[address] << ","
                        //<< lossModelRaytracing->GetCurrentTraceIndex () << ","
                        << uint16_t (sectorId) << "," << uint16_t (antennaId)  << ","
                        << parameters->wifiMac->GetTypeOfStation ()  << ","
                        << map_Mac2ID[parameters->wifiMac->GetBssid ()]  << ","
                        << Simulator::Now () << std::endl;

  if (parameters->wifiMac->GetTypeOfStation () ==  DMG_AP)
  {
	  std::cout << "DMG AP: " << parameters->srcNodeID << " Dest ID: " << map_Mac2ID[address] << " Dest Mac Address: " << address << " Sector ID: " << uint16_t (sectorId)
	              << " Antenna ID: " << uint16_t (antennaId) << std::endl ;
  }
  else {
  std::cout << "DMG STA: " << parameters->srcNodeID << " Dest ID: " << map_Mac2ID[address] << " Dest Mac Address: " << address << " Sector ID: " << uint16_t (sectorId)
            << " Antenna ID: " << uint16_t (antennaId) << std::endl ;
  }
}

static void
CwTrace (Ptr<Parameters> parameters, uint32_t oldval, uint32_t newval)
{
  m_cwStateFile << Simulator::Now ().GetSeconds () << "," << parameters->srcNodeID << "," << oldval << "," << newval << std::endl;
}



void
BackoffTrace (std::string context, uint32_t oldVal, uint32_t newVal)
{
	std::vector< std::string> sep = split(context, '/');
	uint32_t NodeID =  atoi(sep[2].c_str());
	//std::cout << Simulator::Now ().GetSeconds () << " NodeID: "<<  NodeID <<   " Old: " << oldVal << " New: " << newVal << std::endl;
	m_BackOffFile << Simulator::Now ().GetSeconds () << "," << NodeID << "," << oldVal << "," << newVal << std::endl;
}


/*** Beamforming Service Periods ***/
uint16_t biThreshold = 50;                /* BI Threshold to trigger TxSS TXOP. */
//std::map<Mac48Address, uint16_t> biCounter;                   /* Number of beacon intervals that have passed. */
uint16_t biCounter = 0;
void
DataTransmissionIntervalStarted_Trick (Ptr<Node> node, Ptr<DmgApWifiMac> apWifiMac, Ptr<DmgStaWifiMac> staWifiMac, Mac48Address address, Time)
{

	bi_Flag = 0;
	dti_Flag = 1;
	CommunicationPairList_I it = communicationPairList.find (node);
	  if (it != communicationPairList.end ())
	    {
	      if (Simulator::Now () > it->second.stopTime && Simulator::Now () > simulationTime )
	    		  {
	    	  	  	  it->second.srcApp->StopApplication ();
	    		  }

	    }

}

void
BeaconIntervalStarted (Mac48Address address)
{

	    	  	  bi_Flag = 1;
	    	  	  dti_Flag = 0;


}



void ThroughputMonitor (FlowMonitorHelper* fmhelper, Ptr<FlowMonitor> flowMon)
	{
		if ( Simulator::Now ().GetSeconds () <= simulationTime +200)
		{
			flowMon->CheckForLostPackets();
			std::map<FlowId, FlowMonitor::FlowStats> flowStats = flowMon->GetFlowStats();
			Ptr<Ipv4FlowClassifier> classing = DynamicCast<Ipv4FlowClassifier> (fmhelper->GetClassifier());
			for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
			{
				Ipv4FlowClassifier::FiveTuple fiveTuple = classing->FindFlow (stats->first);
				if (stats->second.rxPackets != 0)
					{
			    	  	  m_delayStateFile << Simulator::Now ().GetSeconds () << "," << stats->first << "," << fiveTuple.sourceAddress << "," <<  fiveTuple.destinationAddress << "," << stats->second.delaySum/stats->second.rxPackets <<  std::endl;
			      	  }
				else
			      	  {
			    	  	  //std::cout << "AverageDelay: " << 0 << std::endl;
			    	  	  m_delayStateFile << Simulator::Now ().GetSeconds () << "," << stats->first << "," << fiveTuple.sourceAddress << "," <<  fiveTuple.destinationAddress << "," << 0 <<  std::endl;
			      	  }
			}
			Simulator::Schedule (MilliSeconds (100), &ThroughputMonitor,fmhelper, flowMon );
		}
	}

int
main(int argc, char *argv[])
{
  uint32_t payloadSize = 7675;                  /* Application payload size in bytes. */
  //string dataRate = "6Mbps";                  /* Application data rate. */
  uint32_t msduAggregationSize = 7935;          /* The maximum aggregation size for A-MSDU in Bytes. */
  uint32_t mpduAggregationSize = 262143;        /* The maximum aggregation size for A-MSPU in Bytes. */
  uint32_t queueSize = 100;                    /* Wifi MAC Queue Size. */
  string phyMode = "DMG_MCS12";                 /* Type of the Physical Layer. */
  bool verbose = false;                         /* Print Logging Information. */
  int run = 0; //ADD RUN
  //double simulationTime = 10;                   /* Simulation time in seconds. */
  bool pcapTracing = false;                     /* PCAP Tracing is enabled. */
  int nodeUserNumber = 3;                       /* Total Number of nodes: AP + STAs */
  double AP_STA_DISTANCE = 2;
  string rts_cts = "999999";					/* RTS_CTS threshold */
  int rts_cts_toggle = 0;						/* RTS CTS TOGGLE */
  std::string m_statsFile = "Sim1"; // Prefix for statistics output files
  string path = "results/";
  std::string runstr = "";
  uint32_t distribution_angle = 360;			/* Define the angle of distribution of STA around AP */

  for (int i = 0; i < 500; ++i)
                 {
                                lastupdatestatus[i] = -1;
                                timeIDLE[i]=0;
                                timeCCA[i]=0;
                                timeRX[i]=0;
                                timeTX[i]=0;
                                node_state[i] = {SLEEP};
                 }

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue ("payloadSize", "Application payload size in bytes", payloadSize);
  cmd.AddValue ("dataRate", "Application data rate", dataRate);
  cmd.AddValue ("msduAggregation", "The maximum aggregation size for A-MSDU in Bytes", msduAggregationSize);
  cmd.AddValue ("mpduAggregation", "The maximum aggregation size for A-MPDU in Bytes", mpduAggregationSize);
  cmd.AddValue ("queueSize", "The maximum size of the Wifi MAC Queue", queueSize);
  cmd.AddValue ("scheme", "The access scheme used for channel access (0=SP,1=CBAP)", allocationType);
  cmd.AddValue ("phyMode", "802.11ad PHY Mode", phyMode);
  cmd.AddValue ("verbose", "Turn on all WifiNetDevice log components", verbose);
  cmd.AddValue ("rtscts", "Enable - Disable RTS CTS", rts_cts_toggle);
  cmd.AddValue ("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue ("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.AddValue ("run", "Run index (for setting repeatable seeds)", run); //ADD RUN
  cmd.AddValue ("name", "Simulation Name)", m_statsFile);//Francois
  cmd.AddValue ("nodeNb", "Number of nodes", nodeUserNumber);
  cmd.AddValue ("disApSta", "Distance between AP and STA", AP_STA_DISTANCE);
  cmd.AddValue ("distAngle", "Distance between AP and STA", distribution_angle);
  cmd.AddValue ("path", "Path of the folder to save outputs", path);
  cmd.Parse (argc, argv);

  globalNodeUsernb = nodeUserNumber; //ADD F

  VarMacDrop.resize(uint32_t (nodeUserNumber+1),0);
  VarMacRxDrop.resize(uint32_t (nodeUserNumber+1),0);
  VarPhyRxDrop.resize(uint32_t (nodeUserNumber+1),0);

  std::cout << "Initializing 3D Vectors for NAVs update..." << std::endl;
  uint32_t default_Val = 0;
  node_Pair_rts.resize(uint32_t (nodeUserNumber+1),std::vector<std::vector< uint32_t > >(uint32_t ((nodeUserNumber)*(nodeUserNumber)+1),std::vector< uint32_t >(uint32_t ((nodeUserNumber)*(nodeUserNumber)+1), default_Val)));
  node_Pair_cts.resize(uint32_t (nodeUserNumber+1),std::vector<std::vector< uint32_t > >(uint32_t ((nodeUserNumber)*(nodeUserNumber)+1),std::vector< uint32_t >(uint32_t ((nodeUserNumber)*(nodeUserNumber)+1), default_Val)));
  std::cout << "End Initialization!" << std::endl;
 //To ADD for NAV update

  int flag_run = 0; //test for MCS scenario
  if (run < 0)
  {
	  runstr = std::to_string(run+100);
	  flag_run =1;
	  run = 0;
  }

  SeedManager::SetSeed (1);
  SeedManager::SetRun (run);
  //std::cout << "RUN = " << run << std::endl;
  if (flag_run == 0)
    {
	  runstr = std::to_string(run);
    }

  std::cout << "RTS toggle = " << rts_cts_toggle << std::endl;

  if (rts_cts_toggle)
  {
	  rts_cts = "1500";
  }
  else
  {
	  rts_cts = "999999";
  }
  /* Global params: no fragmentation, no RTS/CTS, fixed rate for all packets */
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("999999"));
  //Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("999999")); // was 999999
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue (rts_cts) );
  Config::SetDefault ("ns3::QueueBase::MaxPackets", UintegerValue (queueSize));

  std::complex<double> test(1,2);
  //std::cout << test << std::endl;

  /**** WifiHelper is a meta-helper: it helps creates helpers ****/
  DmgWifiHelper wifi;

  /* Turn on logging */
  if (verbose)
    {
//      wifi.EnableLogComponents ();
//      LogComponentEnable ("ScenarioFTest", LOG_LEVEL_ALL);
//	  LogComponentEnable ("DmgWifiPhy", LOG_LEVEL_ALL);
	  LogComponentEnable ("DmgWifiMac", LOG_LEVEL_ALL);
//	  LogComponentEnable ("DmgWifiChannel", LOG_LEVEL_ALL);
//	  LogComponentEnable ("PropagationLossModel", LOG_LEVEL_ALL);
      LogComponentEnable ("DmgStaWifiMac", LOG_LEVEL_ALL);
      LogComponentEnable ("DmgApWifiMac", LOG_LEVEL_ALL);
//      LogComponentEnable ("DcaTxop", LOG_LEVEL_ALL);
//      LogComponentEnable ("CodebookAnalytical", LOG_LEVEL_ALL);
//      LogComponentEnable ("SensitivityModel60GHz", LOG_LEVEL_ALL);
//	  LogComponentEnable ("Codebook", LOG_LEVEL_ALL);
//	  LogComponentEnable ("EdcaTxopN", LOG_LEVEL_ALL);
//	  LogComponentEnable ("WifiRemoteStationManager", LOG_LEVEL_ALL);
//	  LogComponentEnable ("MacLow", LOG_LEVEL_ALL);
//	  LogComponentEnable ("OnOffApplication", LOG_LEVEL_ALL);
//	  LogComponentEnable ("WifiMacQueue", LOG_LEVEL_ALL);
//	  LogComponentEnable ("DcfManager", LOG_LEVEL_ALL);
//	  LogComponentEnable ("DcfState", LOG_LEVEL_ALL);
//	  LogComponentEnable ("WifiPhy", LOG_LEVEL_ALL);
//	  LogComponentEnable ("RegularWifiMac", LOG_LEVEL_ALL);
//	  LogComponentEnable ("Callback", LOG_LEVEL_ALL);
//	  LogComponentEnable ("DmgErrorModel", LOG_LEVEL_ALL);
	  //LogComponentEnable ("InterferenceHelper", LOG_LEVEL_ALL);
    }

  /**** Set up Channel ****/
  DmgWifiChannelHelper wifiChannel ;
  /* Simple propagation delay model */
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel", "Frequency", DoubleValue (60.48e9));

  /**** Setup physical layer ****/
  DmgWifiPhyHelper wifiPhy = DmgWifiPhyHelper::Default ();
  /* Nodes will be added to the channel we set up earlier */
  wifiPhy.SetChannel (wifiChannel.Create ());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wifiPhy.Set ("TxPowerStart", DoubleValue (15.0));
  wifiPhy.Set ("TxPowerEnd", DoubleValue (15.0));
  wifiPhy.Set ("TxPowerLevels", UintegerValue (1));

  /* Sensitivity model includes implementation loss and noise figure */
  /* Set error model */
  //wifiPhy.SetErrorRateModel ("ns3::DmgErrorModel","FileName", StringValue ("DmgFiles/ErrorModel/LookupTable_1458.txt"));
  wifiPhy.Set ("RxNoiseFigure", DoubleValue (10));
  wifiPhy.Set ("CcaMode1Threshold", DoubleValue (-79));
  wifiPhy.Set ("EnergyDetectionThreshold", DoubleValue (-79 + 3));
  /* Set default algorithm for all nodes to be constant rate */
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "ControlMode", StringValue (phyMode),
                                                                "DataMode", StringValue (phyMode));

  if (rts_cts_toggle == 1)
    {

	  m_cwStateFile.open(path + m_statsFile + "-cwTraceState-RTS-" + runstr + ".csv");
	  m_throughputStateFile.open(path + m_statsFile + "-throughputState-RTS-" + runstr + ".csv");
	  m_nodeStateFile.open(path + m_statsFile + "-nodePositionTrace-RTS-" + runstr + ".csv");
	  m_delayStateFile.open(path + m_statsFile + "-delayState-RTS-" + runstr + ".csv");
	  m_delay2StateFile.open(path + m_statsFile + "-delay2State-RTS-" + runstr + ".csv");
	  m_dropStateFile.open(path + m_statsFile + "-dropState-RTS-" + runstr + ".csv");
	  m_navUpdateFile.open(path + m_statsFile + "-navUpdate-RTS-" + runstr + ".csv");
	  m_nodeAssocFile.open(path + m_statsFile + "-nodeAssoc-RTS-" + runstr + ".csv");
	  m_BackOffFile.open(path + m_statsFile + "-backOff-RTS-" + runstr + ".csv");
	  m_MacDropFile.open(path + m_statsFile + "-macTxDrop-RTS-" + runstr + ".csv");
	  m_MacRxDropFile.open(path + m_statsFile + "-macRxDrop-RTS-" + runstr + ".csv");
	  m_PhyRxDropFile.open(path + m_statsFile + "-phyRxDrop-RTS-" + runstr + ".csv");

    }
  else if (rts_cts_toggle == 0)
    {
	  m_cwStateFile.open(path + m_statsFile + "-cwTraceState-NO_RTS-" + runstr + ".csv");
	  m_throughputStateFile.open(path + m_statsFile + "-throughputState-NO_RTS-" + runstr + ".csv");
	  m_nodeStateFile.open(path + m_statsFile + "-nodePositionTrace-NO_RTS-" + runstr + ".csv");
	  m_delayStateFile.open(path + m_statsFile + "-delayState-NO_RTS-" + runstr + ".csv");
	  m_delay2StateFile.open(path + m_statsFile + "-delay2State-NO_RTS-" + runstr + ".csv");
	  m_dropStateFile.open(path + m_statsFile + "-dropState-NO_RTS-" + runstr + ".csv");
	  m_navUpdateFile.open(path + m_statsFile + "-navUpdate-NO_RTS-" + runstr + ".csv");
	  m_nodeAssocFile.open(path + m_statsFile + "-nodeAssoc-NO_RTS-" + runstr + ".csv");
	  m_BackOffFile.open(path + m_statsFile + "-backOff-NO_RTS-" + runstr + ".csv");
	  m_MacDropFile.open(path + m_statsFile + "-macTxDrop-NO_RTS-" + runstr + ".csv");
	  m_MacRxDropFile.open(path + m_statsFile + "-macRxDrop-NO_RTS-" + runstr + ".csv");
	  m_PhyRxDropFile.open(path + m_statsFile + "-phyRxDrop-NO_RTS-" + runstr + ".csv");
    }

  m_cwStateFile << "time" << "," << "Node_ID" << "," << "old_Cw_Value" << "," << "new_Cw_Value" << "\n";
  m_throughputStateFile << "time" << "," << "Throughput" << "\n";
  m_delayStateFile  << "Time" << "," << "Flow ID" << "," << "Src" << "," << "Dst" << "," << "Delay" << "\n";
  m_delay2StateFile  << "time" << "," << "Delay" << "," << "ApplicationNumber" << "\n";
  m_nodeStateFile << "Node_ID" << "," << "X" << "," << "Y" << "\n";
  m_nodeStateFile << "AP" << "," << "0" << "," << "0" << "\n";
  m_dropStateFile << "Time" << "," << "Delay" << "\n";
  m_navUpdateFile << "NodeID" << "," << "From" << "," << "To" << "," << "RTS_Value" << "," << "CTS_Value" << "\n";
  m_nodeAssocFile << "NodeID" << "," << "AID" << "\n";
  m_BackOffFile << "time" << "," << "Node_ID" << "," << "old_Cw_Value" << "," << "new_Cw_Value" << "\n";
  m_MacDropFile << "NodeID" << "," << "CollisionNB" << "\n";
  m_MacRxDropFile << "NodeID" << "," << "CollisionNB" << "\n";
  m_PhyRxDropFile << "NodeID" << "," << "CollisionNB" << "\n";
  /* Make n nodes and set them up with the PHY and the MAC */
  //int j = 0;
  NodeContainer apWifiNode;
  apWifiNode.Create (1);
  /* Create n STAs */
  NodeContainer staWifiNodes;
  staWifiNodes.Create (nodeUserNumber);

   /* Add a DMG upper mac */
  DmgWifiMacHelper wifiMac = DmgWifiMacHelper::Default ();

  Ssid ssid = Ssid ("ChiaraScn");
  wifiMac.SetType ("ns3::DmgApWifiMac",
                   "Ssid", SsidValue(ssid),
                   "BE_MaxAmpduSize", UintegerValue (mpduAggregationSize),
                   "BE_MaxAmsduSize", UintegerValue (msduAggregationSize),
                   "SSSlotsPerABFT", UintegerValue (8), "SSFramesPerSlot", UintegerValue (16),
                   "BeaconInterval", TimeValue (MicroSeconds (102400)),  // 102400
				   //"BeaconTransmissionInterval", TimeValue (MicroSeconds (1200)),
                   "ATIPresent", BooleanValue (false)); //false

  /* Set Analytical Codebook for the DMG AP Device */
  wifi.SetCodebook ("ns3::CodebookAnalytical",
                    "CodebookType", EnumValue (SIMPLE_CODEBOOK),
                    "Antennas", UintegerValue (1),
                    "Sectors", UintegerValue (16));

  /* Create Wifi Network Devices (WifiNetDevice) */
  NetDeviceContainer apDevice;
  apDevice = wifi.Install (wifiPhy, wifiMac, apWifiNode);

  wifiMac.SetType ("ns3::DmgStaWifiMac",
                   "Ssid", SsidValue (ssid), "ActiveProbing", BooleanValue (false),
                   "BE_MaxAmpduSize", UintegerValue (mpduAggregationSize),
                   "BE_MaxAmsduSize", UintegerValue (msduAggregationSize));
  /* Set Analytical Codebook for the DMG STA Devices */
  wifi.SetCodebook ("ns3::CodebookAnalytical",
                    "CodebookType", EnumValue (SIMPLE_CODEBOOK),
                    "Antennas", UintegerValue (1),
                    "Sectors", UintegerValue (8));
  wifiPhy.Set ("TxPowerStart", DoubleValue (10.0));
  wifiPhy.Set ("TxPowerEnd", DoubleValue (10.0));

  NetDeviceContainer staDevices;

  staDevices = wifi.Install (wifiPhy, wifiMac, staWifiNodes);

  /* MAP MAC Addresses to NodeIDs */
    NetDeviceContainer devices;
    Ptr<WifiNetDevice> netDevice;
    devices.Add (apDevice);
    devices.Add (staDevices);
    for (uint32_t i = 0; i < devices.GetN (); i++)
      {
        netDevice = StaticCast<WifiNetDevice> (devices.Get (i));
        map_Mac2ID[netDevice->GetMac ()->GetAddress ()] = netDevice->GetNode ()->GetId ();
      }

  /* Setting mobility model */
  MobilityHelper mobilityAP;
  MobilityHelper mobilitySta;
  Ptr<ListPositionAllocator> positionAllocAP = CreateObject<ListPositionAllocator> ();
  positionAllocAP->Add (Vector (0.0, 0.0, 0.0));	/* PCP/AP */
  Ptr<ListPositionAllocator> positionAllocSTA = CreateObject<ListPositionAllocator> ();

  Ptr<UniformRandomVariable> xx = CreateObject<UniformRandomVariable> ();
  Ptr<UniformRandomVariable> yy = CreateObject<UniformRandomVariable> ();

  /***** STA distribution in circular pattern  *****/

  if (distribution_angle == 360)
  {
	  for (int i=0;i<int(nodeUserNumber);i++) {
	        double x, y, temp;
	        x = xx->GetValue(-1*AP_STA_DISTANCE, AP_STA_DISTANCE);
	        //x = xx->GetValue(0, AP_STA_DISTANCE); //PI/4
	        while (true) {
	    	    y = yy->GetValue(-1*AP_STA_DISTANCE, AP_STA_DISTANCE);
	    	    //y = yy->GetValue(0, AP_STA_DISTANCE); //PI/4
	        	temp = sqrt(pow(x,2) + pow(y,2));

	        	if (temp <= AP_STA_DISTANCE) {
	        		break;

	        	}

	        }
	        positionAllocSTA->Add (Vector (x, y, 0.0));
	    	//std::cout << "STA[" << i+1 << "][" << x << "][" << y << "]" << std::endl;
	    	m_nodeStateFile << i+1 << "," << x << "," << y << "\n";
	      }
  }

  else if (distribution_angle == 180)
    {
  	  for (int i=0;i<int(nodeUserNumber);i++) {
  	        double x, y, temp;
  	        x = xx->GetValue(-1*AP_STA_DISTANCE, AP_STA_DISTANCE);
  	        //x = xx->GetValue(0, AP_STA_DISTANCE); //PI/4
  	        while (true) {
  	    	    //y = yy->GetValue(-1*AP_STA_DISTANCE, AP_STA_DISTANCE);
  	    	    y = yy->GetValue(0, AP_STA_DISTANCE); //PI/4
  	        	temp = sqrt(pow(x,2) + pow(y,2));

  	        	if (temp <= AP_STA_DISTANCE) {
  	        			break;

  	        	}

  	        }
  	        positionAllocSTA->Add (Vector (x, y, 0.0));
  	    	//std::cout << "STA[" << i+1 << "][" << x << "][" << y << "]" << std::endl;
  	    	m_nodeStateFile << i+1 << "," << x << "," << y << "\n";
  	      }
    }

  else
  {
	  for (int i=0;i<int(nodeUserNumber);i++)
	  {
	        double x, y, temp;
	        //x = xx->GetValue(-1*AP_STA_DISTANCE, AP_STA_DISTANCE);
	        x = xx->GetValue(0, AP_STA_DISTANCE); //PI/4
	        while (true)
	        {
	    	    //y = yy->GetValue(-1*AP_STA_DISTANCE, AP_STA_DISTANCE);
	    	    y = yy->GetValue(0, AP_STA_DISTANCE); //PI/4
	        	temp = sqrt(pow(x,2) + pow(y,2));
	        	if (atan(y/x) <  (PI/(180/distribution_angle)))
	        	{
	        			if (temp <= AP_STA_DISTANCE)
	        			{
	        			break;
	        			}
        		}

   			}
	        positionAllocSTA->Add (Vector (x, y, 0.0));
	    	//std::cout << "STA[" << i+1 << "][" << x << "][" << y << "]" << std::endl;
	    	m_nodeStateFile << i+1 << "," << x << "," << y << "\n";
      }
  }


  //positionAllocSTA->Add (Vector (x, y, 0.0));

  m_nodeStateFile.close ();
  mobilityAP.SetPositionAllocator (positionAllocAP);
  mobilitySta.SetPositionAllocator (positionAllocSTA);
  mobilityAP.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilitySta.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityAP.Install (apWifiNode);
  mobilitySta.Install (staWifiNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install (apWifiNode);
  stack.Install (staWifiNodes);
 
  Ipv4AddressHelper address;
  address.SetBase ("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer apInterface;
  apInterface = address.Assign (apDevice);
  Ipv4InterfaceContainer staInterface;
  staInterface = address.Assign (staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  /* We do not want any ARP packets */
  PopulateArpCache ();
  
  /** Install Applications **/
    /* DMG STA -->  DMG AP */


    for (uint32_t i = 0; i < staWifiNodes.GetN (); i++)
      {
        communicationPairList[staWifiNodes.Get (i)] = InstallApplications (staWifiNodes.Get (i), apWifiNode.Get (0),
                                                                           apInterface.GetAddress (0), i);
      }

  /* Print Traces */
  if (pcapTracing)
    {
      wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
      wifiPhy.EnablePcap ("Traces/"+ m_statsFile + "-AccessPoint", apDevice, false);
      wifiPhy.EnablePcap ("Traces/"+ m_statsFile + "-Station", staDevices, false);
    }
  
  /* Install FlowMonitor on all nodes */
    FlowMonitorHelper flowmon;
    Ptr<FlowMonitor> monitor = flowmon.InstallAll ();
    monitor = flowmon.GetMonitor(); // 4 lines for flow monitor enhancement
    monitor->SetAttribute ("DelayBinWidth", ns3::DoubleValue(0.001));
    monitor->SetAttribute ("JitterBinWidth", ns3::DoubleValue(0.001));
    monitor->SetAttribute ("PacketSizeBinWidth", ns3::DoubleValue(20));

  /* Stations */

  /* Callback for DMG STAs SLS */
    AsciiTraceHelper ascii;
    Ptr<OutputStreamWrapper> outputSlsPhaseRTS = ascii.CreateFileStream (path + m_statsFile + "-slsResults-RTS-" + runstr + ".csv");
    Ptr<OutputStreamWrapper> outputSlsPhaseNO_RTS = ascii.CreateFileStream (path + m_statsFile + "-slsResults-NO_RTS-" + runstr + ".csv");

    if (rts_cts_toggle == 1)
            {
    			*outputSlsPhaseRTS->GetStream () << "SRC_ID,DST_ID,SECTOR_ID,ANTENNA_ID,ROLE,BSS_ID,Timestamp" << std::endl;
            }
    else if (rts_cts_toggle == 0)
            {
    			*outputSlsPhaseNO_RTS->GetStream () << "SRC_ID,DST_ID,SECTOR_ID,ANTENNA_ID,ROLE,BSS_ID,Timestamp" << std::endl;
            }

    Ptr<WifiNetDevice> wifiNetDevice;
    Ptr<WifiNetDevice> wifiNetDevice_AP;
    Ptr<DmgApWifiMac> apWifiMac;
    Ptr<DmgStaWifiMac> wifiMac_SLS;
    Ptr<WifiPhy> wifiPhy_SLS;
    //Ptr<WifiPhyStateHelper> wifiPhy_Help;
    Ptr<DcaTxop> dcaTxop_F;
    NetDeviceContainer wifiDevices;
    wifiDevices.Add (apDevice);
    wifiDevices.Add (staDevices);
    Ptr<WifiRemoteStationManager> apRemoteStationManager;

  uint32_t nodeId;
    for (uint32_t i = 0; i < staDevices.GetN (); i++)
      {
        Ptr<Parameters> parameters = Create<Parameters> ();
        wifiNetDevice = StaticCast<WifiNetDevice> (staDevices.Get (i));
        wifiNetDevice_AP = StaticCast<WifiNetDevice> (apDevice.Get (0));
        wifiMac_SLS = StaticCast<DmgStaWifiMac> (wifiNetDevice->GetMac ());
        wifiMac_SLS->TraceConnectWithoutContext ("Assoc", MakeBoundCallback (&StationAssoicated, staWifiNodes.Get (i), wifiMac_SLS, apWifiMac));  //CHANGE F
        PointerValue ptr;
        Ptr<RegularWifiMac> rmac = DynamicCast<RegularWifiMac> (wifiMac_SLS);
        rmac->GetAttribute ("BE_EdcaTxopN", ptr);
        dcaTxop_F = ptr.Get<EdcaTxopN> ();

        wifiPhy_SLS = StaticCast<WifiPhy> (wifiNetDevice->GetPhy ());
        nodeId = wifiNetDevice->GetNode ()->GetId ();

        parameters->srcNodeID = nodeId;
        parameters->wifiMac = wifiMac_SLS;
        if (rts_cts_toggle == 1)
        	{
        		wifiMac_SLS->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, outputSlsPhaseRTS, parameters));
            }
        else if (rts_cts_toggle == 0)
        	{
        		wifiMac_SLS->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, outputSlsPhaseNO_RTS, parameters));
        	}
        dcaTxop_F->TraceConnectWithoutContext ("CwTrace", MakeBoundCallback (&CwTrace, parameters));


        apWifiMac = StaticCast<DmgApWifiMac> (wifiNetDevice_AP->GetMac ());

        apWifiMac->TraceConnectWithoutContext ("DTIStarted", MakeBoundCallback (&DataTransmissionIntervalStarted_Trick,  staWifiNodes.Get (i), apWifiMac, wifiMac_SLS));

      }

  /* Connect DMG PCP/AP trace */
  wifiNetDevice = StaticCast<WifiNetDevice> (apDevice.Get (0));
  apWifiMac = StaticCast<DmgApWifiMac> (wifiNetDevice->GetMac ());
  apRemoteStationManager = wifiNetDevice->GetRemoteStationManager ();
  Ptr<Parameters> parametersAP = Create<Parameters> ();
  nodeId = wifiNetDevice_AP->GetNode ()->GetId ();
  parametersAP->srcNodeID = nodeId;
  parametersAP->wifiMac = wifiMac_SLS;

  apWifiMac->TraceConnectWithoutContext ("BIStarted", MakeCallback (&BeaconIntervalStarted));
  if (rts_cts_toggle == 1)
  	  {
	  	  apWifiMac->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, outputSlsPhaseRTS, parametersAP));
      }
  else if (rts_cts_toggle == 0)
  	  {
	  	  apWifiMac->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, outputSlsPhaseNO_RTS, parametersAP));
  	  }

  Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/BE_EdcaTxopN/BackoffTrace", MakeCallback (&BackoffTrace));


  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacTxDrop", MakeCallback(&MacTxDrop_F));
  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacRxDrop", MakeCallback(&MacRxDrop_F));
  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyRxDrop", MakeCallback(&PhyRxDrop));
  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/State/State", MakeCallback(&PhyStateTrace));
  Config::Connect("/NodeList/*/ApplicationList/*/$ns3::PacketSink/DelayF",MakeCallback(&CalculateDelay));
  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/MacLow/TxPkt",MakeCallback(&CalculateDrop));
  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/MacLow/NavUpdate",MakeCallback(&Navupdate)); //To ADD for NAV update


   /* Print Output*/
  std::cout << "Time [s]" << '\t' << "Throughput [Mbps]" << std::endl;

  /* Schedule Throughput Calulcations */
  //Simulator::Schedule (Seconds (2.1), &CalculateThroughput);
  //Simulator::Schedule (Seconds (2.1), &CalculateDelay);
  Simulator::Schedule (Seconds (0.1), &CalculateThroughputAndDelay);
 // Simulator::Schedule (Seconds (0.1), &CalculateDelay);
  Simulator::Schedule (Seconds (0.1), &ThroughputMonitor, &flowmon, monitor);
  ThroughputMonitor(&flowmon, monitor);
  Simulator::Stop (Seconds (simulationTime + 0.101));
  Simulator::Run ();
  
  for (uint32_t i = 0; i < staWifiNodes.GetN (); i++)
        {
          Ptr<Node> node = staWifiNodes.Get (i);
          Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
          Ipv4Address addr = ipv4->GetAddress (1, 0).GetLocal ();

          map_IP2ID[addr] = i;
          //std::cout << "map_IP2ID[" << addr << "]: " << map_IP2ID[addr] << std::endl;
        }
  /* Print per flow statistics */
  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats ();

  	monitor->SerializeToXmlFile ("results_F.flowmon", true, true);
    std::ofstream myfile;
    if (rts_cts_toggle)
            {
    			myfile.open (path + m_statsFile + "-flow-Monitor-out-RTS" + runstr + ".csv");
            }
    else
    		{
    			myfile.open (path + m_statsFile + "-flow-Monitor-out-NO_RTS" + runstr + ".csv");
    		}
    myfile << "From," << "To," << "Tx_Packets," << "Tx_Bytes," << "Tx_Offered," << "Rx_Packets," << "Rx_Bytes," << "Throughput," << "Jitter," << "Delay," << "Packet_Loss," << "NodeID," << "Number of lost packets" << "\n";
    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
    	  Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
    	      //of << "Source" << "," << "Destination" << "," << "Tx Packets" << "," << "Tx Bytes" << "," << "TxOffered" << "," << "Rx Packets" << "," << "Rx Bytes" << "," << "Throughput" << "," << "Sum Jitter" << "," << "AverageDelay" << "," << "Packet Loss";
    	  myfile << t.sourceAddress << "," << t.destinationAddress << ",";
    	  myfile << i->second.txPackets << ",";
    	  myfile << i->second.txBytes << ",";
    	  myfile << i->second.txBytes * 8.0 / ((simulationTime - 1) * 1e6)  << " Mbps" << ",";
    	  myfile << i->second.rxPackets << ",";
    	  myfile << i->second.rxBytes << ",";
    	  myfile << i->second.rxBytes * 8.0 / ((simulationTime - 1) * 1e6)  << " Mbps" << ",";
    	  myfile << i->second.jitterSum << ",";
    	  if (i->second.rxPackets != 0)
    	  	  {
    		  	  myfile << i->second.delaySum/i->second.rxPackets << ",";
    	  	  }
    	  else
    	  	  {
    		  	  myfile << 0 << ",";
    	  	  }
    	  myfile << 100*((i->second.txPackets-i->second.rxPackets)*1.0)/i->second.txPackets << ",";

    	  myfile << map_IP2ID[t.sourceAddress] << ",";
    	  myfile << (i->second.txPackets-i->second.rxPackets) << "\n";

   	      
    }


    for (uint32_t i = 0; i < uint32_t(nodeUserNumber+1); i++)
    	{
            for (uint32_t j = 0; j < uint32_t(nodeUserNumber+1); j++)
            {

            	for (uint32_t k = 0; k < uint32_t(nodeUserNumber+1); k++)
            	{
            		//std::cout << " RTS Rcv: " << i << " From: " << j << " To: " << k << " Value: " << node_Pair_rts[i][j][k] << " " << std::endl;
            		if ((j != k) && (i != j) && (node_Pair_rts[i][j][k] != 0))
            		    	  	  {
            						//std::cout << " RTS Rcv: " << i << " From: " << j << " To: " << k << " Value: " << node_Pair_rts[i][j][k] << " " << std::endl;
            						m_navUpdateFile << i << "," << j << "," << k  << "," << node_Pair_rts[i][j][k] << "," << 0 << "\n";
            		    	  	  }
            	}
            }

        }
    for (uint32_t i = 0; i < uint32_t(nodeUserNumber+1); i++)
        	{
                for (uint32_t j = 0; j < uint32_t(nodeUserNumber+1); j++)
                {

                            	for (uint32_t k = 0; k < uint32_t(nodeUserNumber+1); k++)
                            	{
                            		if ((j != k) && (i != j) && (node_Pair_cts[i][j][k] != 0))
                            		     {

                            					//std::cout << " CTS Rcv: " << i << " From: " << j << " To: " << k << " Value: " << node_Pair_cts[i][j][k] << " " << std::endl;

                            					m_navUpdateFile << i << "," << j << "," << k  << "," << 0 << "," << node_Pair_cts[i][j][k] << "\n";
                            		     }
                            	}
                            }

            }

    for (uint32_t i = 0; i < uint32_t(nodeUserNumber+1); i++)
    {
    	m_MacDropFile <<  i << "," << VarMacDrop[i] <<  std::endl;
    	m_MacRxDropFile <<  i << "," << VarMacRxDrop[i] <<  std::endl;
    	m_PhyRxDropFile <<  i << "," << VarPhyRxDrop[i] <<  std::endl;
    }



    myfile.close ();

     m_cwStateFile.close ();
	 m_throughputStateFile.close();
     m_delayStateFile.close();
     m_delay2StateFile.close();
     m_dropStateFile.close();
     m_navUpdateFile.close();
     m_nodeAssocFile.close();
     m_BackOffFile.close ();
     m_MacDropFile.close ();
     m_MacRxDropFile.close ();
     m_PhyRxDropFile.close ();

     /* Log Phy State data */
     std::ofstream m_outFile;
     if (rts_cts_toggle)
     	 {
    	 	 m_outFile.open (path + m_statsFile + "-Phy-State-out-RTS" + runstr + ".csv");
     	 }
     else
     	 {
    	 	 m_outFile.open (path + m_statsFile + "-Phy-State-out-NO_RTS" + runstr + ".csv");
     	 }
     m_outFile << "reason,Time_TX,Time_RX,Time_CCABUSY,Time_IDLE," <<  std::endl;
     for (int i = 0; i < nodeUserNumber+1; ++i)
     {
    	 m_outFile << i << "," << timeTX[i] << "," << timeRX[i] <<   "," << timeCCA[i] << "," << timeIDLE[i] <<  std::endl;
     }
     m_outFile.close ();



     //std::ofstream logRTS(path + m_statsFile + "-Runs-logfile-RTS.csv", std::ios_base::app | std::ios_base::out);
     //std::ofstream logNO_RTS(path + m_statsFile + "-Runs-logfile-NO_RTS.csv", std::ios_base::app | std::ios_base::out);

     if (rts_cts_toggle == 1)
          	 {
    	 	 	 std::ofstream logRTS(path + m_statsFile + "-Runs-logfile-RTS.csv", std::ios_base::app | std::ios_base::out);
    	 	 	 if (flag_run == 0)
    	          {
    	     	 	 logRTS << "Run: " << run << ",Number of STA: " << nodeUserNumber << ",Simulation Time: " << simulationTime << ",Distance between STA and AP: " << AP_STA_DISTANCE << "\n";

    	          }
    	 	 	 else
    	      	 {
    	     	 	 logRTS << "Run: " << runstr << ",Number of STA: " << nodeUserNumber << ",Simulation Time: " << simulationTime << ",Distance between STA and AP: " << AP_STA_DISTANCE << "\n";

    	          }
          	 }
     else if (rts_cts_toggle == 0)
     	 	 {
    	 	 	 std::ofstream logNO_RTS(path + m_statsFile + "-Runs-logfile-NO_RTS.csv", std::ios_base::app | std::ios_base::out);
    	 	 	 if (flag_run == 0)
    	     	          {
    	     	     	 	 logNO_RTS << "Run: " << run << ",Number of STA: " << nodeUserNumber << ",Simulation Time: " << simulationTime << ",Distance between STA and AP: " << AP_STA_DISTANCE << "\n";

    	     	          }
    	     	 else
    	     	      	 {
    	     	     	 	 logNO_RTS << "Run: " << runstr << ",Number of STA: " << nodeUserNumber << ",Simulation Time: " << simulationTime << ",Distance between STA and AP: " << AP_STA_DISTANCE << "\n";

    	     	          }
         }

  Simulator::Destroy ();
  /* Print Results Summary */
  //std::cout << "Total number of received packets = " << packetSink->GetTotalReceivedPackets () << std::endl;
  //std::cout << "Total throughput [Mbps] = " << throughput/((simulationTime - 1) * 10) << std::endl;

  return 0;
}
