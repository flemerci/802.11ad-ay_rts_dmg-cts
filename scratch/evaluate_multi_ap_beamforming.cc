/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "common-functions.h"

/**
 * Simulation Objective:
 * This script is used to assess the performance of CSMA/CA protocol when multiple APs share the wireless medium.
 *
 * Network Topology:
 * The scenario consists of 2 DMG STAs and two DMG PCP/APs. Each DMG STA associates with its corresponding DMG PCP/AP as following:
 *
 *   DMG AP [1] (-2,+2)                 DMG AP [2] (+2,+2)
 *
 *
 *
 *
 *
 *  DMG STA [1] (-2,-2)                 DMG STA [2] (+2,-2)
 *
 * Simulation Description:
 * Each STA uses simple analytical code with single phased antenna array with different number of sectors.
 *
 * Running the Simulation:
 * To run the script with the default parameters:
 * ./waf --run "evaluate_multi_ap_operation --pcap=1"
 *
 * Simulation Output:
 * The simulation generates the following traces:
 * 1. PCAP traces for each station. From the PCAP files, we can see the allocation of beamforming service periods.
 * 2. SNR Dump for each sector.
 */

NS_LOG_COMPONENT_DEFINE ("MultiAPOperation");

using namespace ns3;
using namespace std;

/* Network Nodes */
Ptr<WifiNetDevice> ap1WifiNetDevice;
Ptr<WifiNetDevice> ap2WifiNetDevice;
Ptr<WifiNetDevice> sta1WifiNetDevice;
Ptr<WifiNetDevice> sta2WifiNetDevice;

Ptr<DmgApWifiMac> ap1WifiMac;
Ptr<DmgApWifiMac> ap2WifiMac;
Ptr<DmgStaWifiMac> sta1WifiMac;
Ptr<DmgStaWifiMac> sta2WifiMac;

/*** Access Point Variables ***/
uint8_t assoicatedStations = 0;           /* Total number of assoicated stations with the AP */

void
StationAssoicated (Ptr<DmgStaWifiMac> staWifiMac, Mac48Address address, uint16_t aid)
{
  std::cout << "DMG STA: " << staWifiMac->GetAddress () << " associated with DMG PCP/AP: " << address;
  std::cout << " with Association ID (AID) = " << aid << std::endl;
  assoicatedStations++;
}

void
SLSCompleted (Ptr<DmgWifiMac> wifiMac, Mac48Address address, ChannelAccessPeriod accessPeriod,
              BeamformingDirection beamformingDirection, bool isInitiatorTxss, bool isResponderTxss,
              SECTOR_ID sectorId, ANTENNA_ID antennaId)
{
  if (wifiMac->GetTypeOfStation () == DMG_AP)
    {
      std::cout << "DMG AP " << wifiMac->GetAddress () << " completed SLS phase with DMG STA " << address << std::endl;
    }
  else
    {
      std::cout << "DMG STA " << wifiMac->GetAddress () << " completed SLS phase with DMG AP " << address << std::endl;
    }
  std::cout << "Best Tx Antenna Configuration: SectorID=" << uint16_t (sectorId) << ", AntennaID=" << uint16_t (antennaId) << std::endl;
}

int
main (int argc, char *argv[])
{
  bool enableJitter = true;                     /* Enable Beacon Interval Jitter to randomize Beacon Interval start time. */
  std::string beaconJitter = "ns3::UniformRandomVariable[Min=0|Max=10]";
  bool verbose = false;                         /* Print Logging Information. */
  double simulationTime = 1;                    /* Simulation time in seconds. */
  bool pcapTracing = false;                     /* PCAP Tracing is enabled or not. */

  /* Command line argument parser setup. */
  CommandLine cmd;
  cmd.AddValue ("enableJitter", "Enable Beacon Interval Jitter to randomize Beacon Interval start time", enableJitter);
  cmd.AddValue ("beaconJitter", "Beacon Jitter value in MicroSeconds", beaconJitter);
  cmd.AddValue ("verbose", "Turn on all WifiNetDevice log components", verbose);
  cmd.AddValue ("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue ("pcap", "Enable PCAP Tracing", pcapTracing);
  cmd.Parse (argc, argv);

  /* Global params: no fragmentation, no RTS/CTS, fixed rate for all packets */
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("999999"));
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("999999"));

  /**** DmgWifiHelper is a meta-helper ****/
  DmgWifiHelper wifi;

  /* Basic setup */
  wifi.SetStandard (WIFI_PHY_STANDARD_80211ad);

  /* Turn on logging */
  if (verbose)
    {
      wifi.EnableLogComponents ();
      LogComponentEnable ("MultiAPOperation", LOG_LEVEL_ALL);
    }

  /**** Setup Channel ****/
  DmgWifiChannelHelper wifiChannel ;
  /* Simple propagation delay model */
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  /* Friis model with standard-specific wavelength */
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel", "Frequency", DoubleValue (60.48e9));

  /**** Setup physical layer ****/
  DmgWifiPhyHelper wifiPhy = DmgWifiPhyHelper::Default ();
  /* Nodes will be added to the channel we set up earlier */
  wifiPhy.SetChannel (wifiChannel.Create ());
  /* All nodes transmit at 10 dBm == 10 mW, no adaptation */
  wifiPhy.Set ("TxPowerStart", DoubleValue (10.0));
  wifiPhy.Set ("TxPowerEnd", DoubleValue (10.0));
  wifiPhy.Set ("TxPowerLevels", UintegerValue (1));
  /* Set operating channel */
  wifiPhy.Set ("ChannelNumber", UintegerValue (2));
  /* Sensitivity model includes implementation loss and noise figure */
  wifiPhy.Set ("CcaMode1Threshold", DoubleValue (-79));
  wifiPhy.Set ("EnergyDetectionThreshold", DoubleValue (-79 + 3));

  /* Make four nodes and set them up with the PHY and the MAC */
  NodeContainer wifiNodes;
  NetDeviceContainer apDevices;
  NetDeviceContainer staDevices;
  wifiNodes.Create (4);
  Ptr<Node> apNode1 = wifiNodes.Get (0);
  Ptr<Node> apNode2 = wifiNodes.Get (1);
  Ptr<Node> staNode1 = wifiNodes.Get (2);
  Ptr<Node> staNode2 = wifiNodes.Get (3);

  /* Add a DMG upper MAC */
  DmgWifiMacHelper wifiMac = DmgWifiMacHelper::Default ();

  /* Install DMG PCP/AP [1] */
  NetDeviceContainer netDevice;
  Ssid ssid1 ("DMG_AP1");
  Ssid ssid2 ("DMG_AP2");
  wifiMac.SetType ("ns3::DmgApWifiMac",
                   "Ssid", SsidValue (ssid1),
                   "BE_MaxAmpduSize", UintegerValue (0),
                   "SSSlotsPerABFT", UintegerValue (8), "SSFramesPerSlot", UintegerValue (8),
                   "BeaconInterval", TimeValue (MicroSeconds (102400)),
                   "EnableBeaconJitter", BooleanValue (enableJitter),
                   "ATIPresent", BooleanValue (false));

  if (enableJitter)
    {
      wifiMac.SetAttrbiute ("BeaconJitter", StringValue (beaconJitter));
    }

  /* Set Analytical Codebook for the DMG PCP/APs */
  wifi.SetCodebook ("ns3::CodebookAnalytical",
                    "CodebookType", EnumValue (SIMPLE_CODEBOOK),
                    "Antennas", UintegerValue (1),
                    "Sectors", UintegerValue (16));

  netDevice = wifi.Install (wifiPhy, wifiMac, apNode1);
  apDevices.Add (netDevice);

  /* Install DMG PCP/AP [2] */
  wifiMac.SetType ("ns3::DmgApWifiMac",
                   "Ssid", SsidValue (ssid2),
                   "BE_MaxAmpduSize", UintegerValue (0),
                   "SSSlotsPerABFT", UintegerValue (8), "SSFramesPerSlot", UintegerValue (8),
                   "BeaconInterval", TimeValue (MicroSeconds (102400)),
                   "EnableBeaconJitter", BooleanValue (true),
                   "ATIPresent", BooleanValue (false));

  if (enableJitter)
    {
      wifiMac.SetAttrbiute ("BeaconJitter", StringValue (beaconJitter));
    }

  netDevice = wifi.Install (wifiPhy, wifiMac, apNode2);
  apDevices.Add (netDevice);

  /* Install DMG STA Node [1] */
  wifiMac.SetType ("ns3::DmgStaWifiMac",
                   "Ssid", SsidValue (ssid1),
                   "ActiveProbing", BooleanValue (false),
                   "BE_MaxAmpduSize", UintegerValue (0));

  /* Set Analytical Codebook for the DMG STAs */
  wifi.SetCodebook ("ns3::CodebookAnalytical",
                    "CodebookType", EnumValue (SIMPLE_CODEBOOK),
                    "Antennas", UintegerValue (1),
                    "Sectors", UintegerValue (8));

  netDevice = wifi.Install (wifiPhy, wifiMac, staNode1);
  staDevices.Add (netDevice);

  /* Install DMG STA Node [2] */
  wifiMac.SetType ("ns3::DmgStaWifiMac",
                   "Ssid", SsidValue (ssid2),
                   "ActiveProbing", BooleanValue (false),
                   "BE_MaxAmpduSize", UintegerValue (0));
  netDevice = wifi.Install (wifiPhy, wifiMac, staNode2);
  staDevices.Add (netDevice);

  /* Setting mobility model */
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (-2.0, +2.0, 0.0));   /* DMG PCP/AP [1] */
  positionAlloc->Add (Vector (+2.0, +2.0, 0.0));   /* DMG PCP/AP [2] */
  positionAlloc->Add (Vector (-2.0, -2.0, 0.0));   /* DMG STA [1] */
  positionAlloc->Add (Vector (+2.0, -2.0, 0.0));   /* DMG STA [2] */

  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install (wifiNodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer apsInterfaces;
  apsInterfaces = address.Assign (apDevices);
  Ipv4InterfaceContainer stasInterfaces;
  stasInterfaces = address.Assign (staDevices);

  /* Populate routing table */
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  /* We do not want any ARP packets */
  PopulateArpCache ();

  /* Enable Traces */
  if (pcapTracing)
    {
      wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
      wifiPhy.EnablePcap ("Traces/AccessPoint", apDevices, false);
      wifiPhy.EnablePcap ("Traces/StaNode", staDevices, false);
    }

  /* Get WifiNetDevices */
  ap1WifiNetDevice = StaticCast<WifiNetDevice> (apDevices.Get (0));
  ap2WifiNetDevice = StaticCast<WifiNetDevice> (apDevices.Get (1));
  sta1WifiNetDevice = StaticCast<WifiNetDevice> (staDevices.Get (0));
  sta2WifiNetDevice = StaticCast<WifiNetDevice> (staDevices.Get (1));

  ap1WifiMac = StaticCast<DmgApWifiMac> (ap1WifiNetDevice->GetMac ());
  ap2WifiMac = StaticCast<DmgApWifiMac> (ap2WifiNetDevice->GetMac ());
  sta1WifiMac = StaticCast<DmgStaWifiMac> (sta1WifiNetDevice->GetMac ());
  sta2WifiMac = StaticCast<DmgStaWifiMac> (sta2WifiNetDevice->GetMac ());

  /** Connect Traces **/
  ap1WifiMac->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, ap1WifiMac));
  ap2WifiMac->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, ap2WifiMac));
  sta1WifiMac->TraceConnectWithoutContext ("Assoc", MakeBoundCallback (&StationAssoicated, sta1WifiMac));
  sta2WifiMac->TraceConnectWithoutContext ("Assoc", MakeBoundCallback (&StationAssoicated, sta2WifiMac));
  sta1WifiMac->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, sta1WifiMac));
  sta2WifiMac->TraceConnectWithoutContext ("SLSCompleted", MakeBoundCallback (&SLSCompleted, sta2WifiMac));

  Simulator::Stop (Seconds (10));
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}
