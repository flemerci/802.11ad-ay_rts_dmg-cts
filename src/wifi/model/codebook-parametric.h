/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef CODEBOOK_PARAMETRIC_H
#define CODEBOOK_PARAMETRIC_H

#include "ns3/object.h"
#include "codebook.h"
#include <complex>
#include <iostream>

namespace ns3 {

typedef std::complex<double> Complex;                   //<! Typedef for a complex number.
typedef std::vector<Complex> WeightsVector;             //<! Typedef for an antenna weights vector.
typedef WeightsVector::const_iterator WeightsVectorCI;  //<! Typedef for an iterator for AWV.
typedef Complex** ArrayPattern;                         //<! Typedef for an phased antenna array pattern.
typedef Directivity** DirectivityMatrix;                //<! Typedef for phased antenna directivity matrix.
typedef Complex*** SteeringVector;                      //<! Typedef for phased antenna steering vector.

/**
 * Interface for generating radiation pattern using parametric method.
 */
struct ParametricPatternConfig : virtual public PatternConfig {
public:
  /**
   * Get the array pattern of the antenna array after applying specific antenna weights.
   * \return The array pattern of the antenna array.
   */
  ArrayPattern GetArrayPattern (void) const;

public:
  WeightsVector elementsWeights;                //!< Weights that define the directivity of the phased antenna array.

protected:
  friend class CodebookParametric;
  ArrayPattern arrayPattern;                    //<! Phased Antenna Array Factcor after applying the weights vector.
  DirectivityMatrix directivity;                //<! Phased Antenna Array Directivity after applying the weights vector.

};

/**
 * Parametric AWV Configuration.
 */
struct Parametric_AWV_Config : virtual public AWV_Config, virtual public ParametricPatternConfig {
};

/**
 * Parametric Sector configuration.
 */
struct ParametricSectorConfig : virtual public SectorConfig, virtual public ParametricPatternConfig {
};

struct ParametricAntennaConfig : public PhasedAntennaArrayConfig {
public:
  uint16_t elements;                          //!< Number of the antenna elements in the antenna array.
  SteeringVector steeringVector;              //!< Steering matrix of MxNxL where L is the number of antenna elements
                                              // and M and N represent the azimuth and elevation angles of the incoming plane wave.

  DirectivityMatrix singleElementDirectivity; //!< The directivity of a single antenna element in linear scale.
  WeightsVector quasiOmniWeights;             //!< Weights that define the directivity of the Quasi-omni pattern.
  uint8_t amplitudeQuantizationBits;          //!< Number of bits for quanitizing gain (amplitude) value.
  uint8_t phaseQuantizationBits;              //!< Number of bits for quanitizing phase value.

  /**
   * Calculate directivity towards certain direction for a given antenna weights vector.
   * \param azimuth The azimuth angle in degrees to calculate the directivity towards.
   * \param elevation The elevation angle in degrees to calculate the directivity towards.
   * \param weightsVector The antenna weights vector to apply to the steering vector of the antenna array.
   * \return the antenna array directivity in linear scale.
   */
  double CalculateDirectivity (double azimuth, double elevation, WeightsVector &weightsVector);
  /**
   * Calculate directivity towards a certain direction using the steering vector.
   * \param azimuth The azimuth angle in degrees to calculate the directivity towards.
   * \param elevation The elevation angle in degrees to calculate the directivity towards.
   * \return the antenna array directivity in linear scale.
   */
  double CalculateDirectivityForDirection (double azimuth, double elevation);
  /**
   * Calculate antenna array factor and directivity for a given antenna weights vector.
   * \param weightsVector The weights of the antenna elements.
   * \param steeringVector The steering vector of the corresponding phased antenna array.
   * \param arrayPattern A matrix of antenna array pattern (Complex values).
   * \param directivity A matrix of antenna array directivity in dBi.
   */
  void CalculateDirectivity (WeightsVector *weights, ArrayPattern &arrayPattern, DirectivityMatrix &directivity);
  /**
   * Get Quasi-Omni Antenna Array Pattern.
   */
  ArrayPattern GetQuasiOmniArrayPattern (void) const;

private:
  friend class CodebookParametric;
  ArrayPattern quasiOmniArrayPattern;         //<! Quasi-omni pattern array factor after applying the weights.
  DirectivityMatrix quasiOmniDirectivity;     //<! Quasi-omni pattern directivity after applying the weights.
  double phaseQuantizationStepSize;           //<! phase quantization step size.

};

/**
 * \brief Codebook for Sectors generated using antenna Array Factor (AF). The user describes the steering vector
 * of the antenna. In addition, the user defines list of antenna weight to describe shapes.
 */
class CodebookParametric : public Codebook
{
public:
  static TypeId GetTypeId (void);

  CodebookParametric (void);
  virtual ~CodebookParametric (void);
  /**
   * Load code book from a text file.
   */
  void LoadCodebook (std::string filename);
  /**
   * Get transmit antenna gain dBi.
   * \param angle The angle towards the intended receiver.
   * \return Transmit antenna gain in dBi based on the steering angle.
   */
  double GetTxGainDbi (double angle);
  /**
   * Get transmit antenna gain in dBi.
   * \param angle The angle towards the intended receiver.
   * \return Receive antenna gain in dBi based on the steering angle.
   */
  double GetRxGainDbi (double angle);
  /**
   * Get transmit antenna gain in dBi.
   * \param azimuth The azimuth angle towards the intedned receiver.
   * \param elevation The elevation angle towards the intedned receiver.
   * \return Transmit antenna gain in dBi based on the steering angle.
   */
  double GetTxGainDbi (double azimuth, double elevation);
  /**
   * Get receive antenna gain in dBi.
   * \param azimuth The azimuth angle towards the intedned receiver.
   * \param elevation The elevation angle towards the intedned receiver.
   * \return Receive antenna gain in dBi based on the steering angle.
   */
  double GetRxGainDbi (double azimuth, double elevation);
  /**
   * Get the total number of sectors for a specific phased antenna array.
   * \param antennaID The ID of the phased antenna array.
   * \return The number of sectors for the specified phased antenna array.
   */
  uint8_t GetNumberSectorsPerAntenna (AntennaID antennaID) const;
  /**
   * Update sector antenna weight vector.
   * \param antennaID The ID of the antenna array.
   * \param sectorID The ID of the sector to update its pattern.
   * \param weightsVector Vector of the antenna weights (Amplitude and phase excitation).
   */
  void UpdateSectorWeights (AntennaID antennaID, SectorID sectorID, WeightsVector &weightsVector);
  /**
   * Update antenna weights corresponding to the quasi-omni pattern.
   * \param antennaID The ID of the antenna array.
   * \param weightsVector Vector of the antenna weights.
   */
  void UpdateQuasiOmniWeights (AntennaID antennaID, WeightsVector &weightsVector);
  /**
   * Append a new sector to for a specific antenna array or update an existing
   * sector in the codebook. If the sector is new then the number of sectors
   * should not exceed 64 sectors per antenna array and 128 across all the antennas.
   * \param antennaID The of the phased antenna array.
   * \param sectorID The ID of the sector to ID.
   * \param sectorUsage The usage of the sector.
   * \param sectorType The type of the sector.
   * \param weightsVector The antenna weights vector of the sector.
   */
  void AppendSector (AntennaID antennaID, SectorID sectorID, SectorUsage sectorUsage,
                     SectorType sectorType, WeightsVector &weightsVector);
  /**
   * Print Codebook Content to the standard output.
   * \printAWVs To indicate whether to print the directivity of the custom AWVs.
   */
  void PrintCodebookContent (bool printAWVs);
  /**
   * Print directivity of the custom AWVs for specfic sector within a specific antenna array.
   * \param antennaID The ID of the antenna array.
   * \param sectorID The ID of the sector to add custom AWV to.
   */
  void PrintAWVs (AntennaID antennaID, SectorID sectorID);
  /**
   * Change phased antenna array orientation.
   * \param antennaID The ID of the antenna array.
   * \param azimuthOrientation The new azimuth orientation of the antenna array in degrees.
   * \param elevationOrientation The new elevation orientation of the antenna array in degrees.
   */
  void ChangeAntennaOrientation (AntennaID antennaID, double azimuthOrientation, double elevationOrientation);
  /**
   * Append a custom AWV for the Beam Refinement and Beam Tracking (BT) phases.
   * \param antennaID The ID of the antenna array.
   * \param sectorID The ID of the sector to add custom AWV to.
   * \param weightsVector The custom antenna weight vectors.
   */
  void AppendBeamRefinementAwv (AntennaID antennaID, SectorID sectorID, WeightsVector &weightsVector);
  /**
   * Append a custom AWV for the Beam Refinement and Beam Tracking (BT) phases.
   * \param antennaID The ID of the antenna array.
   * \param sectorID The ID of the sector to ad custom AWV to.
   * \param steeringAngleAzimuth The steering angle of the custom AWV in the azimuth plane.
   * \param steeringAngleElevation The steering angle of the custom AWV in the elevation plane.
   */
  void AppendBeamRefinementAwv (AntennaID antennaID, SectorID sectorID,
                                double steeringAngleAzimuth, double steeringAngleElevation);
  /**
   * Get the number of antenna elements in specific antenna array.
   * \param antennaID The ID of the antenna array.
   * \return The number of the antenna elements in the antenna array.
   */
  uint16_t GetNumberOfElements (AntennaID antennaID) const;
  /**
   * Get current active transmit antenna array pattern.
   * \return The array pattern of the transmit phased antenna array.
   */
  ArrayPattern GetTxAntennaArrayPattern (void);
  /**
   * Get current active receive antenna array pattern.
   * \return The array pattern of the receive phased antenna array.
   */
  ArrayPattern GetRxAntennaArrayPattern (void);

private:
  /**
   * Print antenna array directivity in the azimuth plane for a specific sector.
   * \param directivity The pre-calculated directivity for a specific sector.
   */
  void PrintDirectivity (DirectivityMatrix directivity) const;
  /**
   * Get transmission gain in dBi based on the selected antenna and sector.
   * \param azimuth The azimuth angle towards the peer device.
   * \param elevation The elevation angle towards the peer device.
   * \param directivity The directivity of the sector after multiplying the antenna weights with the steering vector.
   * \return Transmit/Receive antenna gain in dBi based on the azimuth angle and the selected sector.
   */
  double GetGainDbi (double azimuth, double elevation, DirectivityMatrix directivity) const;
  /**
   * Set Codebook File Name.
   * \param fileName The name of the codebook file to load.
   */
  void SetCodebookFileName (std::string fileName);
  /**
   * Read Antenna Weights Vector for a predefined antenna pattern.
   * \param file The file from where to read the values.
   * \param elements The number of antenna elements in the antenna array.
   * \return A weight vector that includes the excitation (Phase and amplitude) for each antenna element.
   */
  WeightsVector ReadAntennaWeightsVector (std::ifstream &file, double elements);

};

} // namespace ns3

#endif /* CODEBOOK_PARAMETRIC_H */
