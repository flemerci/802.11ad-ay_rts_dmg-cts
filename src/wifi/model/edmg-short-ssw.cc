/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */
#include "ns3/address-utils.h"
#include "ns3/log.h"
#include "ns3/simulator.h"

#include "edmg-short-ssw.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("ShortSSW");

NS_OBJECT_ENSURE_REGISTERED (ShortSSW);

ShortSSW::ShortSSW ()
  : m_packetType (SHORT_SSW),
    m_transmissionDirection (BEAMFORMING_INITIATOR),
    m_addressingMode (),
    m_sourceAID (0),
    m_destinationAID (0),
    m_cdown (0),
    m_rfChainID (0),
    m_bssID (0),
    m_unassociated (false),
    m_setupDuration (0),
    m_feedback (0)
{
  NS_LOG_FUNCTION (this);
}

ShortSSW::~ShortSSW ()
{
  NS_LOG_FUNCTION (this);
}

TypeId
ShortSSW::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::ShortSSW")
    .SetParent<Header> ()
    .AddConstructor<ShortSSW> ()
    ;
  return tid;
}

TypeId
ShortSSW::GetInstanceTypeId (void) const
{
  NS_LOG_FUNCTION (this);
  return GetTypeId ();
}

void ShortSSW::Print (std::ostream &os) const
{
  NS_LOG_FUNCTION (this << &os);
}

uint32_t
ShortSSW::GetSerializedSize () const
{
  NS_LOG_FUNCTION (this);
  return 6;
}

void
ShortSSW::Serialize (Buffer::Iterator start) const
{
  NS_LOG_FUNCTION (this << &start);
  uint32_t value1 = 0;
  uint16_t value2 = 0;
  value1 |= (m_packetType & 0x1) << 31;
  value1 |= (m_transmissionDirection & 0x1) << 30;
  if ((m_transmissionDirection == BEAMFORMING_INITIATOR) && (m_addressingMode == INDIVIDUAL_ADRESS))
    {
      value1 |= (m_addressingMode & 0x1) << 29;
      value2 |= (m_bssID & 0x3FF) << 5;
    }
  else if ((m_transmissionDirection == BEAMFORMING_INITIATOR) && (m_addressingMode == GROUP_ADDRESS))
    {
      value1 |= (m_addressingMode & 0x1) << 29;
      value2 |= (m_setupDuration & 0x3FF) << 5;
    }
  else if (m_transmissionDirection == BEAMFORMING_RESPONDER)
    {
      value2 |= (m_feedback & 0x3FF) << 4;
    }
  value1 |= m_sourceAID << 21;
  value1 |= m_destinationAID << 13;
  value1 |= (m_cdown & 0x3FF) << 2;
  value1 |= (m_rfChainID >> 1) & 0x3;
  value2 |= (m_rfChainID & 0x1) << 15;
  start.WriteHtolsbU32 (value1);
  start.WriteHtolsbU16 (value2);
}

uint32_t
ShortSSW::Deserialize (Buffer::Iterator start)
{
  NS_LOG_FUNCTION (this << &start);
  Buffer::Iterator i = start;
  uint32_t value1 = 0;
  uint16_t value2 = 0;
  value1 = i.ReadLsbtohU32 ();
  value2 = i.ReadLsbtohU16 ();
  m_packetType = static_cast<BeamformingPacketType> ((value1 >> 31) & 0x1);
  m_transmissionDirection = static_cast<TransmissionDirection> ((value1 >> 30) & 0x1);
  if (m_transmissionDirection == BEAMFORMING_INITIATOR)
    {
      m_addressingMode = static_cast<AddressingMode> ((value1 >> 29) & 0x1);
      if (m_addressingMode == INDIVIDUAL_ADRESS)
        {
          m_bssID = (value2 >> 5) & 0x3FF;
        }
      else
        {
          m_setupDuration = (value2 >> 5) & 0x3FF;
        }
    }
  else
    {
      m_feedback = (value2 >> 4) & 0x3FF;
    }
  m_sourceAID = (value1 >> 21) & 0xFF;
  m_destinationAID = (value1 >> 13) & 0xFF;
  m_cdown = (value1 >> 2) & 0x3FF;
  m_rfChainID |= (value1 & 0x3) << 1;
  m_rfChainID |= (value2 >> 15) & 0x1;
  return i.GetDistanceFrom (start);
}

//void
//ShortSSW::GenerateShortScrambledBSSID (Mac48Address bssid)
//{

//}

void
ShortSSW::SetPacketType (BeamformingPacketType type)
{
  m_packetType = type;
}

void
ShortSSW::SetDirection (TransmissionDirection direction)
{
  m_transmissionDirection = direction;
}

void
ShortSSW::SetAddressingMode (AddressingMode mode)
{
  m_addressingMode = mode;
}

void
ShortSSW::SetSourceAID (uint8_t aid)
{
  m_sourceAID = aid;
}

void
ShortSSW::SetDestinationAID (uint8_t aid)
{
  m_destinationAID = aid;
}

void
ShortSSW::SetCDOWN (uint16_t cdown)
{
  m_cdown = cdown;
}

void
ShortSSW::SetRFChainID (uint8_t id)
{
  m_rfChainID = id;
}

void
ShortSSW::SetShortScrambledBSSID (uint16_t bssid)
{
  m_bssID = bssid;
}

void
ShortSSW::SetAsUnassociated (bool value)
{
  m_unassociated = value;
}

void
ShortSSW::SetSetupDuration (uint16_t duration)
{
  m_setupDuration = duration;
}

void
ShortSSW::SetShortSSWFeedback (uint16_t feedback)
{
  m_feedback = feedback;
}

BeamformingPacketType
ShortSSW::GetPacketType (void) const
{
  return m_packetType;
}

TransmissionDirection
ShortSSW::GetDirection (void) const
{
  return m_transmissionDirection;
}

AddressingMode
ShortSSW::GetAddressingMode (void) const
{
  return m_addressingMode;
}

uint8_t
ShortSSW::GetSourceAID (void) const
{
  return m_sourceAID;
}

uint8_t
ShortSSW::GetDestinationAID (void) const
{
  return m_destinationAID;
}

uint16_t
ShortSSW::GetCDOWN (void) const
{
  return m_cdown;
}

uint8_t
ShortSSW::GetRFChainID (void) const
{
  return m_rfChainID;
}

uint16_t
ShortSSW::GetShortScrambledBSSID (void) const
{
  return m_bssID;
}

bool
ShortSSW::GetUnassociated (void) const
{
  return m_unassociated;
}

uint16_t
ShortSSW::GetSetupDuration (void) const
{
  return m_setupDuration;
}

uint16_t
ShortSSW::GetShortSSWFeedback (void) const
{
  return m_feedback;
}

} // namespace ns3
