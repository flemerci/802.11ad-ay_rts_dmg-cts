/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hany Assasa <hany.assasa@gmail.com>
 */

#ifndef QD_PROPAGATION_DELAY_H
#define QD_PROPAGATION_DELAY_H

#include <ns3/mobility-model.h>
#include <ns3/propagation-delay-model.h>

#include <map>

namespace ns3 {

typedef std::vector<double> doubleVector_t;
typedef std::pair<uint32_t, uint32_t> CommunicatingPair;                //!< Typedef for identifying communicating pair.
typedef std::map<CommunicatingPair, doubleVector_t> PropagationDelays;  //!< Check whether trace files have been loaded or not.
typedef PropagationDelays::iterator PropagationDelays_I;                //!< Typedef for iterator over traces files.

class QdPropagationDelay : public PropagationDelayModel
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  /**
   * Use the default parameters from PropagationDelayConstantSpeed.
   */
  QdPropagationDelay ();
  /**
   * Get Delay
   * \param a
   * \param b
   * \return
   */
  virtual Time GetDelay (Ptr<MobilityModel> a, Ptr<MobilityModel> b) const;
  /**
   * \param speed the new speed (m/s)
   */
  void SetSpeed (double speed);
  /**
   * \returns the current propagation speed (m/s).
   */
  double GetSpeed (void) const;

private:
  virtual int64_t DoAssignStreams (int64_t stream);
  /**
   * Read Q-D Channel propagation delay values.
   * \param indexTx The ID of the Tx node.
   * \param indexRx The ID of the Rx node.
   */
  doubleVector_t ReadQDPropagationDelays (uint16_t indexTx, uint16_t indexRx) const;
  /**
   * Set Q-D Channel model folder path.
   * \param folderName The path to the Q-D Channel files.
   */
  void SetQdModelFolder (std::string folderName);
  /**
   * Set starting distance in trace file.
   * \param startDistance The starting distance in the provided trace file.
   */
  void SetStartDistance (uint16_t startDistance);

private:
  std::string m_qdFolder;                     //!< Folder that contains all the Q-D Channel model files.
  double m_speed;                             //!< Trace/Node speed.
  uint16_t m_startDistance;                   //!< Starting point in the trace file.
  mutable uint16_t m_currentIndex;            //!< Current index in the trace file.
  mutable PropagationDelays m_delays;         //!< Status of the traces files.

};

}  //namespace ns3

#endif /* QD_PROPAGATION_DELAY_H */
