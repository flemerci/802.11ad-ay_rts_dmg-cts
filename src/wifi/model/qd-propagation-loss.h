/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015, NYU WIRELESS, Tandon School of Engineering, New York University
 * Copyright (c) 2018-2019 National Institute of Standards and Technology (NIST)
 * Copyright (c) 2015-2019 IMDEA Networks Institute
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Marco Mezzavilla <mezzavilla@nyu.edu>
 *          Sourjya Dutta <sdutta@nyu.edu>
 *          Russell Ford <russell.ford@nyu.edu>
 *          Menglei Zhang <menglei@nyu.edu>
 *
 * The original file is adapted from NYU Channel Ray-Tracer.
 * Modified By: Tanguy Ropitault <tanguy.ropitault@gmail.com>
 *              Hany Assasa <hany.assasa@gmail.com>
 *
 * Certain portions of this software were contributed as a public
 * service by the National Institute of Standards and Technology (NIST)
 * and are not subject to US Copyright.  Such contributions are provided
 * “AS-IS” and may be used on an unrestricted basis.  To the extent
 * foreign copyright exists,  such contributions are subject to the
 * GNU General Public License version 2, as consistent with Federal
 * law. Individual source files clarify to which portion they belong.
 */

#ifndef QD_CHANNEL_H
#define QD_CHANNEL_H

#include <ns3/angles.h>
#include <ns3/mobility-model.h>
#include <ns3/net-device.h>
#include <ns3/net-device-container.h>
#include <ns3/random-variable-stream.h>
#include <ns3/spectrum-propagation-loss-model.h>
#include <ns3/spectrum-signal-parameters.h>
#include <ns3/spectrum-value.h>

#include <complex>
#include <map>
#include <tuple>

#include "codebook-parametric.h"

namespace ns3 {

typedef std::vector<double> doubleVector_t;
typedef std::vector<std::complex<double> > complexVector_t;
typedef std::vector<complexVector_t> complex2DVector_t;
typedef std::vector<doubleVector_t> double2DVector_t;
typedef std::vector<float> floatVector_t;
typedef std::vector<floatVector_t> float2DVector_t;

struct AnglesTransformed {
  double elevation;
  double azimuth;
};

typedef std::tuple<AntennaID, bool, uint8_t> AntennaConfig;             //!< Generic antenna array configuration tuple.
typedef AntennaConfig AntennaConfigTx;                                  //!< Transmit Antenna array configuration tuple.
typedef AntennaConfig AntennaConfigRx;                                  //!< Receive Antenna array configuration tuple.
typedef std::tuple<Ptr<NetDevice>, Ptr<NetDevice>, AntennaConfigTx, AntennaConfigRx> LinkConfiguration;   //!< Link Configuration key.
typedef std::map<LinkConfiguration, Ptr<SpectrumValue> > ChannelMatrix; //!< Channel matrix defining channel gain for all the possible combinations in the scenario.
typedef ChannelMatrix::iterator ChannelMatrix_I;                        //!< Typedef for iterator over channel matrix.
typedef ChannelMatrix::const_iterator ChannelMatrix_CI;                 //!< Typedef for constant iterator over channel matrix.
typedef std::pair<uint32_t, uint32_t> CommunicatingPair;                //!< Typedef for identifying communicating pair.
typedef std::vector<CommunicatingPair> TraceFiles;                      //!< Check whether trace files have been loaded or not.
typedef TraceFiles::iterator TraceFiles_I;                              //!< Typedef for iterator over traces files.

class QdPropagationLossModel : public SpectrumPropagationLossModel
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);

  QdPropagationLossModel ();
  virtual ~QdPropagationLossModel ();

  /**
   * Get Current Trace Index.
   * \return Return the trace index in Q-D Channel.
   */
  uint16_t GetCurrentTraceIndex (void) const;

protected:
  virtual void DoDispose ();

private:
  Ptr<SpectrumValue> DoCalcRxPowerSpectralDensity (Ptr<const SpectrumValue> txPsd,
                                                   Ptr<const MobilityModel> a,
                                                   Ptr<const MobilityModel> b) const;
  /**
   * Initialize Q-D Channel model parameters.
   * \param a Mobility model of the transmitter.
   * \param b Mobility model of the receivers.
   * \param indexTx The ID of the Tx node.
   * \param indexRx The ID of the Rx node.
   */
  void InitializeQDModelParameters (Ptr<const MobilityModel> a, Ptr<const MobilityModel> b,
                                    uint16_t indexTx, uint16_t indexRx) const;
  /**
   * Compute the channel gain between two devices.
   * \param txPsd The power spectral density of the transmitted signal.
   * \param indexTx The ID of the Tx node.
   * \param indexRx The ID of the Rx node.
   * \param txCodebook Pointer to the codebook of the Tx device.
   * \param rxCodebook Pointer to the codebook of the Tx device.
   * \return Channel gain between Tx and Tx device as Spectrum Value.
   */
  Ptr<SpectrumValue> GetChannelGain (Ptr<const SpectrumValue> txPsd, uint16_t pathNum,
                                     uint32_t indexTx, uint32_t indexRx,
                                     Ptr<CodebookParametric> txCodebook, Ptr<CodebookParametric> rxCodebook) const;
  /**
   * Quaternion Transformtion from the reference axis to the desired axis.
   * \param givenAxix The reference axis.
   * \param desiredAxix The target axis.
   * \param rotmVector Rotation Vector corresponding to the quaternion transformation.
   */
  void QuaternionTransform (double givenAxix[3], double desiredAxix[3], float2DVector_t& rotmVector) const;
  /**
   * Transform angles using the rotation vector obtained from the quaternion transformation.
   * \param elevation The elevation angle in degree.
   * \param azimuth The azimuth angle in degree.
   * \param isDoa Flag to indicate if it is direction of arrival.
   * \param rotmVector Rotation vectors.
   * \return The transformed angles in the new coordinate system measured in degree.
   */
  AnglesTransformed GetTransformedAngles(double elevation, double azimuth, bool isDoa, float2DVector_t& rotmVector) const;
  /**
   * Set Q-D Channel model folder path.
   * \param folderName The path to the Q-D Channel files.
   */
  void SetQdModelFolder (std::string folderName);
  /**
   * Set starting distance in trace file.
   * \param startDistance The starting distance in the provided trace file.
   */
  void SetStartDistance (uint16_t startDistance);

private:
  mutable ChannelMatrix m_channelMatrixMap;   //!< Channel matrix for the whole communication network.
  std::string m_qdFolder;                     //!< Folder that contains all the Q-D Channel model files.
  Ptr<UniformRandomVariable> m_uniformRv;     //!< Uniform random variable for doppler.
  double m_speed;                             //!< Trace/Node speed.
  uint16_t m_startDistance;                   //!< Starting point in the trace file.
  mutable uint16_t m_currentIndex;            //!< Current index in the trace file.
  mutable TraceFiles m_traceFiles;            //!< Status of the traces files.
  mutable uint16_t m_numTraces;               //!< The number of traces in QD files.

  mutable std::map<uint16_t, std::map<uint16_t, doubleVector_t> >   nbMultipathTxRx;      //!< Number of multipaths.
  mutable std::map<uint16_t, std::map<uint16_t, double2DVector_t> > delayTxRx;            //!< Delay spread in ns.
  mutable std::map<uint16_t, std::map<uint16_t, double2DVector_t> > pathLossTxRx;         //!< PathLoss (dB).
  mutable std::map<uint16_t, std::map<uint16_t, double2DVector_t> > phaseTxRx;            //!< Phase (radians).
  mutable std::map<uint16_t, std::map<uint16_t, double2DVector_t> > dopplerShiftTxRx;     //!< Doppler shift in Hz.
  /* Cannot have more than 8 antenna arrays for 802.11ay -> no more than 8 angles to transform */
  mutable std::map<uint16_t, std::map<uint16_t, double2DVector_t> > aodAzimuthTxRx[8];    //!< AoD Azimuth (Degrees) per antenna.
  mutable std::map<uint16_t, std::map<uint16_t, double2DVector_t> > aodElevationTxRx[8];  //!< AoD Elevation (Degrees) per antenna.
  mutable std::map<uint16_t, std::map<uint16_t, double2DVector_t> > aoaElevationTxRx[8];  //!< AoA Elevation (Degrees) per antenna.
  mutable std::map<uint16_t, std::map<uint16_t, double2DVector_t> > aoaAzimuthTxRx[8];    //!< AoA Azimuth (Degrees) per antenna.

};

}  //namespace ns3

#endif /* QD_CHANNEL_H */
