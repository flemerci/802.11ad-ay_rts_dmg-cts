# -*-coding:Utf-8 -*

import warnings
warnings.filterwarnings("ignore", module="matplotlib")
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import pylab
import sys
import csv
import copy
import pickle
#from collections import Counter
import os
import shutil, errno

print "Computing Throughput Graphs..."

def csvcount(filename):
    with open(filename, 'r') as f:
        i = 0
        for ligne in f:
            i += 1
    return i

###print int(sys.argv[1])

run_failed = []
for j in range(int(sys.argv[1])):	
		run_failed.append(j)


crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile'+'.csv',"rb"))
   

for row in crlog:
	if int(row[0][5:]) in run_failed:
  	    run_failed.remove(int(row[0][5:]))

###print run_failed
casefile = []
for i in range(int(sys.argv[1])):
	if i not in run_failed:
	    casefile.append(sys.argv[8]+sys.argv[2]+'-throughputState-'+str(i)+'.csv')

list_Delay = []
X = []
Xint = []


CCA = []
IDLE = []
bins = []

real_runs = len(casefile)

#s2 = 0.0
y =0
with open('NOFILE_TO_CREATE_Value_STA_Assoc.csv', "wb") as file:
    fileCDF = csv.writer(file)
    #fileCDF.writerow(["Case",
                      #"Val"])
    for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))


        #print csvcount('stats100-1.csv')
        cr.next()



    for row in cr:
        #s = row[1]
        #print row
        #print y
        #s2 = s2+float(s)
        X.append(row[0])
       
    
    Y = [[] for i in range(int(sys.argv[1]))]
    for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))


        #print csvcount('stats100-1.csv')
        cr.next()
	
        for row in cr:
            #s = row[1]
            #print row
            #print y
            #s2 = s2+float(s)
	    #print X[y]
	    Y[i].append(row[1])
	    y = y +1
    


    ###print Y
    Z = [[] for i in range(len(X))]
    for j in range(len(X)):
	
	for i in range(real_runs): ### Changed was int(sys.argv[1])
            
	    Z[j].append(float(Y[i][j]))
    
    for j in range(len(X)):
        Z[j] = np.average(Z[j])


    


    x = np.array(X)
    #y = np.array(Y)
    z = np.array(Z)


plt.plot(x,z,label='Throughput', marker='o')

z_mean = [np.mean(z)]*len(x)

plt.plot(x,z_mean, label='Mean', linestyle='--')
#plt.ylim(0, 1000)

#plt.xticks(ind, bins)
plt.xlabel("Time (s)")
plt.ylabel("Throughput (Mbps)")
#plt.legend(loc="center right")
plt.title("Avg Throughput (Nb of STA: " + sys.argv[3] + " Sim. Time: " + sys.argv[4] + "s Distance: " + sys.argv[5] + "m Runs: " + sys.argv[1] + " PhyMode: " + sys.argv[6] + " DataRate: " + sys.argv[7] + " RTS/CTS: " + sys.argv[9] + ")", fontsize = 8)




#plt.figure()
#plt.bar([TX,RX,CCA,IDLE], bins, stacked=True, normed = True)
#plt.show()
if (sys.argv[9] == "No"):
	plt.savefig(sys.argv[8]+'NO_RTS_PDFs/'+"Average_Throughput_"+sys.argv[2]+".pdf", dpi = 150)
else:
	plt.savefig(sys.argv[8]+'RTS_PDFs/'+"Average_Throughput_"+sys.argv[2]+".pdf", dpi = 150)
print " Done!"
