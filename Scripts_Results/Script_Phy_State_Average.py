# -*-coding:Utf-8 -*

import warnings
warnings.filterwarnings("ignore", module="matplotlib")
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import pylab
import sys
import csv
import copy
import pickle
#from collections import Counter
import os
import shutil, errno

print "Computing PhyState BarGraphs..."

def csvcount(filename):
    with open(filename, 'r') as f:
        i = 0
        for ligne in f:
            i += 1
    return i


run_failed = []
for j in range(int(sys.argv[1])):	
		run_failed.append(j)

if (sys.argv[9] == '1'):
	crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile-RTS'+'.csv',"rb"))
elif (sys.argv[9] == '0'):
	crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile-NO_RTS'+'.csv',"rb"))
   

for row in crlog:
	if int(row[0][5:]) in run_failed:
  	    run_failed.remove(int(row[0][5:]))



casefile = []
for i in range(int(sys.argv[1])):
	if i not in run_failed:
		if (sys.argv[9] == '1'):
	    		casefile.append(sys.argv[8]+sys.argv[2]+'-Phy-State-out-RTS'+str(i)+'.csv')
		elif (sys.argv[9] == '0'):
			casefile.append(sys.argv[8]+sys.argv[2]+'-Phy-State-out-NO_RTS'+str(i)+'.csv')
list_Delay = []
#Y = [[] for i in range(int(sys.argv[1]))]
TX = [[] for i in range(int(sys.argv[3])+1)]
RX = [[] for i in range(int(sys.argv[3])+1)]
CCA = [[] for i in range(int(sys.argv[3])+1)]
IDLE = [[] for i in range(int(sys.argv[3])+1)]
bins = []


real_runs = len(casefile)
#s2 = 0.0
y =0
with open('Average_Value_STA_Assoc.csv', "wb") as file:
    fileCDF = csv.writer(file)
    #fileCDF.writerow(["Case",
                      #"Val"])
    for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))


        #print csvcount('stats100-1.csv')
        cr.next()
	y =0

        for row in cr:
            #s = row[1]
            ###print row
            ###print y
            #s2 = s2+float(s)
            #bins[y].append(int(row[0]))
            TX[y].append(float(row[1]))
            RX[y].append(float(row[2]))
            CCA[y].append(float(row[3]))
            IDLE[y].append(float(row[4]))
            #bins.append(row[0])
	    y = y +1

    for j in range(len(TX)):
        TX[j] = np.average(TX[j])
        RX[j] = np.average(RX[j])
        CCA[j] = np.average(CCA[j])
        IDLE[j] = np.average(IDLE[j])

    for j in range(len(TX)):
	if j == 0:
	     bins.append("AP")
	else:
	     bins.append(j)

###print bins

tx = np.array(TX)
rx = np.array(RX)
cca = np.array(CCA)
idle = np.array(IDLE)
ind = [x for x, _ in enumerate(bins)]
###print ind

plt.bar(ind, tx, width=0.8, label='TX', color='#3dcc76', bottom = rx+cca+idle, linewidth = 0)
plt.bar(ind, rx, width=0.8, label='RX', color='#edd223', bottom = cca+idle, linewidth = 0)
plt.bar(ind, cca, width=0.8, label='CCA_Busy', color='#ef6e6e', bottom = idle, linewidth = 0)
plt.bar(ind, idle, width=0.8, label='IDLE', color='#4e7bba', linewidth = 0)

plt.xticks(ind, bins, rotation = 'vertical', fontsize = 8)
plt.ylabel("Total time in second")
plt.xlabel("STA")
plt.legend(loc="bottom right")
plt.title("Phy State (Nb of STA: " + sys.argv[3] + " Sim. Time: " + sys.argv[4] + "s Distance: " + sys.argv[5] + "m Runs: " + sys.argv[1] + " PhyMode: " + sys.argv[6] + " DataRate: " + sys.argv[7] + " RTS: " + sys.argv[9] + ")", fontsize = 8)




#plt.figure()
#plt.bar([TX,RX,CCA,IDLE], bins, stacked=True, normed = True)
#plt.show()
if (sys.argv[10] == 'SimpleTest'):
	plt.savefig(sys.argv[8]+'PDFs/'+"Average_PhyState_"+sys.argv[2]+".pdf", dpi = 150)
else:
	if (sys.argv[9] == '1'):
		plt.savefig(sys.argv[8]+'RTS_PDFs/'+"Average_PhyState_"+sys.argv[2]+".pdf", dpi = 150)
	elif (sys.argv[9] == '0'):
		plt.savefig(sys.argv[8]+'NO_RTS_PDFs/'+"Average_PhyState_"+sys.argv[2]+".pdf", dpi = 150)
print " Done!"
