import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
import math
import warnings
warnings.filterwarnings("ignore", module="matplotlib")
from datetime import datetime
import pylab
import sys
import csv
import copy
import pickle
import os
import time
import shutil, errno

print "Computing PacketLoss Graphs..."

def csvcount(filename):
    with open(filename, 'r') as f:
        i = 0
        for ligne in f:
            i += 1
    return i

run_failed = []
for j in range(int(sys.argv[1])):	
		run_failed.append(j)


crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile'+'.csv',"rb"))
   

for row in crlog:
	if int(row[0][5:]) in run_failed:
  	    run_failed.remove(int(row[0][5:]))






packet_loss = [[] for i in range(int(sys.argv[1])+1)]
for i in range(int(sys.argv[1])):
	for j in range(int(sys.argv[3])+1):	
		packet_loss[i].append(100.0)
	
###print packet_loss
coordinates = [[[] for i in range(int(sys.argv[3])+1)]for i in range(int(sys.argv[1]))]
###print coordinates
casefile = []
casefile2 = []
casefile3 = []
for i in range(int(sys.argv[1])):
    if i not in run_failed:
    	casefile.append(sys.argv[8]+sys.argv[2]+'-nodePositionTrace-'+str(i)+'.csv')
    	casefile2.append(sys.argv[8]+sys.argv[2]+'-flow-Monitor-out'+str(i)+'.csv')
    	casefile3.append(sys.argv[8]+sys.argv[2]+'-nodeAssoc-'+str(i)+'.csv')

real_runs = len(casefile)
number_of_pklost = [[] for i in range(real_runs)]

node_not_associated = [[] for i in range(real_runs)]
for i in range(real_runs):
	for j in range(int(sys.argv[3])):	
		node_not_associated[i].append(j+1)  #add

###print node_not_associated

for i in range(real_runs):     ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))
    
	cr.next()
	y =0

        for row in cr:
	    if row[0] == 'AP':
		coordinates[i][y].append(row[0])
	    else:
		coordinates[i][y].append(float(row[0]))
          
	    coordinates[i][y].append(float(row[1]))
            coordinates[i][y].append(float(row[2]))
  	    y = y +1

###print coordinates

for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile2[i],"rb"))
   
	cr.next()
	temp_pl = 0
	
	packet_loss[i][0] = 0.0
        for row in cr:
	    
            	
		packet_loss[i][int(row[11])+1] = float(row[10])
		
		temp_pl = temp_pl + int(row[12])
		
		###node_not_associated[i].remove(int(row[11]))### delete for slsresults change

	cr2 = csv.reader(open(casefile3[i],"rb"))
   
	cr2.next()
	number_of_pklost[i] = temp_pl 

	
        for row in cr2:
		

		if int(row[0]) in node_not_associated[i]:

	  		node_not_associated[i].remove(int(row[0]))
				


###print 'NODE NOT ASSOCIATED: ', node_not_associated

###print number_of_pklost

N = int(sys.argv[3])+1
area = []
for size in range(N):
	area.append(200)#Size of the nodes   was 500
 
r = [[[] for i in range(N)]for i in range(int(sys.argv[1]))]
theta = [[[] for i in range(N)]for i in range(int(sys.argv[1]))]
r_annot = [[[] for i in range(N)]for i in range(int(sys.argv[1]))]
theta_annot = [[[] for i in range(N)]for i in range(int(sys.argv[1]))]
	
for j in range(real_runs): ### Changed was int(sys.argv[1])


	sta_id = []
	sta_id.append('AP')
	for k in range(N-1):
		sta_id.append(str(k+1))
	 
	for i in range(N):
		###print i,j

		if coordinates[j][i][1] == 0.0 and coordinates[j][i][2] == 0.0:
			r[j][i] = 0.0
			theta [j][i] = 0.0

		elif coordinates[j][i][1] == 0.0 and coordinates[j][i][2] > 0.0:
			r[j][i] = coordinates[j][i][2]
			theta [j][i] = (np.pi / 2)
		elif coordinates[j][i][1] == 0.0 and coordinates[j][i][2] < 0.0:
			r[j][i] = -1*coordinates[j][i][2]
			theta [j][i] = -1*(np.pi / 2)
		elif coordinates[j][i][2] == 0.0 and coordinates[j][i][1] > 0.0:
			r[j][i] = coordinates[j][i][1]
			theta [j][i] = 0.0
		elif coordinates[j][i][2] == 0.0 and coordinates[j][i][1] < 0.0:
			r[j][i] = -1*coordinates[j][i][1]
			theta [j][i] = np.pi
		elif coordinates[j][i][1] < 0 and coordinates[j][i][2] < 0 :
			r[j][i] = sqrt((coordinates[j][i][1]*coordinates[j][i][1])+(coordinates[j][i][2]*coordinates[j][i][2]))
			theta [j][i] = math.atan(coordinates[j][i][2]/coordinates[j][i][1]) + np.pi
		elif coordinates[j][i][1] < 0 and coordinates[j][i][2] > 0 :
			r[j][i] = sqrt((coordinates[j][i][1]*coordinates[j][i][1])+(coordinates[j][i][2]*coordinates[j][i][2]))
			theta [j][i] = math.atan(coordinates[j][i][2]/coordinates[j][i][1]) + np.pi
		
		elif coordinates[j][i][1] > 0 and coordinates[j][i][2] < 0 :
			r[j][i] = sqrt((coordinates[j][i][1]*coordinates[j][i][1])+(coordinates[j][i][2]*coordinates[j][i][2]))
			theta [j][i] = math.atan(coordinates[j][i][2]/coordinates[j][i][1]) + (2* np.pi)
			
		else :
			r[j][i] = sqrt((coordinates[j][i][1]*coordinates[j][i][1])+(coordinates[j][i][2]*coordinates[j][i][2]))
			theta [j][i] = math.atan(coordinates[j][i][2]/coordinates[j][i][1])
			

		if sta_id[i] == 'AP':
			r_annot[j][i] = sqrt(((coordinates[j][i][1]-(int(sys.argv[5])*0.048))*(coordinates[j][i][1]-(int(sys.argv[5])*0.048)))+((coordinates[j][i][2]-(int(sys.argv[5])*0.03))*(coordinates[j][i][2]-(int(sys.argv[5])*0.03))))
			theta_annot [j][i] = math.atan((coordinates[j][i][2]-(int(sys.argv[5])*0.03))/(coordinates[j][i][1]-(int(sys.argv[5])*0.048))) + np.pi
			
		elif int(sta_id[i]) > 9:
			if coordinates[j][i][1]-(int(sys.argv[5])*0.048) < 0.0 and coordinates[j][i][2]-(int(sys.argv[5])*0.03) < 0.0:
				r_annot[j][i] = sqrt(((coordinates[j][i][1]-(int(sys.argv[5])*0.048))*(coordinates[j][i][1]-(int(sys.argv[5])*0.048)))+((coordinates[j][i][2]-(int(sys.argv[5])*0.03))*(coordinates[j][i][2]-(int(sys.argv[5])*0.03))))
				theta_annot [j][i] = math.atan((coordinates[j][i][2]-(int(sys.argv[5])*0.03))/(coordinates[j][i][1]-(int(sys.argv[5])*0.048))) + np.pi
		
			elif coordinates[j][i][1]-(int(sys.argv[5])*0.048) < 0 and coordinates[j][i][2]-(int(sys.argv[5])*0.03) > 0 :
				r_annot[j][i] = sqrt(((coordinates[j][i][1]-(int(sys.argv[5])*0.048))*(coordinates[j][i][1]-(int(sys.argv[5])*0.048)))+((coordinates[j][i][2]-(int(sys.argv[5])*0.03))*(coordinates[j][i][2]-(int(sys.argv[5])*0.03))))
				theta_annot [j][i] = math.atan((coordinates[j][i][2]-(int(sys.argv[5])*0.03))/(coordinates[j][i][1]-(int(sys.argv[5])*0.048))) + np.pi

			elif coordinates[j][i][1]-(int(sys.argv[5])*0.048) > 0 and coordinates[j][i][2]-(int(sys.argv[5])*0.03) < 0 :
				r_annot[j][i] = sqrt(((coordinates[j][i][1]-(int(sys.argv[5])*0.048))*(coordinates[j][i][1]-(int(sys.argv[5])*0.048)))+((coordinates[j][i][2]-(int(sys.argv[5])*0.03))*(coordinates[j][i][2]-(int(sys.argv[5])*0.03))))
				theta_annot [j][i] = math.atan((coordinates[j][i][2]-(int(sys.argv[5])*0.03))/(coordinates[j][i][1]-(int(sys.argv[5])*0.048))) + (2* np.pi)
			else :
				r_annot[j][i] = sqrt(((coordinates[j][i][1]-(int(sys.argv[5])*0.048))*(coordinates[j][i][1]-(int(sys.argv[5])*0.048)))+((coordinates[j][i][2]-(int(sys.argv[5])*0.03))*(coordinates[j][i][2]-(int(sys.argv[5])*0.03))))
				theta_annot [j][i] = math.atan((coordinates[j][i][2]-(int(sys.argv[5])*0.03))/(coordinates[j][i][1]-(int(sys.argv[5])*0.048)))
		else :
			if coordinates[j][i][1]-(int(sys.argv[5])*0.026) < 0.0 and coordinates[j][i][2]-(int(sys.argv[5])*0.03) < 0.0:
				r_annot[j][i] = sqrt(((coordinates[j][i][1]-(int(sys.argv[5])*0.026))*(coordinates[j][i][1]-(int(sys.argv[5])*0.026)))+((coordinates[j][i][2]-(int(sys.argv[5])*0.03))*(coordinates[j][i][2]-(int(sys.argv[5])*0.03))))
				theta_annot [j][i] = math.atan((coordinates[j][i][2]-(int(sys.argv[5])*0.03))/(coordinates[j][i][1]-(int(sys.argv[5])*0.026))) + np.pi
		

			elif coordinates[j][i][1]-(int(sys.argv[5])*0.026) < 0 and coordinates[j][i][2]-(int(sys.argv[5])*0.03) > 0 :
				r_annot[j][i] = sqrt(((coordinates[j][i][1]-(int(sys.argv[5])*0.026))*(coordinates[j][i][1]-(int(sys.argv[5])*0.026)))+((coordinates[j][i][2]-(int(sys.argv[5])*0.03))*(coordinates[j][i][2]-(int(sys.argv[5])*0.03))))
				theta_annot [j][i] = math.atan((coordinates[j][i][2]-(int(sys.argv[5])*0.03))/(coordinates[j][i][1]-(int(sys.argv[5])*0.026))) + np.pi

			elif coordinates[j][i][1]-(int(sys.argv[5])*0.026) > 0 and coordinates[j][i][2]-(int(sys.argv[5])*0.03) < 0 :
				r_annot[j][i] = sqrt(((coordinates[j][i][1]-(int(sys.argv[5])*0.026))*(coordinates[j][i][1]-(int(sys.argv[5])*0.026)))+((coordinates[j][i][2]-(int(sys.argv[5])*0.03))*(coordinates[j][i][2]-(int(sys.argv[5])*0.03))))
				theta_annot [j][i] = math.atan((coordinates[j][i][2]-(int(sys.argv[5])*0.03))/(coordinates[j][i][1]-(int(sys.argv[5])*0.026))) + (2* np.pi)
			else :
				r_annot[j][i] = sqrt(((coordinates[j][i][1]-(int(sys.argv[5])*0.026))*(coordinates[j][i][1]-(int(sys.argv[5])*0.026)))+((coordinates[j][i][2]-(int(sys.argv[5])*0.03))*(coordinates[j][i][2]-(int(sys.argv[5])*0.03))))
				theta_annot [j][i] = math.atan((coordinates[j][i][2]-(int(sys.argv[5])*0.03))/(coordinates[j][i][1]-(int(sys.argv[5])*0.026)))

	###print r
	###print theta

	colors = packet_loss[j]
	 
	#colors = packet_Loss
	
	###print colors
	###print sta_id
	fig = plt.figure()
	#plt.grid(True)
	
	ax = fig.add_subplot(111, projection = 'polar')
	
	for x, txt in enumerate(sta_id):
		
		if txt == 'AP':
			###ax.annotate("_", (theta[j][x], r[j][x]), color='red', rotation=0, fontsize = 44, horizontalalignment='center', verticalalignment='center', fontstretch= 'ultra-condensed', fontproperties = 'light')
			###ax.annotate("_", (theta[j][x], r[j][x]), color='red', rotation=90, fontsize = 44, horizontalalignment='center', verticalalignment='center', fontstretch= 'ultra-condensed', fontproperties = 'light')
			###ax.annotate("_", (theta[j][x], r[j][x]), color='red', rotation=45, fontsize = 44, horizontalalignment='center', verticalalignment='center', fontstretch= 'ultra-condensed', fontproperties = 'light')
			###ax.annotate("_", (theta[j][x], r[j][x]), color='red', rotation=135, fontsize = 44, horizontalalignment='center', verticalalignment='center', fontstretch= 'ultra-condensed', fontproperties = 'light')
			###ax.annotate('annotate', xy=(2, 1), xytext=(3, 4),arrowprops=dict(facecolor='black', shrink=0.05))
			ax.annotate(txt, (theta_annot[j][x], r_annot[j][x]), color='white')

		elif int(txt) in node_not_associated[j]:
			
			###print txt
			ax.annotate(txt, (theta_annot[j][x], r_annot[j][x]), color='yellow')

		else:
			ax.annotate(txt, (theta_annot[j][x], r_annot[j][x]), color='white')
			###ax.annotate("_", (theta[j][x], r[j][x]), color='red', rotation=0, fontsize = 44, horizontalalignment='center', verticalalignment='center', fontstretch= 'ultra-condensed', fontproperties = 'light')
			###ax.annotate("_", (theta[j][x], r[j][x]), color='red', rotation=90, fontsize = 44, horizontalalignment='center', verticalalignment='center', fontstretch= 'ultra-condensed', fontproperties = 'light')
			###ax.annotate("_", (theta[j][x], r[j][x]), color='red', rotation=45, fontsize = 44, horizontalalignment='center', verticalalignment='center', fontstretch= 'ultra-condensed', fontproperties = 'light')
			###ax.annotate("_", (theta[j][x], r[j][x]), color='red', rotation=135, fontsize = 44, horizontalalignment='center', verticalalignment='center', fontstretch= 'ultra-condensed', fontproperties = 'light')

	###print theta[j]
	###print r[j]
	c = ax.scatter(theta[j], r[j], c=colors, edgecolors='face', s=area, cmap= plt.matplotlib.cm.jet , alpha=0.85, vmin=0, vmax=100)



	
	###print number_of_pklost
	cbar = plt.colorbar(c)
	cbar.set_label('Packet loss (%)')
	ax.set_ylim(0, int(sys.argv[5])+1)
	plt.title("Packet Loss (STA: " + sys.argv[3] + " Time: " + sys.argv[4] + "s Dist.: " + sys.argv[5] + "m Runs: " + sys.argv[1] + " Phy: " + sys.argv[6] + " DR: " + sys.argv[7] + " TPL: " + str(number_of_pklost[j]) + " Moy: " + str(sum(number_of_pklost)/len(number_of_pklost)) + " RTS/CTS: " + sys.argv[9] + ")", fontsize = 8)


#ax.set_xlim(-10,10)
#ax.set_ylim(-10,10)
#major_ticks = np.arange(-10, 10, 1)
#minor_ticks = np.arange(-10, 10, 1)
#ax.set_xticks(major_ticks)
#ax.set_xticks(minor_ticks, minor = True)
#ax.set_yticks(major_ticks)
#ax.set_yticks(minor_ticks, minor = True)
#ax.grid(which='minor')
#ax.grid(which='major')
	###fig.set_size_inches(3,3)
	if (sys.argv[9] == "No"):
		plt.savefig(sys.argv[8]+'NO_RTS_PDFs/'+sys.argv[2]+"-PacketLoss_PolarGraph-Run-"+str(j)+".pdf", dpi = 150)
	else:
		plt.savefig(sys.argv[8]+'RTS_PDFs/'+sys.argv[2]+"-PacketLoss_PolarGraph-Run-"+str(j)+".pdf", dpi = 150)
	
print " Done!"
print str(len(run_failed)) + "/"+ str(int(sys.argv[1])) + " Failed runs: ", run_failed
