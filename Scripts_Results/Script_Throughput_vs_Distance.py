# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings("ignore", module="matplotlib")
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import pylab
import sys
import csv
import copy
import pickle
#from collections import Counter
import os
import shutil, errno

print "Computing Throughput Graphs..."

def csvcount(filename):
    with open(filename, 'r') as f:
        i = 0
        for ligne in f:
            i += 1
    return i

###print int(sys.argv[1])

run_failed = []
for j in range(int(sys.argv[1])):	
		run_failed.append(j)


crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile'+'.csv',"rb"))
   

for row in crlog:
	if int(row[0][5:]) in run_failed:
  	    run_failed.remove(int(row[0][5:]))


casefile = []
for i in range(int(sys.argv[1])):
	if i not in run_failed:
	    casefile.append(sys.argv[8]+sys.argv[2]+'-throughputState-'+str(i)+'.csv')

list_Delay = []
X = []
Xint = []


CCA = []
IDLE = []
bins = []

real_runs = len(casefile)

#s2 = 0.0
y =0
with open('NOFILE_TO_CREATE_Value_STA_Assoc.csv', "wb") as file:
    fileCDF = csv.writer(file)
    #fileCDF.writerow(["Case",
                      #"Val"])
    for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))


        #print csvcount('stats100-1.csv')
        cr.next()



    for row in cr:
        #s = row[1]
        #print row
        #print y
        #s2 = s2+float(s)
        X.append(row[0])
       
    
    Y = [[] for i in range(int(sys.argv[1]))]
    for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))


        #print csvcount('stats100-1.csv')
        cr.next()
	
        for row in cr:
            #s = row[1]
            #print row
            #print y
            #s2 = s2+float(s)
	    #print X[y]
	    if (row[1] != '0'):
		Y[i].append(float(row[1]))
	    y = y +1
    


    

throughputdef = [[] for i in range(int(sys.argv[1]))]

for i in range(len(Y)):

  throughputdef[i] = np.array(Y[i])

print throughputdef
    


plt.boxplot(throughputdef)


#plt.xticks(ind, bins)
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'])

plt.xlabel("Distance in meter")
plt.ylabel("Throughput (Mbps)")
#plt.legend(loc="center right")
plt.title("Throughput vs Distance (Nb of STA: " + sys.argv[3] + " Sim. Time: " + sys.argv[4] + "s Distance: " + sys.argv[5] + "m Runs: " + sys.argv[1] + " PhyMode: " + sys.argv[6] + " DataRate: " + sys.argv[7] + ")", fontsize = 8)




#plt.figure()
#plt.bar([TX,RX,CCA,IDLE], bins, stacked=True, normed = True)
#plt.show()
plt.savefig(sys.argv[8]+'PDFs/'+"Average_Throughput_"+sys.argv[2]+".pdf", dpi = 150)
print " Done!"
