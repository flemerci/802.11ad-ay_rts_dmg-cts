import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
import math
import warnings
warnings.filterwarnings("ignore", module="matplotlib")
from datetime import datetime
import pylab
import sys
import csv
import copy
import pickle
import os
import time
import shutil, errno

print "Computing Contention Window BarGraphs..."

def csvcount(filename):
    with open(filename, 'r') as f:
        i = 0
        for ligne in f:
            i += 1
    return i

run_failed = []
for j in range(int(sys.argv[1])):	
		run_failed.append(j)

if (sys.argv[9] == '1'):
	crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile-RTS'+'.csv',"rb"))
elif (sys.argv[9] == '0'):
   	crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile-NO_RTS'+'.csv',"rb"))

for row in crlog:
	if int(row[0][5:]) in run_failed:
  	    run_failed.remove(int(row[0][5:]))


packetLoss_STA2 = [[[] for i in range(10)]for i in range(int(sys.argv[1]))]
moy_cw_Graph = [[[] for i in range(10)]for i in range(int(sys.argv[1]))]

moy_CW_STA = [[[] for i in range(int(sys.argv[3]))]for i in range(int(sys.argv[1]))]


packetLoss_STA = [[] for i in range(int(sys.argv[3]))]

packet_loss = [[] for i in range(int(sys.argv[1])+1)]
for i in range(int(sys.argv[1])):
	for j in range(int(sys.argv[3])+1):	
		packet_loss[i].append(100.0)
	
###print packet_loss
coordinates = [[[] for i in range(int(sys.argv[3])+1)]for i in range(int(sys.argv[1]))]
###print coordinates
casefile = []
casefile2 = []
casefile3 = []
for i in range(int(sys.argv[1])):
    if i not in run_failed:
	if (sys.argv[9] == '1'):
    		casefile.append(sys.argv[8]+sys.argv[2]+'-cwTraceState-RTS-'+str(i)+'.csv')
    		casefile2.append(sys.argv[8]+sys.argv[2]+'-flow-Monitor-out-RTS'+str(i)+'.csv')
    		
	elif (sys.argv[9] == '0'):
		casefile.append(sys.argv[8]+sys.argv[2]+'-cwTraceState-NO_RTS-'+str(i)+'.csv')
    		casefile2.append(sys.argv[8]+sys.argv[2]+'-flow-Monitor-out-NO_RTS'+str(i)+'.csv')
    		

real_runs = len(casefile)
number_of_pklost = [[] for i in range(real_runs)]

node_not_associated = [[] for i in range(real_runs)]
for i in range(real_runs):
	for j in range(int(sys.argv[3])):	
		node_not_associated[i].append(j+1)  #add

###print node_not_associated

for i in range(real_runs):     ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))
    
	cr.next()

        for row in cr:
	    moy_CW_STA[i][int(row[1])-1].append(int(row[3])) 

  	    


  	    

for i in range(real_runs):     ### Changed was int(sys.argv[1])

        
	for j in range(int(sys.argv[3])):
		
		
		moy_CW_STA[i][j] = np.average(moy_CW_STA[i][j]) 
							




for i in range(real_runs):     ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile2[i],"rb"))
    
	cr.next()

        for row in cr:
		
		if 0.0 <= float(row[10]) < 10.0:
			packetLoss_STA2[i][0].append(int(row[11]))
		if 10.0 < float(row[10]) < 20.0:
			packetLoss_STA2[i][1].append(int(row[11]))
		if 20.0 < float(row[10]) < 30.0:
			packetLoss_STA2[i][2].append(int(row[11]))
		if 30.0 < float(row[10]) < 40.0:
			packetLoss_STA2[i][3].append(int(row[11]))
		if 40.0 < float(row[10]) < 50.0:
			packetLoss_STA2[i][4].append(int(row[11]))
		if 50.0 < float(row[10]) < 60.0:
			packetLoss_STA2[i][5].append(int(row[11]))
		if 60.0 < float(row[10]) < 70.0:
			packetLoss_STA2[i][6].append(int(row[11]))
		if 70.0 < float(row[10]) < 80.0:
			packetLoss_STA2[i][7].append(int(row[11]))
		if 80.0 < float(row[10]) < 90.0:
			packetLoss_STA2[i][8].append(int(row[11]))
		if 90.0 < float(row[10]) <= 100.0:
			packetLoss_STA2[i][9].append(int(row[11]))
		


for i in range(real_runs):
	for j in range(10):
		
		
		#print packetLoss_STA2
		if len(packetLoss_STA2[i][j]) == 0:

			moy_cw_Graph[i][j] = 0.0
			
		
		for k in range(len(packetLoss_STA2[i][j])):
			#print packetLoss_STA2[i][j][k]
			#print moy_CW_STA[i][packetLoss_STA2[i][j][k]]
			
			moy_cw_Graph[i][j].append(moy_CW_STA[i][packetLoss_STA2[i][j][k]])
			


for i in range(real_runs):     ### Changed was int(sys.argv[1])

        
	for j in range(10):
		
		
		moy_cw_Graph[i][j] = np.average(moy_cw_Graph[i][j]) 
		


#######
for i in range(real_runs):
	
	#plt.plot(x,z,label='Throughput', marker='o')

	
	labels = ['0-10%', '10-20%', '20-30%', '30-40%', '40-50%', '50-60%', '60-70%', '70-80%', '80-90%', '90-100%']
	x = np.arange(len(labels))	
	width = 0.15

	plt.cla()
	plt.bar(x,moy_cw_Graph[i],color='blue',edgecolor='black')
	
	plt.xticks(x, ('0-10%', '10-20%', '20-30%', '30-40%', '40-50%', '50-60%', '60-70%', '70-80%', '80-90%', '90-100%'))
	plt.xlabel("Packet Loss")
	plt.ylabel("Average CW Size")
	
	#plt.legend(loc="center right")
	plt.title("CW Size vs PacketLoss (Nb of STA: " + sys.argv[3] + " Sim. Time: " + sys.argv[4] + "s Distance: " + sys.argv[5] + "m Runs: " + sys.argv[1] + " PhyMode: " + sys.argv[6] + " DataRate: " + sys.argv[7] + " RTS: " + sys.argv[9]  + ")", fontsize = 8)




	if (sys.argv[10] == 'SimpleTest'):
		plt.savefig(sys.argv[8]+'PDFs/'+sys.argv[2]+"-CW_BarGraph-Run-"+str(i)+".pdf", dpi = 150)
	else:

		if (sys.argv[9] == '1'):
			plt.savefig(sys.argv[8]+'RTS_PDFs/'+sys.argv[2]+"-CW_BarGraph-Run-"+str(i)+".pdf", dpi = 150)
		if (sys.argv[9] == '0'):
			plt.savefig(sys.argv[8]+'NO_RTS_PDFs/'+sys.argv[2]+"-CW_BarGraph-Run-"+str(i)+".pdf", dpi = 150)




print " Done!"

