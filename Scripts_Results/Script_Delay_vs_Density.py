# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings("ignore", module="matplotlib")
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import pylab
import sys
import csv
import copy
import pickle
#from collections import Counter
import os
import shutil, errno

print "Computing Delay VS Density..."

def csvcount(filename):
    with open(filename, 'r') as f:
        i = 0
        for ligne in f:
            i += 1
    return i


run_failed = []
for j in range(int(sys.argv[1])):	
		run_failed.append(j)


crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile'+'.csv',"rb"))
   

for row in crlog:
	if int(row[0][5:]) in run_failed:
  	    run_failed.remove(int(row[0][5:]))



casefile = []
for i in range(int(sys.argv[1])):
	if i not in run_failed:
	    casefile.append(sys.argv[8]+sys.argv[2]+'-flow-Monitor-out'+str(i)+'.csv')

list_Delay = []
#Y = [[] for i in range(int(sys.argv[1]))]
delais = [[] for i in range(int(sys.argv[1]))]

bins = []


real_runs = len(casefile)
#s2 = 0.0
y =0
with open('Average_Value_STA_Assoc.csv', "wb") as file:
    fileCDF = csv.writer(file)
    #fileCDF.writerow(["Case",
                      #"Val"])
    for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))


        #print csvcount('stats100-1.csv')
        cr.next()
	

        for row in cr:
            #s = row[1]
            #print row
            ###print y
            #s2 = s2+float(s)
            #bins[y].append(int(row[0]))
	    if (row[9] != '0'):
		delais[y].append(float(row[9][1:-2]))
            #bins.append(row[0])
	y = y +1
    


   ### for j in range(len(delais)):
   ###     delais[j] = np.average(delais[j])


   ### for j in range(len(delais)):
	      ###bins.append(j+1)


delaisdef = [[] for i in range(int(sys.argv[1]))]

for i in range(len(delais)):

  delaisdef[i] = np.array(delais[i])

#print delaisdef
ind = [x for x, _ in enumerate(bins)]
###print bins

###plt.plot(bins, delais, label='Delay', color='#3dcc76')


plt.boxplot(delais)
plt.yscale('linear')
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], ['10', '20', '30', '40', '50', '60', '70', '80', '90', '100'])

###plt.yscale('log')
###plt.xticks(ind, bins, rotation = 'vertical', fontsize = 8)
plt.ylabel("Delay in nanosecond")
plt.xlabel("Number of STA")
###plt.legend(loc="center left")
plt.title("Delay vs Density (Nb of STA: " + "Multiple" + " Sim. Time: " + sys.argv[4] + "s Distance: " + sys.argv[5] + "m Runs: " + sys.argv[1] + " PhyMode: " + sys.argv[6] + " DataRate: " + sys.argv[7] + ")", fontsize = 8)




#plt.figure()
#plt.bar([TX,RX,CCA,IDLE], bins, stacked=True, normed = True)
#plt.show()
plt.savefig(sys.argv[8]+'PDFs/'+"Average_DelaisVSdensity_"+sys.argv[2]+".pdf", dpi = 150)
print " Done!"
