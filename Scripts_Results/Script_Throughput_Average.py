# -*-coding:Utf-8 -*

import warnings
warnings.filterwarnings("ignore", module="matplotlib")
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import pylab
import sys
import csv
import copy
import pickle
#from collections import Counter
import os
import shutil, errno

print "Computing Throughput Graphs..."

def csvcount(filename):
    with open(filename, 'r') as f:
        i = 0
        for ligne in f:
            i += 1
    return i

###print int(sys.argv[1])

run_failed = []
for j in range(int(sys.argv[1])):	
		run_failed.append(j)

if (sys.argv[9] == '1'):
	
	crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile-RTS'+'.csv',"rb"))
elif (sys.argv[9] == '0'):
	
	crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile-NO_RTS'+'.csv',"rb"))
   

for row in crlog:
	if int(row[0][5:]) in run_failed:
  	    run_failed.remove(int(row[0][5:]))

###print run_failed
casefile = []
for i in range(int(sys.argv[1])):
	if i not in run_failed:
		if (sys.argv[9] == '1'):
	    		casefile.append(sys.argv[8]+sys.argv[2]+'-throughputState-RTS-'+str(i)+'.csv')
		elif (sys.argv[9] == '0'):
			casefile.append(sys.argv[8]+sys.argv[2]+'-throughputState-NO_RTS-'+str(i)+'.csv')

list_Delay = []
X = []
Xint = []


CCA = []
IDLE = []
bins = []

real_runs = len(casefile)

#s2 = 0.0
y =0
with open('NOFILE_TO_CREATE_Value_STA_Assoc.csv', "wb") as file:
    fileCDF = csv.writer(file)
    #fileCDF.writerow(["Case",
                      #"Val"])
    for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))


        #print csvcount('stats100-1.csv')
        cr.next()



    for row in cr:
        #s = row[1]
        #print row
        #print y
        #s2 = s2+float(s)
        X.append(row[0])
       
    
    Y = [[] for i in range(int(sys.argv[1]))]
    for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))


        #print csvcount('stats100-1.csv')
        cr.next()
	
        for row in cr:
            #s = row[1]
            #print row
            #print y
            #s2 = s2+float(s)
	    #print X[y]
	    Y[i].append(row[1])
	    y = y +1
    


    ###print Y
    Z = [[] for i in range(len(X))]
    for j in range(len(X)):
	
	for i in range(real_runs): ### Changed was int(sys.argv[1])
            
	    Z[j].append(float(Y[i][j]))
    

    for j in range(len(X)):
        Z[j] = np.average(Z[j])




    x = np.array(X) #Changed to match Z length
    #x = np.array(X[50:200])
    #y = np.array(Y)
    z = np.array(Z)     #Changed for mean after 5
    #z = np.array(Z[50:200])  # To be changed if going back to full mean


plt.plot(x,z,label='Throughput', marker='o')

z_mean = [np.mean(z)]*len(x)


#plt.plot(x,z_mean, label='Mean', linestyle='--')
#plt.ylim(0, 1000)
#plt.annotate(str(z_mean[0])+'Mbps', xy = (5, z_mean[0]), color='green' )
#plt.xticks(ind, bins)
plt.xlabel("Time (s)")
plt.ylabel("Throughput (Mbps)")
#plt.xlim([5, 20])
#plt.legend(loc="center right")
plt.title("Average Throughput (Nb of STA: " + sys.argv[3] + " Sim. Time: " + sys.argv[4] + "s Distance: " + sys.argv[5] + "m Runs: " + sys.argv[1] + " PhyMode: " + sys.argv[6] + " DataRate: " + sys.argv[7] + " RTS: " + sys.argv[9]  + ")", fontsize = 8)




#plt.figure()
#plt.bar([TX,RX,CCA,IDLE], bins, stacked=True, normed = True)
#plt.show()
if (sys.argv[10] == 'SimpleTest'):
	plt.savefig(sys.argv[8]+'PDFs/'+"Average_Throughput_"+sys.argv[2]+".pdf", dpi = 150)
else:
	if (sys.argv[9] == '1'):
		plt.savefig(sys.argv[8]+'RTS_PDFs/'+"Average_Throughput_"+sys.argv[2]+".pdf", dpi = 150)
	elif (sys.argv[9] == '0'):
		plt.savefig(sys.argv[8]+'NO_RTS_PDFs/'+"Average_Throughput_"+sys.argv[2]+".pdf", dpi = 150)
print " Done!"
