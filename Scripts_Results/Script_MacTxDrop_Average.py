# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings("ignore", module="matplotlib")
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import pylab
import sys
import csv
import copy
import pickle
#from collections import Counter
import os
import shutil, errno

print "Computing MacTxDrop Graph..."

def csvcount(filename):
    with open(filename, 'r') as f:
        i = 0
        for ligne in f:
            i += 1
    return i


run_failed = []
for j in range(int(sys.argv[1])):	
		run_failed.append(j)

if (sys.argv[9] == '1'):

	crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile-RTS'+'.csv',"rb"))
   
elif (sys.argv[9] == '0'):
	crlog = csv.reader(open(sys.argv[8]+sys.argv[2]+'-Runs-logfile-NO_RTS'+'.csv',"rb"))

for row in crlog:
	if int(row[0][5:]) in run_failed:
  	    run_failed.remove(int(row[0][5:]))



casefile = []
for i in range(int(sys.argv[1])):
	if i not in run_failed:
		if (sys.argv[9] == '1'):
	    		casefile.append(sys.argv[8]+sys.argv[2]+'-macTxDrop-RTS-'+str(i)+'.csv')
		elif (sys.argv[9] == '0'):
			casefile.append(sys.argv[8]+sys.argv[2]+'-macTxDrop-NO_RTS-'+str(i)+'.csv')

list_Delay = []
#Y = [[] for i in range(int(sys.argv[1]))]
delais = [[] for i in range(int(sys.argv[1]))]

bins = []


real_runs = len(casefile)
#s2 = 0.0
y =0
packets =0
with open('Average_Value_STA_Assoc.csv', "wb") as file:
    fileCDF = csv.writer(file)
    #fileCDF.writerow(["Case",
                      #"Val"])
    for i in range(real_runs): ### Changed was int(sys.argv[1])

        cr = csv.reader(open(casefile[i],"rb"))


        #print csvcount('stats100-1.csv')
        cr.next()
	cr.next()

        for row in cr:
            #s = row[1]
            #print row
            ###print y
	    packets = packets +1
            #s2 = s2+float(s)
            #bins[y].append(int(row[0]))
	    
	    delais[y].append(int(row[1]))
            #bins.append(row[0])
	y = y +1
    


   ### for j in range(len(delais)):
   ###     delais[j] = np.average(delais[j])


   ### for j in range(len(delais)):
	      ###bins.append(j+1)


delaisdef = [[] for i in range(int(sys.argv[1]))]

for i in range(len(delais)):

  delaisdef[i] = np.array(delais[i])

macdrop_sum = []
indice = []
for i in range(int(sys.argv[1])):
  indice.append(i+1) 
  macdrop_sum.append(str(np.sum(delaisdef[i])))

###print delais
ind = [x for x, _ in enumerate(bins)]
###print bins

###plt.plot(bins, delais, label='Delay', color='#3dcc76')


plt.boxplot(delais, sym='')
plt.yscale('linear')
plt.xticks(indice, macdrop_sum, fontsize = 8)

###plt.yscale('log')
###plt.xticks(ind, bins, rotation = 'vertical', fontsize = 8)
plt.ylabel("MacTxDrop Value")
plt.xlabel("Run Number")
###plt.legend(loc="center left")
plt.title("MacTxDrop (Nb of STA: " + sys.argv[3] + " Sim. Time: " + sys.argv[4] + "s Distance: " + sys.argv[5] + "m Runs: " + sys.argv[1] + " Phy: " + sys.argv[6] + " DR: " + sys.argv[7] + " RTS: " + sys.argv[9] + ")", fontsize = 8)




#plt.figure()
#plt.bar([TX,RX,CCA,IDLE], bins, stacked=True, normed = True)
#plt.show()
if (sys.argv[10] == 'SimpleTest'):
	plt.savefig(sys.argv[8]+'PDFs/'+"Average_MacTxDrop"+sys.argv[2]+".pdf", dpi = 150)
else:
	if (sys.argv[9] == '1'):
		plt.savefig(sys.argv[8]+'RTS_PDFs/'+"Average_MacTxDrop"+sys.argv[2]+".pdf", dpi = 150)
	elif (sys.argv[9] == '0'):
		plt.savefig(sys.argv[8]+'NO_RTS_PDFs/'+"Average_MacTxDrop"+sys.argv[2]+".pdf", dpi = 150)
print " Done!"
