#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os
import time
from multiprocessing import Pool
	

if not os.path.exists('results/'+sys.argv[5]):
	os.mkdir(os.path.abspath('results/'+sys.argv[5]), 0755)
	os.mkdir(os.path.abspath('results/'+sys.argv[5] +'/RTS_PDFs'), 0755)
	os.mkdir(os.path.abspath('results/'+sys.argv[5] +'/NO_RTS_PDFs'), 0755)
	path  = 'results/'+sys.argv[5]+'/'
else:
	os.mkdir(os.path.abspath('results/'+sys.argv[5]+'-'+str(time.time())), 0755)
	os.mkdir(os.path.abspath('results/'+sys.argv[5]+'-'+str(time.time()) +'/RTS_PDFs'), 0755)
	os.mkdir(os.path.abspath('results/'+sys.argv[5]+'-'+str(time.time()) +'/NO_RTS_PDFs'), 0755)
	path  = 'results/'+sys.argv[5]+'-'+str(time.time())+'/'



def start_simulation(data):
	simulationTime = data[0]
	disApSta = data[1]
	nodeNb = data[2]
	run_ns3 = data[3]
	name_sim = data[4]
	data_Rate = data[5]
	phy_Mode = data[6]
	rts_Cts = data[7]
	
	
	

# > LogF_Essai_Fail 2>&1
# > LogF_RTS_Fail.txt 2>&1

	
	os.system('./waf --run "scenario_F_test_2STA --scheme=1 --dataRate=%s --phyMode=%s --name=%s --simulationTime=%d --disApSta=%d --nodeNb=%d --run=%d --rtscts=%d --path=%s  --pcap=false --verbose=false" > LogF_RTS_Fail_RTS.txt 2>&1 ' % (data_Rate, phy_Mode, name_sim, simulationTime, disApSta, nodeNb, run_ns3, rts_Cts ,path))

if len(sys.argv) != 8:
	print "usage: Simulation Time / Distance AP STA / Number of STA / Number of Runs / Name / DataRate / PhyMode "
	sys.exit(0)

max_homes = int(sys.argv[1])
step = int(sys.argv[2])
sub_runs = int(sys.argv[3])
processes = int(sys.argv[4])

max_homes = 1
step = 1
sub_runs = int(sys.argv[4])
processes = 20



##create params
#params = []
#run = 1
#print path
#for i in range(0, max_homes+step, step):
#	homes = i
#	if homes == 0 and step > 1:
#		homes = 1
#	elif homes == 0 and step == 1:
#		continue
#
#	for j in range(sub_runs):
#		params.append([int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), j, sys.argv[5], sys.argv[6], sys.argv[7], 0 ,path])
#		print(params)
#		#params.append([run, disApSta+1, nodeNb])
#	run += 1

# run
#pool = Pool(processes=processes)
#pool.map(start_simulation, params)
#os.system('python Script_Throughput_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
#os.system('python Script_Phy_State_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
#os.system('python Script_Polar_Graph_PacketLoss.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
#os.system('python Script_Delay_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
#os.system('python Script_Delay_Average2.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
#os.system('python Script_BackOff_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
#os.system('python Script_MacTxDrop_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
#os.system('python Script_PhyRxDrop_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
#os.system('python Script_BO_Graph.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
#os.system('python Script_CW_Graph.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '0' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate

params2 = []
run = 1

for i in range(0, max_homes+step, step):
	homes = i
	if homes == 0 and step > 1:
		homes = 1
	elif homes == 0 and step == 1:
		continue

	for j in range(sub_runs):
		params2.append([int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), j, sys.argv[5], sys.argv[6], sys.argv[7], int(1), path])
		print(params2)
		#params.append([run, disApSta+1, nodeNb])
	run += 1

# run
pool = Pool(processes=processes)
pool.map(start_simulation, params2)
os.system('python Script_Throughput_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
os.system('python Script_Phy_State_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
os.system('python Script_Polar_Graph_PacketLoss.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
os.system('python Script_Delay_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
os.system('python Script_Delay_Average2.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
os.system('python Script_BackOff_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
os.system('python Script_MacTxDrop_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
os.system('python Script_PhyRxDrop_Average.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRateos.system('python Script_BO_Graph.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
os.system('python Script_CW_Graph.py '+ sys.argv[4] + ' ' + sys.argv[5] + ' ' + sys.argv[3] + ' ' + sys.argv[1] + ' ' + sys.argv[2] + ' ' + sys.argv[7] + ' ' + sys.argv[6] + ' ' + path + ' ' + '1' + ' ' + '0')
#						Nb of Run		Name Of Simulation   Nb of STA        Duration 		 Distance             PhyMode	        DataRate
